# Planets #

* A simple planet generator. Planets are generated from cubic voxels and are fairly cubic themselves.
* Terrain Generated via 6 height maps
* Has biomes and multiple texture supports
* Dynamic voxel mesh through polygon creation
* Simple point colliders (**Note that cameras attatched to point colliders will be able to see through mesh if close enough due to near clipping planes)

### How do I get set up? ###

* Project is a Unity project folder. Open it in Unity (Unity 5.5.2)

![planets.png](https://bitbucket.org/repo/Ggnzb6x/images/1365864762-planets.png)

### Notes ###

*Contains a fairly expansive collision library
*Performant axis aligned bbox trees and voxel specific structures.