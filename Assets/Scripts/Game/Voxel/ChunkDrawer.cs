﻿using UnityEngine;
using System.Collections.Generic;

public partial class VoxelGroup
{
    partial class Chunk
    {
        /// <summary>
        /// Used to draw voxels. 
        /// </summary>
        public partial class ChunkDrawer
        {
            /// <summary>
            /// Chunk this drawer belongs to
            /// </summary>
            Chunk Parent
            {
                get
                {
                    return parent;
                }
            }
            Chunk parent;

            Dictionary<Material, VoxelMesh> Mesh;

            public ChunkDrawer(Chunk Parent)
            {
                parent = Parent;
            }

            /// <summary>
            /// Draws the chunk. Clears old data if necessary.
            /// </summary>
            public void Draw()
            {
                //delete existing mesh
                DeleteMesh();

                //ignore air blocks
                if (Parent.AirChunk) return;
                //solid chunks
                if (Parent.SolidChunk) { DrawSolidChunk(); return;}

                int i = Parent.VoxelLength;
                Vector3Int min = Parent.Min;
                Vector3Int max = Parent.Max;
                //verts and tris
                Dictionary<int, List<Vector3>> verts = new Dictionary<int, List<Vector3>>();
                Dictionary <int, List<int>> tris = new Dictionary<int, List<int>>();
                Dictionary <int, List<Vector2>> uvs = new Dictionary<int, List<Vector2>>();

                //draw
                DrawSegment(min, max, i, verts, tris, uvs);

                //handle adjacent chunk degenerate cases
                DoAdjacent(i, verts, tris, uvs);

                //make objects
                SpawnMeshObjects(verts, tris, uvs);
            }

            /// <summary>
            /// Create Mesh Objects from the input mesh data
            /// </summary>
            /// <param name="verts"></param>
            /// <param name="tris"></param>
            /// <param name="uvs"></param>
            void SpawnMeshObjects(Dictionary<int, List<Vector3>> verts, Dictionary<int, List<int>> tris,
                Dictionary<int, List<Vector2>> uvs)
            {
                //foreach material
                foreach (var batch_id in verts.Keys)
                {
                    if (ReferenceEquals(Mesh, null))
                        Mesh = new Dictionary<Material, VoxelMesh>();
                    var mat = BlockLibrary.BatchMaterial(batch_id);
                    if (ReferenceEquals(mat, null))
                        throw new System.Exception("Error, tried to draw voxels with null material");
                    Mesh.Add(mat, VoxelMesh.NewVoxelMesh(verts[batch_id].ToArray(),
                        tris[batch_id].ToArray(), mat, uvs[batch_id].ToArray(), Parent.Parent));
                }
            }

            /// <summary>
            /// Draw a rectangular region of this chunk
            /// </summary>
            /// <param name="min"> min box coordinate</param>
            /// <param name="max"> max box coordinate </param>
            /// <param name="i"> stride, should equal chunk.voxellength </param>
            /// <param name="m"> marching cubes object</param>
            /// <param name="verts"> vert dictioary</param>
            /// <param name="tris"> tris dictionary</param>
            /// <param name="uvs"> uvs dictionary</param>
            void DrawSegment(Vector3Int min, Vector3Int max, int i,
                Dictionary<int, List<Vector3>> verts, Dictionary<int, List<int>> tris,
                Dictionary<int, List<Vector2>> uvs)
            {
                //iterate through voxels
                for (int x = min.x; x < max.x; x += i)
                {
                    for (int y = min.y; y < max.y; y += i)
                    {
                        for (int z = min.z; z < max.z; z += i)
                        {
                            //get current non-smooth block
                            var block = BlockLibrary.GetBlock(parent[x, y, z]);
                            //draw cubes
                            if (block.Drawable && block.Block_Type == Block.BlockType.CUBE)
                            {
                                //batch id
                                var batch_id = block.BatchID;

                                //add if not added yet
                                if (!verts.ContainsKey(batch_id))
                                {
                                    verts.Add(batch_id, new List<Vector3>());
                                    tris.Add(batch_id, new List<int>());
                                    uvs.Add(batch_id, new List<Vector2>());
                                }

                                //cube position
                                var bpos = new Vector3(x, y, z);
                                //get blocks
                                var up = BlockLibrary.GetBlock(parent[x, y + i, z]);
                                var down = BlockLibrary.GetBlock(parent[x, y - 1, z]);
                                var left = BlockLibrary.GetBlock(parent[x - 1, y, z]);
                                var right = BlockLibrary.GetBlock(parent[x + i, y, z]);
                                var front = BlockLibrary.GetBlock(parent[x, y, z + i]);
                                var back = BlockLibrary.GetBlock(parent[x, y, z - 1]);
                                //add cube verts
                                DrawCube(verts[batch_id], tris[batch_id], uvs[batch_id],
                                    bpos, block, up, down, front, back, left, right);
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// Handle drawing degenerate cases for adjacent chunks. Returns true if it drew something.
            /// </summary>
            /// <param name="i"></param>
            /// <param name="m"></param>
            /// <param name="verts"></param>
            /// <param name="tris"></param>
            /// <param name="uvs"></param>
            bool DoAdjacent(int i,
                Dictionary<int, List<Vector3>> verts, Dictionary<int, List<int>> tris,
                Dictionary<int, List<Vector2>> uvs)
            {
                //return true if we draw something
                bool return_value = false;

                //degenerate cases where we draw face segments from other chunks.
                bool draw_degen_down = Parent.Down == null || Parent.Down.AirChunk;
                bool draw_degen_left = Parent.Left == null || Parent.Left.AirChunk;
                bool draw_degen_back = Parent.Back == null || Parent.Back.AirChunk;

                //for reference, corners:
                //    4-------5
                //   /|      /|
                //  / |     / |
                // 7--|----6  |
                // |  0----|--1
                // | /     | /
                // 3-------2
                //for reference, edges:
                //    *---4---*
                //   /|      /|
                //  7 |     5 |
                // *--|6---*  |
                // |  *--0-|--*
                // | 3     | 1
                // *---2---*
                //for reference, edges:
                //    *-------*
                //   /|      /|
                //  / 8     / 9
                // *--|----*  |
                // |   *---|--*
                // 11/    10 /
                // *-------*

                //degen corner cases
                bool dc_3 = draw_degen_down && draw_degen_left && draw_degen_back;
                bool dc_0 = draw_degen_down && draw_degen_left;
                bool dc_7 = draw_degen_left && draw_degen_back, dc_2 = draw_degen_down && draw_degen_back;

                //degen edges
                bool de_2 = draw_degen_down && draw_degen_back, de_3 = draw_degen_down && draw_degen_left;
                bool de_11 = draw_degen_back && draw_degen_left;

                //degen faces
                //down
                if (draw_degen_down)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Min.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y - 1, Parent.Min.z);
                    DrawSegment(min, max, i,verts, tris, uvs); return_value = true;
                }
                //left
                if (draw_degen_left)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x, Parent.Max.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x-1, Parent.Min.y, Parent.Min.z);
                    DrawSegment(min, max, i,verts, tris, uvs); return_value = true;
                }
                //back
                if (draw_degen_back)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y, Parent.Min.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Min.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                //degen corners
                if (dc_3)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Min.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x-1, Parent.Min.y-1, Parent.Min.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                if (dc_0)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x - 1, Parent.Min.y - 1, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                if (dc_7)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x, Parent.Max.y, Parent.Min.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x - 1, Parent.Max.y - 1, Parent.Min.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                if (dc_2)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Min.y, Parent.Min.z);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y - 1, Parent.Min.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                //degen edges
                if (de_11)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x, Parent.Max.y, Parent.Min.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x - 1, Parent.Min.y, Parent.Min.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                if (de_2)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Min.y, Parent.Min.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y - 1, Parent.Min.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }
                if (de_3)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x - 1, Parent.Min.y - 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs); return_value = true;
                }

                return return_value;
            }

            /// <summary>
            /// Draw a solid chunk.
            /// </summary>
            void DrawSolidChunk()
            {
                //get immediate neighbor chunks
                var up = Parent.Up;
                var down = Parent.Down;
                var left = Parent.Left;
                var right = Parent.Right;
                var front = parent.Forward;
                var back = Parent.Back;

                //adjacent non-null non-single block chunks
                bool draw_up = up != null && !up.IsSingleBlockChunk;
                bool draw_down = down != null && !down.IsSingleBlockChunk;
                bool draw_left = left != null && !left.IsSingleBlockChunk;
                bool draw_right = right != null && !right.IsSingleBlockChunk;
                bool draw_front = front != null && !front.IsSingleBlockChunk;
                bool draw_back = back != null && !back.IsSingleBlockChunk;

                int i = Parent.VoxelLength;
                //verts and tris
                Dictionary<int, List<Vector3>> verts = new Dictionary<int, List<Vector3>>();
                Dictionary<int, List<int>> tris = new Dictionary<int, List<int>>();
                Dictionary<int, List<Vector2>> uvs = new Dictionary<int, List<Vector2>>();

                //do degenerate cases
                bool drew_adjacents = DoAdjacent(i, verts, tris, uvs);

                //if all false
                if (!draw_up && !draw_down && !draw_left && !draw_right && !draw_front && !draw_back
                    && !drew_adjacents)
                    return;

                //for reference, corners:
                //    4-------5
                //   /|      /|
                //  / |     / |
                // 7--|----6  |
                // |  0----|--1
                // | /     | /
                // 3-------2
                //for reference, edges:
                //    *---4---*
                //   /|      /|
                //  7 |     5 |
                // *--|6---*  |
                // |  *--0-|--*
                // | 3     | 1
                // *---2---*
                //for reference, edges:
                //    *-------*
                //   /|      /|
                //  / 8     / 9
                // *--|----*  |
                // |   *---|--*
                // 11/    10 /
                // *-------*

                //assign draw values for each corner
                bool c0 = draw_left || draw_front || draw_down, c1 = draw_right || draw_front || draw_down;
                bool c2 = draw_right || draw_back || draw_down, c3 = draw_left || draw_back || draw_down;
                bool c4 = draw_left || draw_front || draw_up, c5 = draw_right || draw_front || draw_up;
                bool c6 = draw_right || draw_back || draw_up, c7 = draw_left || draw_back || draw_up;

                //values for each edge
                bool e0 = draw_front || draw_down, e1 = draw_right || draw_down, e2 = draw_back || draw_down;
                bool e3 = draw_left || draw_down, e4 = draw_front || draw_up, e5 = draw_right || draw_up;
                bool e6 = draw_back || draw_up, e7 = draw_left || draw_up, e8 = draw_left || draw_front;
                bool e9 = draw_front || draw_right, e10 = draw_right || draw_back, e11 = draw_back || draw_left;

                if (draw_up)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Max.y, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Max.y - 1, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (draw_down)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Min.y + 1, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Min.y, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (draw_left)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Max.y - 1, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y + 1, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (draw_right)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y - 1, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y + 1, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (draw_front)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Max.y - 1, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Min.y + 1, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (draw_back)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Max.y - 1, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Min.y + 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c0)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Min.y + 1, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c1)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Min.y + 1, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c2)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Min.y + 1, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c3)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Min.y + 1, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c4)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Max.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Max.y - 1, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c5)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Max.y - 1, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c6)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Max.y - 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (c7)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Max.y, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Max.y - 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e0)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Min.y + 1, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Min.y, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e1)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Min.y + 1, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e2)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Min.y + 1, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Min.y, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e3)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Min.y + 1, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e4)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Max.y, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Max.y - 1, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e5)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Max.y - 1, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e6)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x - 1, Parent.Max.y, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x + 1, Parent.Max.y - 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e7)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Max.y, Parent.Max.z - 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Max.y - 1, Parent.Min.z + 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e8)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Max.y - 1, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y + 1, Parent.Max.z  - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e9)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y - 1, Parent.Max.z);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y + 1, Parent.Max.z - 1);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e10)
                {
                    Vector3Int max = new Vector3Int(Parent.Max.x, Parent.Max.y - 1, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Max.x - 1, Parent.Min.y + 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }
                if (e11)
                {
                    Vector3Int max = new Vector3Int(Parent.Min.x + 1, Parent.Max.y - 1, Parent.Min.z + 1);
                    Vector3Int min = new Vector3Int(Parent.Min.x, Parent.Min.y + 1, Parent.Min.z);
                    DrawSegment(min, max, i, verts, tris, uvs);
                }

                //make objects
                SpawnMeshObjects(verts, tris, uvs);
            }

            /// <summary>
            /// Draw a cube at the input position 'bpos', adds vertices and and triangles to input lists.
            /// The input blocks are the directly adjacent blocks
            /// </summary>
            /// <param name="verts"></param>
            /// <param name="tris"></param>
            /// <param name="bpos"></param>
            /// <param name="up"></param>
            /// <param name="down"></param>
            /// <param name="front"></param>
            /// <param name="back"></param>
            /// <param name="left"></param>
            /// <param name="right"></param>
            void DrawCube(List<Vector3> verts, List<int> tris, List<Vector2> uvs, Vector3 bpos,
                Block block, Block up, Block down, Block front, Block back, Block left, Block right)
            {
                float x = bpos.x; float y = bpos.y; float z = bpos.z;
                if (!up.Drawable)
                {
                    int c = verts.Count;
                    verts.Add(new Vector3(x, y + 1, z));
                    verts.Add(new Vector3(x, y + 1, z + 1));
                    verts.Add(new Vector3(x + 1, y + 1, z));
                    verts.Add(new Vector3(x + 1, y + 1, z + 1));
                    tris.Add(c + 2); tris.Add(c); tris.Add(c + 1);
                    tris.Add(c + 2); tris.Add(c + 1); tris.Add(c + 3);
                    if (!ReferenceEquals(null, block.texture))
                    {
                        var min = block.TileMin; var max = block.TileMax;
                        uvs.Add(min);uvs.Add(new Vector2(min.x, max.y));
                        uvs.Add(new Vector2(max.x, min.y));uvs.Add(max);
                    }
                }
                if(!down.Drawable)
                {
                    int c = verts.Count;
                    verts.Add(new Vector3(x, y, z));
                    verts.Add(new Vector3(x, y, z + 1));
                    verts.Add(new Vector3(x + 1, y, z));
                    verts.Add(new Vector3(x + 1, y, z + 1));
                    tris.Add(c + 1); tris.Add(c); tris.Add(c + 2);
                    tris.Add(c + 3); tris.Add(c + 1); tris.Add(c + 2);
                    if (!ReferenceEquals(null, block.texture))
                    {
                        var min = block.TileMin; var max = block.TileMax;
                        uvs.Add(min); uvs.Add(new Vector2(min.x, max.y));
                        uvs.Add(new Vector2(max.x, min.y)); uvs.Add(max);
                    }
                }
                if (!front.Drawable)
                {
                    int c = verts.Count;
                    verts.Add(new Vector3(x, y, z + 1));
                    verts.Add(new Vector3(x, y + 1, z + 1));
                    verts.Add(new Vector3(x + 1, y, z + 1));
                    verts.Add(new Vector3(x + 1, y + 1, z + 1));
                    tris.Add(c); tris.Add(c + 2); tris.Add(c + 3);
                    tris.Add(c); tris.Add(c + 3); tris.Add(c + 1);
                    if (!ReferenceEquals(null, block.texture))
                    {
                        var min = block.TileMin; var max = block.TileMax;
                        uvs.Add(min); uvs.Add(new Vector2(min.x, max.y));
                        uvs.Add(new Vector2(max.x, min.y)); uvs.Add(max);
                    }
                }
                if (!back.Drawable)
                {
                    int c = verts.Count;
                    verts.Add(new Vector3(x, y, z));
                    verts.Add(new Vector3(x, y + 1, z));
                    verts.Add(new Vector3(x + 1, y, z));
                    verts.Add(new Vector3(x + 1, y + 1, z));
                    tris.Add(c + 2); tris.Add(c); tris.Add(c + 1);
                    tris.Add(c + 2); tris.Add(c + 1); tris.Add(c + 3);
                    if (!ReferenceEquals(null, block.texture))
                    {
                        var min = block.TileMin; var max = block.TileMax;
                        uvs.Add(min); uvs.Add(new Vector2(min.x, max.y));
                        uvs.Add(new Vector2(max.x, min.y)); uvs.Add(max);
                    }
                }
                if (!right.Drawable)
                {
                    int c = verts.Count;
                    verts.Add(new Vector3(x + 1, y, z));
                    verts.Add(new Vector3(x + 1, y, z + 1));
                    verts.Add(new Vector3(x + 1, y + 1, z));
                    verts.Add(new Vector3(x + 1, y + 1, z + 1));
                    tris.Add(c + 1); tris.Add(c); tris.Add(c + 2);
                    tris.Add(c + 1); tris.Add(c + 2); tris.Add(c + 3);
                    if (!ReferenceEquals(null, block.texture))
                    {
                        var min = block.TileMin; var max = block.TileMax;
                        uvs.Add(min); uvs.Add(new Vector2(max.x, min.y));
                        uvs.Add(new Vector2(min.x, max.y)); uvs.Add(max);
                    }
                }
                if (!left.Drawable)
                {
                    int c = verts.Count;
                    verts.Add(new Vector3(x, y, z));
                    verts.Add(new Vector3(x, y, z + 1));
                    verts.Add(new Vector3(x, y + 1, z));
                    verts.Add(new Vector3(x, y + 1, z + 1));
                    tris.Add(c); tris.Add(c + 1); tris.Add(c + 3);
                    tris.Add(c); tris.Add(c + 3); tris.Add(c + 2);
                    if (!ReferenceEquals(null, block.texture))
                    {
                        var min = block.TileMin; var max = block.TileMax;
                        uvs.Add(min); uvs.Add(new Vector2(max.x, min.y));
                        uvs.Add(new Vector2(min.x, max.y)); uvs.Add(max);
                    }
                }
            }

            /// <summary>
            /// Draw a cube without respect to neighbors
            /// </summary>
            /// <param name="verts"></param>
            /// <param name="tris"></param>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <param name="z"></param>
            void DebugCube(List<Vector3> verts, List<int> tris, float x, float y, float z)
            {
                int c = verts.Count;
                
                verts.Add(new Vector3(x, y, z));
                verts.Add(new Vector3(x, y, z + 1));
                verts.Add(new Vector3(x, y + 1, z));
                verts.Add(new Vector3(x, y + 1, z + 1));
                verts.Add(new Vector3(x + 1, y, z));
                verts.Add(new Vector3(x + 1, y, z + 1));
                verts.Add(new Vector3(x + 1, y + 1, z));
                verts.Add(new Vector3(x + 1, y + 1, z + 1));

                tris.Add(c); tris.Add(c + 1); tris.Add(c + 3);

                tris.Add(c); tris.Add(c + 3); tris.Add(c + 2);

                tris.Add(c + 1); tris.Add(c + 5); tris.Add(c + 7);

                tris.Add(c + 1); tris.Add(c + 7); tris.Add(c + 3);

                tris.Add(c + 5); tris.Add(c + 4); tris.Add(c + 6);

                tris.Add(c + 5); tris.Add(c + 6); tris.Add(c + 7);

                tris.Add(c + 4); tris.Add(c); tris.Add(c + 2);

                tris.Add(c + 4); tris.Add(c + 2); tris.Add(c + 6);

                tris.Add(c + 6); tris.Add(c + 2); tris.Add(c + 3);

                tris.Add(c + 6); tris.Add(c + 3); tris.Add(c + 7);

                tris.Add(c + 1); tris.Add(c); tris.Add(c + 4);

                tris.Add(c + 5); tris.Add(c + 1); tris.Add(c + 4);
            }

            /// <summary>
            /// Delete the mesh for this chunk
            /// </summary>
            public void DeleteMesh()
            {
                if (Mesh != null)
                {
                    //DESTROY
                    foreach (var vm in Mesh)
                        MonoBehaviour.Destroy(vm.Value);
                    Mesh.Clear();
                }
            }
        }
    }
}
