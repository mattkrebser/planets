﻿using UnityEngine;
using System.Collections;

using Voxel = System.Int32;

public partial class VoxelGroup
{
    /// <summary>
    /// A chunk tree designed for storage of any amount of voxel data.
    /// </summary>
    partial class FullChunkTree
    {
        ResolutionTree<Chunk> tree;

        public FullChunkTree(ResolutionTree<Chunk> ctree)
        {
            if (ctree == null)
                throw new System.Exception("Error, must input non-null tree");
            tree = ctree;
        }

        /// <summary>
        /// Add a chunk
        /// </summary>
        /// <param name="c"></param>
        public void AddChunk(Chunk c)
        {
            tree[c.ChunkPos, c.Resolution] = c;
        }

        /// <summary>
        /// remove a chunk and all of its children
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool DeleteChunk(Chunk c)
        {
            return tree.RemoveValue(c.ChunkPos, c.Resolution);
        }

        /// <summary>
        /// update an entire chunk upwards.
        /// </summary>
        /// <param name="c"></param>
        public void UpdateChunkUp(Chunk c)
        {
            ResolutionTree<Chunk>.ResolutionTreeNode<Chunk> node;
            if (tree.TryGetNode(c.ChunkPos, c.Resolution, out node))
            {
                //trickle changes up
                int to_res = tree.Root == null ? 0 : tree.Root.resolution + 1;
                Vector3Int min = c.Min; Vector3Int max = c.Max;
                for (int i = c.Resolution; i < to_res; i++)
                {
                    TrickleUp(node, min, max);
                    node = node.parent;
                    if (node == null) break;
                }
            }
            else
                throw new System.Exception("Error, tried to update non existing chunk");
        }

        void TrickleDown(Chunk c, Vector3Int min, Vector3Int max)
        {

        }

        /// <summary>
        /// Trickle the node value (chunk) upwards. The input 'min' and 'max' are expected to not
        /// overlap multiple chunks
        /// </summary>
        /// <param name="node"> node to trickle up</param>
        /// <param name="min"> start iteration bounds </param>
        /// <param name="max"> end iteration bounds </param>
        void TrickleUp(ResolutionTree<Chunk>.ResolutionTreeNode<Chunk> node,
            Vector3Int min, Vector3Int max)
        {
            //if the parent is null then we are on the root, so just return
            if (node.parent == null) return;

            if (node.Value.IsInvalid)
                throw new System.Exception("Error, input unitialized chunk.");

            //if the low resolution chunk is null, then we need to create it
            if (node.parent.Value == null)
            {
                //create a new chunk and set it's position & resolution
                node.parent.Value = new Chunk(node.Value.Parent);
                node.parent.Value.Set(node.parent.pos, node.parent.resolution);
            }

            //get low and high res chunk references
            Chunk high_res = node.Value;
            Chunk low_res = node.parent.Value;

            //singblock chunk, just asign a single voxel upwards
            if (high_res.IsSingleBlockChunk)
            {
                //if the low res chunk has not been initialized
                if (low_res.IsInvalid)
                {
                    //assign single block ID, and that's it
                    low_res.SingleBlockID = high_res.SingleBlockID;
                }
                else
                {
                    //if the low res chunk is a single block chunk...
                    if (low_res.IsSingleBlockChunk)
                    {
                        //if same block ID's, then just return, no copy needed
                        if (low_res.SingleBlockID == high_res.SingleBlockID)
                            return;
                        //otherwise, we have to get a voxel array and assign values to it
                        low_res.Voxels = GetFreeVoxelArray(low_res.SingleBlockID);
                        //then copy the high_res singleID into the low_res voxel array
                        ChunkValueCopy(low_res, high_res.SingleBlockID, min, max);
                    }
                    //otherwise, the low res chunk should have an initialized voxel array
                    else
                    {
                        //just copy the value into the low res chunk
                        ChunkValueCopy(low_res, high_res.SingleBlockID, min, max);
                    }
                }
            }
            //otherwise we copy the whole chunk
            else
            {
                ChunkOverlapCopyUp(high_res, low_res, min, max);
            }
        }

        /// <summary>
        /// Copy a high resolution chunk's data into a lower resolution chunk's data. 
        /// It is assumed that there is a difference in resolution of '1'
        /// </summary>
        /// <param name="high_res"> input high resolution chunk </param>
        /// <param name="low_res"> input low resolution chunk </param>
        /// <param name="min"> start iteration copy bounds </param>
        /// <param name="max"> stop iteration copy bounds </param>
        void ChunkOverlapCopyUp(Chunk high_res, Chunk low_res, Vector3Int min, Vector3Int max)
        {
            if (low_res.Resolution - high_res.Resolution != 1)
                throw new System.Exception("Chunk copy resolution error: expected resolution difference is 1");

            //get low resolution iteration bounds
            Vector3Int lmin = ResolutionTree<Chunk>.ToRes(min - low_res.ChunkPos, 0, low_res.Resolution);
            Vector3Int lmax = ResolutionTree<Chunk>.ToRes(max - min, 0, low_res.Resolution);
            //high resolution iteration parameters
            int hx = lmin.x, hy = lmin.y, hz = lmin.z;
            //voxel
            int voxel = Block.NULL;
            //loop through data
            for (int x = lmin.x; x < lmax.x; x++)
            {
                for (int y = lmin.y; y < lmax.y; y++)
                {
                    for (int z = lmin.x; z < lmax.x; z++)
                    {
                        //we have tocheck all 8 blocks in the high res chunk, but we
                        //take the first solid or drawable voxel
                        var blocks = Chunk.block(voxel=high_res.Voxels[hx,hy,hz]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx, hy, hz] = voxel; continue; };

                        blocks = Chunk.block(voxel = high_res.Voxels[hx, hy, hz+1]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx, hy, hz+1] = voxel; continue; };

                        blocks = Chunk.block(voxel = high_res.Voxels[hx, hy+1, hz]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx, hy+1, hz] = voxel; continue; };

                        blocks = Chunk.block(voxel = high_res.Voxels[hx+1, hy, hz]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx+1, hy, hz] = voxel; continue; };

                        blocks = Chunk.block(voxel = high_res.Voxels[hx+1, hy, hz+1]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx+1, hy, hz+1] = voxel; continue; };

                        blocks = Chunk.block(voxel = high_res.Voxels[hx+1, hy+1, hz]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx+1, hy+1, hz] = voxel; continue; };

                        blocks = Chunk.block(voxel = high_res.Voxels[hx+1, hy+1, hz+1]);
                        if (blocks.Drawable || blocks.Collide)
                        { low_res.Voxels[hx+1, hy+1, hz+1] = voxel; continue; };

                        //increment high_res
                        hz += 2;
                    }

                    //increment high res
                    hy += 2;
                    hz = lmin.z;
                }

                //increment high res
                hy = lmin.y;
                hx += 2;
            }
        }

        /// <summary>
        /// Copy a voxel value into the input chunk over the input iteration bounds
        /// </summary>
        /// <param name="c"></param>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        void ChunkValueCopy(Chunk c, Voxel value, Vector3Int min, Vector3Int max)
        {
            //get low resolution iteration bounds
            Vector3Int lmin = ResolutionTree<Chunk>.ToRes(min - c.ChunkPos, 0, c.Resolution);
            Vector3Int lmax = ResolutionTree<Chunk>.ToRes(max - min, 0, c.Resolution);
            //loop through data
            for (int x = lmin.x; x < lmax.x; x++)
            {
                for (int y = lmin.y; y < lmax.y; y++)
                {
                    for (int z = lmin.x; z < lmax.x; z++)
                    {
                        c.Voxels[x, y, z] = value;
                    }
                }
            }
        }

        /// <summary>
        /// Get a free voxel array, TODO: Implement chunk voxel pool
        /// </summary>
        /// <returns></returns>
        Voxel[,,] GetFreeVoxelArray(Voxel Initial_Value)
        {
            var result = new int[ChunkSize, ChunkSize, ChunkSize];
            for (int x = 0; x < ChunkSize; x++)
            {
                for (int y = 0; y < ChunkSize; y++)
                {
                    for (int z = 0; z < ChunkSize; z++)
                    {
                        result[x, y, z] = Initial_Value;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get a free voxel array, TODO: Implement chunk voxel pool
        /// </summary>
        /// <returns></returns>
        Voxel[,,] GetFreeVoxelArray()
        {
            return new int[ChunkSize, ChunkSize, ChunkSize];
        }
    }
}
