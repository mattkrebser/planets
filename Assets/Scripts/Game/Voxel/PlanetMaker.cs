﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

using Voxel = System.Int32;
using Chunk = VoxelGroup.Chunk;
using Faces = VoxelGroup.Faces;

public class PlanetMaker : VoxelGroup.VoxelMaker
{
    /// <summary>
    /// Chunk Generation object
    /// </summary>
    Generator generator;
    Planet planet;

    public PlanetMaker(Planet _planet) : base()
    {
        planet = _planet;
    }

    /// <summary>
    /// Generate the whole voxel group at input resolution
    /// </summary>
    public override void GenerateAll(int resolution = 0)
    {
        GeneratePlanetAll(planet, resolution);
    }

    /// <summary>
    /// Initialize generator
    /// </summary>
    public override void Initialize()
    {
        InitPlanetGen();
    }

    /// <summary>
    /// Initialize generator objects
    /// </summary>
    void InitPlanetGen()
    {
        if (generator == null)
        {
            generator = new Generator();
            generator.NoiseObj = new Noise(seed);
            generator.up = new ResolutionTree2d<HeightMap>
                (new Vector2Int(Parent.VoxelGroupSize.x, Parent.VoxelGroupSize.z), VoxelGroup.ChunkSizeResolution);
            generator.down = new ResolutionTree2d<HeightMap>
                (new Vector2Int(Parent.VoxelGroupSize.x, Parent.VoxelGroupSize.z), VoxelGroup.ChunkSizeResolution);
            generator.right = new ResolutionTree2d<HeightMap>
                (new Vector2Int(Parent.VoxelGroupSize.y, Parent.VoxelGroupSize.z), VoxelGroup.ChunkSizeResolution);
            generator.left = new ResolutionTree2d<HeightMap>
                (new Vector2Int(Parent.VoxelGroupSize.y, Parent.VoxelGroupSize.z), VoxelGroup.ChunkSizeResolution);
            generator.forward = new ResolutionTree2d<HeightMap>
                (new Vector2Int(Parent.VoxelGroupSize.x, Parent.VoxelGroupSize.y), VoxelGroup.ChunkSizeResolution);
            generator.back = new ResolutionTree2d<HeightMap>
                (new Vector2Int(Parent.VoxelGroupSize.x, Parent.VoxelGroupSize.y), VoxelGroup.ChunkSizeResolution);
        }
    }

    /// <summary>
    /// Generate an entire planet. (for this voxel group only)
    /// </summary>
    void GeneratePlanetAll(Planet planet, int resolution)
    {
        Vector3Int cSize = Parent.VoxelGroupSize;
        if (cSize.x != cSize.y || cSize.y != cSize.z || cSize.y != cSize.z)
            throw new System.Exception("Error, planets must be cubed bbox");
        //since all equal, cSize.x==y==z
        int size = cSize.x;
        //get increment for resolution
        int i = ResolutionTree<Chunk>.ToRes(VoxelGroup.ChunkSize, resolution, 0);
        for (int x = 0; x < size; x += i)
        {
            for (int y = 0; y < size; y += i)
            {
                for (int z = 0; z < size; z += i)
                {
                    Parent.VoxelData[x, y, z, resolution] =
                        GenerateChunk(new Vector3Int(x, y, z), resolution);
                }
            }
        }
    }

    /// <summary>
    /// Generate a chunk.
    /// </summary>
    /// <param name="ChunkPos"></param>
    public override Chunk GenerateChunk(Vector3Int pos, int resolution)
    {
        Chunk c = new Chunk(Parent);
        c.Set(pos, resolution);
        var settings = planet.Settings;

        //get faces (up direction for this chunk)
        var faces = MakeHeightMaps(pos, settings, c);
        bool multi_face = MultipleFaces(faces);
        Vector3Int Center = Parent.VoxelGroupCenter;

        //get height map if not multi faced
        HeightMap map = multi_face ? null : GetHeightMap(faces, pos, resolution);

        //if the chunk has multiple block types in it, then it must be filled
        //if it doesn't, then don't fill, just return the chunk
        if (!SingleBlockFill(c, faces, Center))
        {
            int cSize = VoxelGroup.ChunkSize;
            var min = MinChunkIterationBounds(pos, resolution);
            var max = MaxChunkIterationBounds(
                new Vector3Int(pos.x + cSize, pos.y + cSize, pos.z + cSize), resolution);
            var bounds = max - min;

            for (int x = 0; x < bounds.x; x++)
            {
                for (int y = 0; y < bounds.y; y++)
                {
                    for (int z = 0; z < bounds.z; z++)
                    {
                        //if multiple faces, for this chunk, then we have to get the face set that pertains to this block
                        if (multi_face)
                        {
                            faces = Parent.Face(new Vector3Int(pos.x + x, pos.y + y, pos.z + z));
                            map = GetHeightMap(faces, pos, resolution);
                        }
                        //if not, then the block face is already computed
                        //by face I mean which side of the planet the block is on (so we know which height map to use)
                        //see VoxelGroup.Face for more details

                        //Fill the block at the input position and face
                        FillBlock(faces, new Vector3Int(x, y, z), map, c,
                            Vector3Int.Abs(new Vector3Int(pos.x + x, pos.y + y, pos.z + z) - Center));
                    }
                }
            }
        }

        return c;
    }

    /// <summary>
    /// Get Minimum chunk iteration bounds. (Clamping coordinates to be inside parent voxel bbox)
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="resolution"></param>
    /// <returns></returns>
    Vector3Int MinChunkIterationBounds(Vector3Int pos, int resolution)
    {
        var bbox = Parent.VoxelGroupLocalBBox;
        var b_pos = new Vector3Int((int)bbox.Min.x, (int)bbox.Min.y, (int)bbox.Min.z);
        var ipos = new Vector3Int(
            pos.x < b_pos.x ? ResolutionTree<int>.ToRes((b_pos.x), resolution) : pos.x,
            pos.y < b_pos.y ? ResolutionTree<int>.ToRes((b_pos.y), resolution) : pos.y,
            pos.z < b_pos.z ? ResolutionTree<int>.ToRes((b_pos.z), resolution) : pos.z);
        return ipos;
    }
    /// <summary>
    /// Get Maximum chunk iteration bounds. (Clamping coordinates to be inside parent voxel bbox)
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="resolution"></param>
    /// <returns></returns>
    Vector3Int MaxChunkIterationBounds(Vector3Int pos, int resolution)
    {
        var bbox = Parent.VoxelGroupLocalBBox;
        var b_pos = new Vector3Int((int)bbox.Max.x, (int)bbox.Max.y, (int)bbox.Max.z);
        var ipos = new Vector3Int(
            pos.x > b_pos.x ? ResolutionTree<int>.ToRes((b_pos.x), resolution) : pos.x,
            pos.y > b_pos.y ? ResolutionTree<int>.ToRes((b_pos.y), resolution) : pos.y,
            pos.z > b_pos.z ? ResolutionTree<int>.ToRes((b_pos.z), resolution) : pos.z);
        return ipos;
    }

    /// <summary>
    /// Attempts to 'single fill' the chunk. (If the chunk is completely above or below the height map,
    /// then we dont do any filling, we just mark a flag and are done
    /// </summary>
    /// <param name="c"></param>
    /// <param name="face"></param>
    /// <param name="center"></param>
    /// <returns></returns>
    bool SingleBlockFill(Chunk c, Faces face, Vector3Int center)
    {
        Faces f_null = default(Faces);
        Faces f_solid = default(Faces);
        HeightMap up = null, down = null, left = null,
            right = null, front = null, back = null;
        int null_count = 0;
        int solid_count = 0;
        //only uses first 2 coords of int array
        if (face.up)
        {
            int cmax = Mathf.Max(Mathf.Abs(c.Max.y - center.y), Mathf.Abs(c.Min.y - center.y));
            int cmin = Mathf.Min(Mathf.Abs(c.Max.y - center.y), Mathf.Abs(c.Min.y - center.y));
            var map = generator.up[c.ChunkPos.x, c.ChunkPos.z, c.Resolution];

            //if lowest point on chunk is above highest point on height map..
            if (map.Max <= cmin)
            {
                f_null.up = true; null_count++;
            }
            //if lowest point on map is above highest point on chunk...
            if (map.Min >= cmax)
            {
                f_solid.up = true; solid_count++;
            }
            up = map;
        }
        if (face.down)
        {
            //min is further from the center, so we reverse max and min here
            int cmax = Mathf.Max(Mathf.Abs(c.Max.y - center.y), Mathf.Abs(c.Min.y - center.y));
            int cmin = Mathf.Min(Mathf.Abs(c.Max.y - center.y), Mathf.Abs(c.Min.y - center.y));
            var map = generator.down[c.ChunkPos.x, c.ChunkPos.z, c.Resolution];
            //if lowest point on chunk is above highest point on height map..
            if (map.Max <= cmin)
            {
                f_null.down = true; null_count++;
            }
            //if lowest point on map is above highest point on chunk...
            if (map.Min >= cmax)
            {
                f_solid.down = true; solid_count++;
            }
            down = map;
        }
        if (face.right)
        {
            int cmax = Mathf.Max(Mathf.Abs(c.Max.x - center.x), Mathf.Abs(c.Min.x - center.x));
            int cmin = Mathf.Min(Mathf.Abs(c.Max.x - center.x), Mathf.Abs(c.Min.x - center.x));
            var map = generator.right[c.ChunkPos.y, c.ChunkPos.z, c.Resolution];
            //if lowest point on chunk is above highest point on height map..
            if (map.Max <= cmin)
            {
                f_null.right = true; null_count++;
            }
            //if lowest point on map is above highest point on chunk...
            if (map.Min >= cmax)
            {
                f_solid.right = true; solid_count++;
            }
            right = map;
        }
        if (face.left)
        {
            //min is further from the center, so we reverse max and min here
            int cmax = Mathf.Max(Mathf.Abs(c.Max.x - center.x), Mathf.Abs(c.Min.x - center.x));
            int cmin = Mathf.Min(Mathf.Abs(c.Max.x - center.x), Mathf.Abs(c.Min.x - center.x));
            var map = generator.left[c.ChunkPos.y, c.ChunkPos.z, c.Resolution];
            //if lowest point on chunk is above highest point on height map..
            if (map.Max <= cmin)
            {
                f_null.left = true; null_count++;
            }
            //if lowest point on map is above highest point on chunk...
            if (map.Min >= cmax)
            {
                f_solid.left = true; solid_count++;
            }
            left = map;
        }
        if (face.forward)
        {
            int cmax = Mathf.Max(Mathf.Abs(c.Max.z - center.z), Mathf.Abs(c.Min.z - center.z));
            int cmin = Mathf.Min(Mathf.Abs(c.Max.z - center.z), Mathf.Abs(c.Min.z - center.z));
            var map = generator.forward[c.ChunkPos.x, c.ChunkPos.y, c.Resolution];
            //if lowest point on chunk is above highest point on height map..
            if (map.Max <= cmin)
            {
                f_null.forward = true; null_count++;
            }
            //if lowest point on map is above highest point on chunk...
            if (map.Min >= cmax)
            {
                f_solid.forward = true; solid_count++;
            }
            front = map;
        }
        if (face.back)
        {
            //min is further from the center, so we reverse max and min here
            int cmax = Mathf.Max(Mathf.Abs(c.Max.z - center.z), Mathf.Abs(c.Min.z - center.z));
            int cmin = Mathf.Min(Mathf.Abs(c.Max.z - center.z), Mathf.Abs(c.Min.z - center.z));
            var map = generator.back[c.ChunkPos.x, c.ChunkPos.y, c.Resolution];
            //if lowest point on chunk is above highest point on height map..
            if (map.Max <= cmin)
            {
                f_null.back = true; null_count++;
            }
            //if lowest point on map is above highest point on chunk...
            if (map.Min >= cmax)
            {
                f_solid.back = true; solid_count++;
            }
            back = map;
        }

        //some are above and below, just draw it normallly
        if (null_count != 0 && solid_count != 0)
            return false;

        //if all faces were above
        if (null_count != 0 && Faces.Equal(face, f_null))
        {
            c.SingleBlockID = Block.NULL;
            return true;
        }
        //if all solids were above
        else if (solid_count != 0 && Faces.Equal(face, f_solid))
        {
            //determine if this chunk needs to be filled in precisly using a height map
            bool multi_fill =
                (up == null ? false : up.MultiBiome) ||
                (down == null ? false : down.MultiBiome) ||
                (left == null ? false : left.MultiBiome) ||
                (right == null ? false : right.MultiBiome) ||
                (front == null ? false : front.MultiBiome) ||
                (back == null ? false : back.MultiBiome);
            //if multi_fill (some heightmap needs multiple biome types)
            if (multi_fill)
                return false;
            else
            {
                BiomeSettings single_biome;
                //check to see if all of the heightmaps contain equal biomes
                if (AllSingleBiomeEQUAL(up, down, left, right, front, back, out single_biome))
                {
                    if (single_biome == null)
                        throw new System.Exception("Error, null single biome. Null biomes are not allowed!");
                    if (single_biome.surface == null)
                        throw new System.Exception("Error, null biome surface. Biomes must have a surface!");
                    //if all equal, then we can do a single block fill
                    Voxel v = single_biome.surface.ID;
                    c.SingleBlockID = v;
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    /// <summary>
    /// Determines if all input HeightMap.single_biomes are equal
    /// </summary>
    /// <param name="h1"></param>
    /// <param name="h2"></param>
    /// <param name="h3"></param>
    /// <param name="h4"></param>
    /// <param name="h5"></param>
    /// <param name="h6"></param>
    /// <returns></returns>
    bool AllSingleBiomeEQUAL(HeightMap h1, HeightMap h2, HeightMap h3,
        HeightMap h4, HeightMap h5, HeightMap h6, out BiomeSettings single_biome)
    {
        //*****a comparison with a null biome in this function results in true****

        //get the first non null biome
        BiomeSettings b1 = null, b2 = null, b3 = null, b4 = null, b5 = null, b6 = null;
        BiomeSettings b = (b1 = h1 == null ? null : h1.single_biome);
        if (b == null)
        {
            b = (b2 = h2 == null ? null : h2.single_biome);
            if (b == null)
            {
                b = (b3 = h3 == null ? null : h3.single_biome);
                if (b == null)
                {
                    b = (b4 = h4 == null ? null : h4.single_biome);
                    if (b == null)
                    {
                        b = (b5 = h5 == null ? null : h5.single_biome);
                        if (b == null)
                        {
                            b = (b6 = h6 == null ? null : h6.single_biome);
                        }
                    }
                }
            }
        }
        single_biome = b;
        //if all null, return true because they are all equal
        if (b == null)
            return true;
        //return true if b is equal to all other non null biomes
        return
            (b1 == null ? true : b.Equals(b1)) &&
            (b2 == null ? true : b.Equals(b2)) &&
            (b3 == null ? true : b.Equals(b3)) &&
            (b4 == null ? true : b.Equals(b4)) &&
            (b5 == null ? true : b.Equals(b5)) &&
            (b6 == null ? true : b.Equals(b6));
    }

    /// <summary>
    /// Height map accessor
    /// </summary>
    /// <param name="face"></param>
    /// <param name="pos"></param>
    /// <param name="resolution"></param>
    /// <returns></returns>
    HeightMap GetHeightMap(Faces face, Vector3Int pos, int resolution)
    {
        //2 coordinates are in the order of x > y > z
        //where first is greatest

        //Note** even if a block is multi face, we can just pick any of the available height maps
        //as they will all have the same value

        if (face.up)
            return generator.up[pos.x, pos.z, resolution];
        else if (face.down)
            return generator.down[pos.x, pos.z, resolution];
        else if (face.right)
            return generator.right[pos.y, pos.z, resolution];
        else if (face.left)
            return generator.left[pos.y, pos.z, resolution];
        else if (face.forward)
            return generator.forward[pos.x, pos.y, resolution];
        else
            return generator.back[pos.x, pos.y, resolution];
    }
    /// <summary>
    /// Fills the block at the input position
    /// 'pos' is the local xyz coordinate of the voxel in its own chunk
    /// 'bpos' is the absolute value (resolution 0 ) position of the block in the VoxelGRoup relative to the center ofthe voxel group
    /// </summary>
    /// <param name="face"></param>
    /// <param name="ChunkPos"></param>
    /// <param name="BlockPos"></param>
    /// <returns></returns>
    void FillBlock(Faces face, Vector3Int pos, HeightMap map, Chunk c, Vector3Int bpos)
    {
        //2 coordinates are in the order of x > y > z
        //where first is greatest

        //Note** even if a block is multi face, we can just pick any of the available height maps
        //as they will all have the same value

        if (c.Voxels == null)
            c.Voxels = new int[VoxelGroup.ChunkSize, VoxelGroup.ChunkSize, VoxelGroup.ChunkSize];

        //only uses first 2 coords of int array
        if (face.up)
        {
            var b = map.bmap[pos.x, pos.z].surface;
            int height = map.map[pos.x, pos.z];
            Voxel v = b.ID;
            if (bpos.y <= height)
                c.Voxels[pos.x, pos.y, pos.z] = v;
        }
        else if (face.down)
        {
            int height = map.map[pos.x, pos.z];
            var b = map.bmap[pos.x, pos.z].surface;
            Voxel v = b.ID;
            if (bpos.y <= height)
                c.Voxels[pos.x, pos.y, pos.z] = v;
        }
        else if (face.left)
        {
            int height = map.map[pos.y, pos.z];
            var b = map.bmap[pos.y, pos.z].surface;
            Voxel v = b.ID;
            if (bpos.x <= height)
                c.Voxels[pos.x, pos.y, pos.z] = v;
        }
        else if (face.right)
        {
            int height = map.map[pos.y, pos.z];
            var b = map.bmap[pos.y, pos.z].surface;
            Voxel v = b.ID;
            if (bpos.x <= height)
                c.Voxels[pos.x, pos.y, pos.z] = v;
        }
        else if (face.forward)
        {
            int height = map.map[pos.x, pos.y];
            var b = map.bmap[pos.x, pos.y].surface;
            Voxel v = b.ID;
            if (bpos.z <= height)
                c.Voxels[pos.x, pos.y, pos.z] = v;
        }
        else
        {
            int height = map.map[pos.x, pos.y];
            var b = map.bmap[pos.x, pos.y].surface;
            Voxel v = b.ID;
            if (bpos.z <= height)
                c.Voxels[pos.x, pos.y, pos.z] = v;
        }
    }
    /// <summary>
    /// Make height maps for the input chunk
    /// </summary>
    /// <param name="ChunkPos">Position of chunk relative to VoxelGroup (chunk coords)</param>
    /// <param name="RegionPos"></param>
    Faces MakeHeightMaps(Vector3Int pos, PlanetSettings settings, Chunk c)
    {
        //first get corners of each chunk
        int cSize = c.Size - 1;
        Vector3Int pos1 = pos;
        Vector3Int pos2 = new Vector3Int(pos1.x, pos1.y, pos1.z + cSize);
        Vector3Int pos3 = new Vector3Int(pos1.x, pos1.y + cSize, pos1.z);
        Vector3Int pos4 = new Vector3Int(pos1.x, pos1.y + cSize, pos1.z + cSize);
        Vector3Int pos5 = new Vector3Int(pos1.x + cSize, pos1.y, pos1.z);
        Vector3Int pos6 = new Vector3Int(pos1.x + cSize, pos1.y, pos1.z + cSize);
        Vector3Int pos7 = new Vector3Int(pos1.x + cSize, pos1.y + cSize, pos1.z);
        Vector3Int pos8 = new Vector3Int(pos1.x + cSize, pos1.y + cSize, pos1.z + cSize);
        //test each corner to see what side of the chunk group it is on
        var v = Parent.Face(pos1);
        v = Faces.OR(v, Parent.Face(pos2));
        v = Faces.OR(v, Parent.Face(pos3));
        v = Faces.OR(v, Parent.Face(pos4));
        v = Faces.OR(v, Parent.Face(pos5));
        v = Faces.OR(v, Parent.Face(pos6));
        v = Faces.OR(v, Parent.Face(pos7));
        v = Faces.OR(v, Parent.Face(pos8));
        //now that we know which side of the VoxelGroup this chunk is on, we can make a height map to use for it
        GenCubeHeightMap(v, pos1, settings, c);
        return v;
    }

    bool MultipleFaces(Faces faces)
    {
        return ((faces.forward ? 1 : 0) + (faces.back ? 1 : 0) + (faces.right ? 1 : 0) +
            (faces.left ? 1 : 0) + (faces.up ? 1 : 0) + (faces.down ? 1 : 0)) > 1;
    }
    /// <summary>
    /// Make a heighmap for the input chunk if it doesn't exist yet
    /// </summary>
    /// <param name="f"></param>
    /// <param name="ChunkPos">Position of chunk relative to VoxelGroup(block coordinates)</param>
    void GenCubeHeightMap(Faces f, Vector3Int pos, PlanetSettings settings, Chunk c)
    {
        int cSize = c.Size;
        //if top face
        if (f.up)
        {
            //if we havent already computed this height map....
            if (!generator.up.Contains(new Vector2Int(pos.x, pos.z), c.Resolution))
            {
                Vector3Int min = new Vector3Int(pos.x, Parent.VoxelGroupSize.y, pos.z);
                Vector3Int max = new Vector3Int(pos.x + cSize, Parent.VoxelGroupSize.y, pos.z + cSize);
                MakeHeightMap(min, max, settings, Vector3Int.up, c);
            }
        }
        //if bottom face
        if (f.down)
        {
            //if we havent already computed this height map....
            if (!generator.down.Contains(new Vector2Int(pos.x, pos.z), c.Resolution))
            {
                Vector3Int min = new Vector3Int(pos.x, 0, pos.z);
                Vector3Int max = new Vector3Int(pos.x + cSize, 0, pos.z + cSize);
                MakeHeightMap(min, max, settings, Vector3Int.down, c);
            }
        }
        //if right face
        if (f.right)
        {
            //if we havent already computed this height map....
            if (!generator.right.Contains(new Vector2Int(pos.y, pos.z), c.Resolution))
            {
                Vector3Int min = new Vector3Int(Parent.VoxelGroupSize.x, pos.y, pos.z);
                Vector3Int max = new Vector3Int(Parent.VoxelGroupSize.x, pos.y + cSize, pos.z + cSize);
                MakeHeightMap(min, max, settings, Vector3Int.right, c);
            }
        }
        //if left face
        if (f.left)
        {
            //if we havent already computed this height map....
            if (!generator.left.Contains(new Vector2Int(pos.y, pos.z), c.Resolution))
            {
                Vector3Int min = new Vector3Int(0, pos.y, pos.z);
                Vector3Int max = new Vector3Int(0, pos.y + cSize, pos.z + cSize);
                MakeHeightMap(min, max, settings, Vector3Int.left, c);
            }
        }
        //if front
        if (f.forward)
        {
            //if we havent already computed this height map....
            if (!generator.forward.Contains(new Vector2Int(pos.x, pos.y), c.Resolution))
            {
                Vector3Int min = new Vector3Int(pos.x, pos.y, Parent.VoxelGroupSize.z);
                Vector3Int max = new Vector3Int(pos.x + cSize, pos.y + cSize, Parent.VoxelGroupSize.z);
                MakeHeightMap(min, max, settings, Vector3Int.forward, c);
            }
        }
        //if back
        if (f.back)
        {
            //if we havent already computed this height map....
            if (!generator.back.Contains(new Vector2Int(pos.x, pos.y), c.Resolution))
            {
                Vector3Int min = new Vector3Int(pos.x, pos.y, 0);
                Vector3Int max = new Vector3Int(pos.x + cSize, pos.y + cSize, 0);
                MakeHeightMap(min, max, settings, Vector3Int.back, c);
            }
        }
    }
    /// <summary>
    /// Make a 2D height map. Values are extracted from the 2D box formed by min to max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <param name="settings"></param>
    /// <returns></returns>
    void MakeHeightMap(Vector3Int min, Vector3Int max, PlanetSettings settings, Vector3Int up, Chunk c)
    {
        //clamp to bounds of the world (inclusive, so parent.VoxelGroupSize.x is out of bounds but we allow it)
        min = Vector3Int.VectorClamp(min, Vector3Int.zero, Parent.VoxelGroupSize);
        max = Vector3Int.VectorClamp(max, Vector3Int.zero, Parent.VoxelGroupSize);

        //allocate height map
        HeightMap map = new HeightMap();

        //get base level, all blocks will be base_level height + noise value
        //we multiply by x because x == y == z so it doesn't matter (planets are a cube)
        int base_level = (int)(settings.BaseLevel * Parent.VoxelGroupSize.x / 2);

        //we change the bounary to wrap around at the base level
        if (up.x != 0)
        {
            min.x = base_level; max.x = base_level;
            if (up.x < 0) { min.x = -base_level; max.x = -base_level; }
        }
        else if (up.y != 0)
        {
            min.y = base_level; max.y = base_level;
            if (up.y < 0) { min.y = -base_level; max.y = -base_level; }
        }
        else
        {
            min.z = base_level; max.z = base_level;
            if (up.z < 0) { min.z = -base_level; max.z = -base_level; }
        }

        //bbox that surrounds the planet
        BBox bbox = new BBox((Vector3d)(Parent.VoxelGroupCenter - new Vector3Int(base_level, base_level, base_level)),
            (Vector3d)(new Vector3Int(base_level, base_level, base_level) * 2));

        //three loops here but for one of the values n, n==n so its like having 2 for loops (if input data is correct)
        int i = 0, j = 0;
        for (int x = min.x; x <= max.x; x++)
        {
            for (int y = min.y; y <= max.y; y++)
            {
                for (int z = min.z; z <= max.z; z++)
                {
                    //get i,j
                    if (up.x != 0) { i = y - min.y; j = z - min.z; }
                    else if (up.y != 0) { i = x - min.x; j = z - min.z; }
                    else { i = x - min.x; j = y - min.y; }

                    //we iterate + 1 over, so we do a bounds check
                    if (i == VoxelGroup.ChunkSize || j == VoxelGroup.ChunkSize)
                        continue;

                    //compute height and assign to map at (i,j)
                    var height = ComputeHeight(new Vector3d(x, y, z), settings, base_level, bbox, up);
                    map.Set(i, j, height.height, height.biome);
                }
            }
        }
        //assign
        if (up.x != 0)
        {
            if (up.x > 0)
                generator.right[min.y, min.z, c.Resolution] = map;
            else
                generator.left[min.y, min.z, c.Resolution] = map;
        }
        else if (up.y != 0)
        {
            if (up.y > 0)
                generator.up[min.x, min.z, c.Resolution] = map;
            else
                generator.down[min.x, min.z, c.Resolution] = map;
        }
        else
        {
            if (up.z > 0)
                generator.forward[min.x, min.y, c.Resolution] = map;
            else
                generator.back[min.x, min.y, c.Resolution] = map;
        }
    }

    ///// <summary>
    ///// Compute the height of an individual position
    ///// </summary>
    ///// <param name="pos"></param>
    ///// <param name="settings"></param>
    ///// <param name="base_level"></param>
    ///// <returns></returns>
    //HeightResult ComputeHeight(Vector2d pos, PlanetSettings settings, int base_level)
    //{
    //    //compute noise values
    //    var height = (generator.NoiseObj.pnoise2(pos.x / 16d, pos.y / 16d, 16, 16) + 1) * 0.5;
    //    var temperature = (generator.NoiseObj.pnoise2((pos.x + 43983) / 16d, (pos.y + 1034) / 16d, 16, 16) + 1) * 0.5;
    //    var humidity = (generator.NoiseObj.pnoise2((pos.x + 2034) / 16d, (pos.y + 27823) / 16d, 16, 16) + 1) * 0.5;

    //    //make result and return it
    //    HeightResult result = new HeightResult();
    //    result.height = base_level + (int)(height*50);
    //    result.biome = settings.GetBiome(temperature, humidity);
    //    return result;
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="settings"></param>
    /// <param name="base_level"></param>
    /// <param name="baseBox"></param>
    /// <param name="up"></param>
    /// <returns></returns>
    HeightResult ComputeHeight(Vector3d pos, PlanetSettings settings,
        int base_level, BBox baseBox, Vector3Int up)
    {
        //params
        float Height_Scale = 10;
        float edge_scale = 0.75f;

        //compute noise values
        var height = (generator.NoiseObj.noise3(pos.x / VoxelGroup.ChunkSize,
            pos.y / VoxelGroup.ChunkSize, pos.z / VoxelGroup.ChunkSize) + 1) * 0.5;
        var temperature = (generator.NoiseObj.noise3((pos.x + 43983) / VoxelGroup.ChunkSize,
            (pos.y + 1034) / VoxelGroup.ChunkSize, (pos.z + 9873) / VoxelGroup.ChunkSize) + 1) * 0.5;
        var humidity = (generator.NoiseObj.noise3((pos.x + 2034) / VoxelGroup.ChunkSize,
            (pos.y + 27823) / VoxelGroup.ChunkSize, (pos.z + 37521) / VoxelGroup.ChunkSize) + 1) * 0.5;

        //modify height
        int addHeight = (int)(height * Height_Scale);

        //cap height at ceiling
        if (up.x != 0)
        {
            if (pos.y >= baseBox.Max.y || pos.y <= baseBox.Min.y ||
                pos.z >= baseBox.Max.z || pos.z <= baseBox.Min.z)
                addHeight = 0;
            else
            {
                float ceiling = (float)(Math.Min(Math.Min(baseBox.Max.y - pos.y, pos.y - baseBox.Min.y),
                                  Math.Min(baseBox.Max.z - pos.z, pos.z - baseBox.Min.z)) * edge_scale);
                float c_pri = ceiling > Height_Scale ? 0 : (1 - ceiling / Height_Scale);
                addHeight = (int)(addHeight * (1 - c_pri) + ceiling * c_pri);
            }
        }
        else if (up.y != 0)
        {
            if (pos.x >= baseBox.Max.x || pos.x <= baseBox.Min.x ||
                pos.z >= baseBox.Max.z || pos.z <= baseBox.Min.z)
                addHeight = 0;
            else
            {
                float ceiling = (float)(Math.Min(Math.Min(baseBox.Max.x - pos.x, pos.x - baseBox.Min.x),
                                  Math.Min(baseBox.Max.z - pos.z, pos.z - baseBox.Min.z)) * edge_scale);
                float c_pri = ceiling > Height_Scale ? 0 : (1 - ceiling / Height_Scale);
                addHeight = (int)(addHeight * (1 - c_pri) + ceiling * c_pri);
            }
        }
        else
        {
            if (pos.x >= baseBox.Max.x || pos.x <= baseBox.Min.x ||
                pos.y >= baseBox.Max.y || pos.y <= baseBox.Min.y)
                addHeight = 0;
            else
            {
                float ceiling = (float)(Math.Min(Math.Min(baseBox.Max.x - pos.x, pos.x - baseBox.Min.x),
                                  Math.Min(baseBox.Max.y - pos.y, pos.y - baseBox.Min.y)) * edge_scale);
                float c_pri = ceiling > Height_Scale ? 0 : (1 - ceiling / Height_Scale);
                addHeight = (int)(addHeight * (1 - c_pri) + ceiling * c_pri);
            }
        }

        //make result and return it
        HeightResult result = new HeightResult();
        result.height = base_level + addHeight;
        result.biome = settings.GetBiome(temperature, humidity);
        return result;
    }

    ///// <summary>
    ///// Takes an input position that is assumed to be on the surface of the input cube.
    ///// Function will error if not
    ///// </summary>
    ///// <param name="position"></param>
    ///// <param name="cube"></param>
    ///// <returns></returns>
    //Vector2d CubeSurfacePointTo2DMapping(Vector3d position, BBox cube)
    //{
    //    //convert input position to represent a value between (-1,-1,-1) and (1,1,1)
    //    Vector3d translate = -cube.Center;
    //    position = position + translate;
    //    position = new Vector3d(position.x / cube.Size.x * 2, position.y / cube.Size.y * 2, position.z / cube.Size.z * 2);
    //    //get position on sphere
    //    Vector3d spherepos = CubeToSpherePosition3D(position);
    //    //convert to cartesian
    //    return Sphere3DToLatLon(spherepos, 1);
    //}

    ///// <summary>
    ///// Cartesian coordinates are in radians
    ///// </summary>
    ///// <param name="position"></param>
    ///// <param name="radius"></param>
    ///// <returns></returns>
    //Vector2d Sphere3DToLatLon(Vector3d position, double radius)
    //{
    //    double latitude = 90 - Math.Acos(position.y / radius) * 180 / Math.PI;
    //    double longitude = Math.Atan2(position.z, position.x) * 180 / Math.PI;
    //    return new Vector2d(latitude, longitude);
    //}

    ///// <summary>
    ///// Inputs a position from (-1,-1,-1) to (1,1,1) and outputs a position on a unit sphere
    ///// </summary>
    ///// <param name="pos"></param>
    ///// <returns></returns>
    //Vector3d CubeToSpherePosition3D(Vector3d normalized_position)
    //{
    //    //interesting read
    //    //http://mathproofs.blogspot.com/2005/07/mapping-cube-to-sphere.html

    //    double x = normalized_position.x;
    //    double y = normalized_position.y;
    //    double z = normalized_position.z;
    //    double y2 = y * y;
    //    double x2 = x * x;
    //    double z2 = z * z;
    //    return new Vector3d
    //        (
    //        x * Math.Sqrt(1 - (y2 / 2) - (z2 / 2) - ((y2 * z2) / 3)),
    //        y * Math.Sqrt(1 - (z2 / 2) - (x2 / 2) - ((z2 * x2) / 3)),
    //        z * Math.Sqrt(1 - (x2 / 2) - (y2 / 2) - ((x2 * y2) / 3))
    //        );
    //}

    /// <summary>
    /// Chunk Generation object holder
    /// </summary>
    class Generator
    {
        /// <summary>
        /// Height map for top face
        /// </summary>
        public ResolutionTree2d<HeightMap> up;
        /// <summary>
        /// height map for bottom face
        /// </summary>
        public ResolutionTree2d<HeightMap> down;
        /// <summary>
        /// height map for right face
        /// </summary>
        public ResolutionTree2d<HeightMap> right;
        /// <summary>
        /// height map for left face
        /// </summary>
        public ResolutionTree2d<HeightMap> left;
        /// <summary>
        /// height map for forward face
        /// </summary>
        public ResolutionTree2d<HeightMap> forward;
        /// <summary>
        /// height map for back face
        /// </summary>
        public ResolutionTree2d<HeightMap> back;
        /// <summary>
        /// Noise object
        /// </summary>
        public Noise NoiseObj;
    }

    /// <summary>
    /// Height map
    /// </summary>
    class HeightMap
    {
        public HeightMap()
        {
            map = new int[VoxelGroup.ChunkSize, VoxelGroup.ChunkSize];
            bmap = new BiomeSettings[VoxelGroup.ChunkSize, VoxelGroup.ChunkSize];
        }
        /// <summary>
        /// height map
        /// </summary>
        public int[,] map;
        /// <summary>
        /// biome map
        /// </summary>
        public BiomeSettings[,] bmap;
        /// <summary>
        /// multiple biomes exist in height map?
        /// </summary>
        public bool MultiBiome = false;
        /// <summary>
        /// Biome for this height map if only one biome exists on this height map.
        /// This value is only valid if 'MultiBiome' == false
        /// </summary>
        public BiomeSettings single_biome;
        /// <summary>
        /// Set HeightMap parameters. Only use this function to modify this object.
        /// Modifying fiels directly will cause undefined behaviour.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="val"></param>
        /// <param name="b"></param>
        public void Set(int x, int y, int val, BiomeSettings b)
        {
            //assign values
            map[x, y] = val;
            bmap[x, y] = b;

            //keep track of min and max
            if (val > Max)
                Max = val;
            if (val < Min)
                Min = val;

            //keep track if this is a multi biome heightmap
            if (MultiBiome == false && !ReferenceEquals(single_biome, null))
                MultiBiome = !single_biome.Equals(b);
            single_biome = b;
        }
        public int Max = int.MinValue;
        public int Min = int.MaxValue;
    }

    /// <summary>
    /// Used to return result from 'ComputeHeight' function
    /// </summary>
    struct HeightResult
    {
        public int height;
        public BiomeSettings biome;
    }
}
