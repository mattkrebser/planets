﻿using UnityEngine;
using System.Collections.Generic;
using System;

using Voxel = System.Int32;

public partial class VoxelGroup
{
    /// <summary>
    /// Voxel data holder class.
    /// </summary>
    public partial class Chunk : IEquatable<Chunk>
    {
        /// <summary>
        /// VoxelGroup parent
        /// </summary>
        public VoxelGroup Parent
        {
            get
            {
                return parent;
            }
        }
        VoxelGroup parent = null;

        /// <summary>
        /// Position of this chunk inside the chunk group in blocks (resolution 0)
        /// </summary>
        public Vector3Int ChunkPos
        {
            get
            {
                return cpos;
            }
        }
        Vector3Int cpos = new Vector3Int(-1, -1, -1);
        /// <summary>
        /// Min==ChunkPos, minimum voxel coordinate in chunk (resolution 0) coordinates
        /// </summary>
        public Vector3Int Min
        {
            get
            {
                return cpos;
            }
        }
        /// <summary>
        /// Maximum voxel coordinate in the chunk. (resolution 0) coordinates
        /// </summary>
        public Vector3Int Max
        {
            get
            {
                return new Vector3Int(cpos.x + size, cpos.y + size, cpos.z + size);
            }
        }

        /// <summary>
        /// Size (in blocks, resolution 0) of the chunk. (All chunks are cubes)
        /// </summary>
        public int Size
        {
            get
            {
                return size;
            }
        }
        int size = -1;

        /// <summary>
        /// The length of a voxel. (Resolution 0)
        /// </summary>
        public int VoxelLength
        {
            get
            {
                return ResolutionTree<Chunk>.ToRes(1, Resolution, 0);
            }
        }

        /// <summary>
        /// Hold mesh information and can draw the chunk
        /// </summary>
        public ChunkDrawer Drawer
        {
            get
            {
                return _drawer;
            }
        }
        ChunkDrawer _drawer;

        public Chunk(VoxelGroup Parent)
        {
            if (ReferenceEquals(Parent, null))
                throw new System.Exception("Error, spawned chunk with no parent.");
            parent = Parent;
            _drawer = new ChunkDrawer(this);
        }

        /// <summary>
        /// Set a chunk's position and resolution (Position is always resolution of (0))
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="Resolution"></param>
        public void Set(Vector3Int Position, int Resolution)
        {
            cpos = Position;
            resolution = Resolution;
            size = ResolutionTree<Chunk>.ToRes(ChunkSize, Resolution, 0);
        }

        /// <summary>
        /// Resolution if this chunk. A resolution of '8' means that this chunk represents 2^8^3 blocks
        /// </summary>
        public int Resolution
        {
            get
            {
                return resolution;
            }

        }
        int resolution = -1;

        /// <summary>
        /// Single block ID, if this chunk is only composed of a single block, then this will not == Block.INVALID
        /// </summary>
        public Voxel SingleBlockID = Block.INVALID;

        /// <summary>
        /// Returns true if this chunk is only composed of a single voxel type
        /// </summary>
        public bool IsSingleBlockChunk
        {
            get
            {
                return SingleBlockID != Block.INVALID;
            }
        }

        /// <summary>
        /// Invalid/Unitialized. If true, then this chunk likely has been created but has not had
        /// any voxel data added to it.
        /// </summary>
        public bool IsInvalid
        {
            get
            {
                return SingleBlockID == Block.INVALID && Voxels == null;
            }
        }

        /// <summary>
        /// SOlid chunk? (Made up of all solid voxels with no air blocks)
        /// Solid implies drawable, not collideable
        /// </summary>
        public bool SolidChunk
        {
            get
            {
                return SingleBlockID != Block.INVALID &&
                    BlockLibrary.Blocks[SingleBlockID].Drawable;
            }
        }
        /// <summary>
        /// Air chunk? (only composed of NULL type blocks)
        /// </summary>
        public bool AirChunk
        {
            get
            {
                return SingleBlockID == Block.NULL;
            }
        }

        /// <summary>
        /// Voxel data. Smooth blocks are the first 12 bits, Any other block type occupies the last 20 bits
        /// </summary>
        public Voxel[,,] Voxels;

        /// <summary>
        /// last accessed chunk cache so we dont have to keep searching the tree
        /// (typically the same chunk is accessed many times over, so we cache a result)
        /// </summary>
        Chunk last_access = null;
        /// <summary>
        /// Access chunk data. (Resolution 0) input coordinates. This accessor will also return voxels
        /// from different chunks if input coordinates are out of bounds.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public Voxel this[int x, int y, int z]
        {
            get
            {
                //if inside this chunk
                if (InBounds(x, y, z))
                {
                    //return voxel
                    return SingleBlockID != Block.INVALID ? SingleBlockID :
                        Voxels[(x - cpos.x) >>Resolution, (y - cpos.y) >>Resolution, (z - cpos.z) >>Resolution];
                }
                //if last cached access is null or the input coordinate is not inside it
                if (last_access == null || !last_access.InBounds(x, y, z))
                {
                    //cache new chunk
                    last_access = Parent.VoxelData[x, y, z, Resolution];
                    //if no chunk existed, then return null
                    if (last_access == null)
                        return Block.NULL;
                }
                //if we get here, then last_access contains the input point, so return the voxel
                return last_access.SingleBlockID != Block.INVALID ? last_access.SingleBlockID :
                    last_access.Voxels
                [
                    (x - last_access.cpos.x) >> last_access.Resolution,
                    (y - last_access.cpos.y) >> last_access.Resolution,
                    (z - last_access.cpos.z) >> last_access.Resolution
                ];
            }
            set
            {
                //no bounds checking for setting
                Voxels[x, y, z] = value;
            }
        }
      
        /// <summary>
        /// Access voxel data without doing bounds check. Input position is a resolution '0'
        /// global coordinate. Also 'this' chunk is assumed to not be a single chunk block.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public Voxel UnboundedGetVoxel(int x, int y, int z)
        {
            return Voxels[(x - cpos.x) >> Resolution, (y - cpos.y) >> Resolution, (z - cpos.z) >> Resolution];
        }

        /// <summary>
        /// Access voxel data without doing bounds check. Input position is a resolution '0'
        /// global coordinate.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public Voxel CheckedGetVoxel(int x, int y, int z)
        {
            return SingleBlockID != Block.INVALID ? SingleBlockID : 
                Voxels[(x - cpos.x) >> Resolution, (y - cpos.y) >> Resolution, (z - cpos.z) >> Resolution];
        }

        /// <summary>
        /// get block object from voxel
        /// </summary>
        /// <param name="voxel"></param>
        /// <returns></returns>
        public static Block block(Voxel voxel)
        {
            return BlockLibrary.Blocks[voxel];
        }

        /// <summary>
        /// Chunk directly above this one. Could be a different resolution or null.
        /// y+ direction
        /// </summary>
        public Chunk Up
        {
            get
            {
                return Parent.VoxelData[cpos.x, cpos.y + size, cpos.z, Resolution];
            }
        }
        /// <summary>
        /// Neighbor chunk. Could be a different resolution or null. y- direction
        /// </summary>
        public Chunk Down
        {
            get
            {
                return Parent.VoxelData[cpos.x, cpos.y - 1, cpos.z, Resolution];
            }
        }
        /// <summary>
        /// Neighbor chunk. Could be a different resolution or null. x+ direction
        /// </summary>
        public Chunk Right
        {
            get
            {
                return Parent.VoxelData[cpos.x + size, cpos.y, cpos.z, Resolution];
            }
        }
        /// <summary>
        /// Neighbor chunk. Could be a different resolution or null. x- direction
        /// </summary>
        public Chunk Left
        {
            get
            {
                return Parent.VoxelData[cpos.x - 1, cpos.y, cpos.z, Resolution];
            }
        }
        /// <summary>
        /// Neighbor chunk. Could be a different resolution or null. z+ direction
        /// </summary>
        public Chunk Forward
        {
            get
            {
                return Parent.VoxelData[cpos.x, cpos.y, cpos.z + size, Resolution];
            }
        }
        /// <summary>
        /// Neighbor chunk. Could be a different resolution or null. z- direction
        /// </summary>
        public Chunk Back
        {
            get
            {
                return Parent.VoxelData[cpos.x, cpos.y, cpos.z - 1, Resolution];
            }
        }

        /// <summary>
        /// Input (resolution 0) coordinates in bounds?
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public bool InBounds(int x, int y, int z)
        {
            if (x < cpos.x || x >= (cpos.x + size) || y < cpos.y ||
                y >= (cpos.y + size) || z < cpos.z || z >= (cpos.z + size))
                return false;
            return true;
        }

        public bool Equals(Chunk c)
        {
            return c == null ? false : ChunkPos == c.ChunkPos && Parent == c.Parent;
        }
        public override bool Equals(object obj)
        {
            return obj is Chunk ? Equals((Chunk)obj) : false;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 29;
                hash = hash * 17 + ChunkPos.GetHashCode();
                hash = hash * 17 + (Parent == null ? 0 : Parent.GetHashCode());
                return hash;
            }
        }
        public static bool operator==(Chunk c1, Chunk c2)
        {
            if (ReferenceEquals(c1, null))
            {
                return (ReferenceEquals(c2, null));
            }
            else
                return c1.Equals(c2);
        }
        public static bool operator!=(Chunk c1, Chunk c2)
        {
            return !(c1 == c2);
        }

        /// <summary>
        /// Clears voxel data, parent reference, and all other information
        /// </summary>
        public void Reset()
        {
            if (Parent != null)
                ClearVoxels();
            parent = null;
            cpos = new Vector3Int(-1, -1, -1);
            resolution = -1;
            size = -1;
            SingleBlockID = Block.INVALID;
        }
        /// <summary>
        /// Clears voxel information. Deletes associated voxel mesh.
        /// </summary>
        public void ClearVoxels()
        {
            int NULL = Block.NULL;
            int cpos = ChunkSize;
            SingleBlockID = Block.INVALID;
            if (Voxels != null)
                for (int x = 0; x < cpos; x++)
                    for (int y = 0; y < cpos; y++)
                        for (int z = 0; z < cpos; z++)
                            Voxels[x, y, z] = NULL;
            if (Drawer != null)
                Drawer.DeleteMesh();
        }

        public partial class ChunkDrawer
        {

        }

        /// <summary>
        /// Struct representing a pair of blocks
        /// </summary>
        public struct BlockPair
        {
            public Block Cube;
            public Block Smooth;
            public BlockPair(Block c, Block s)
            {
                Cube = c; Smooth = s;
            }
        }
    }
}
