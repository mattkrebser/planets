﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Voxel Mesh: Unity component that organized and interacts with unity behaviours to draw a mesh for a given chunk
/// </summary>
public class VoxelMesh : MonoBehaviour
{

    /// <summary>
    /// Returns a newly created voxel mesh object
    /// </summary>
    public static VoxelMesh NewVoxelMesh(Vector3[] verts, int[] tris, Material mat, Vector2[] uvs, VoxelGroup parent)
    {
        GameObject o = new GameObject(".");

        if (parent.UnityObject == null)
            parent.CreatePivot();

        o.transform.parent = parent.UnityObjectChunkParent.transform;
        o.transform.localPosition = Vector3.zero;
        o.transform.localScale = Vector3.one;
        o.transform.rotation = Quaternion.identity;

        var mr = o.AddComponent<MeshRenderer>();
        var mf = o.AddComponent<MeshFilter>();
        var vm = o.AddComponent<VoxelMesh>();
        mr.material = mat;
        mf.mesh.vertices = verts;
        mf.mesh.triangles = tris;
        if (uvs != null)
            mf.mesh.uv = uvs;
        mf.mesh.RecalculateNormals();
        return vm;
    }

    /// <summary>
    /// Create and return arbitrary mesh object
    /// </summary>
    /// <param name="verts"></param>
    /// <param name="tris"></param>
    /// <param name="mat"></param>
    /// <param name="uvs"></param>
    /// <returns></returns>
    public static GameObject NewMesh(Vector3[] verts, int[] tris, Material mat, Vector2[] uvs)
    {
        GameObject o = new GameObject("New Mesh Object");

        o.transform.localPosition = Vector3.zero;
        o.transform.localScale = Vector3.one;
        o.transform.rotation = Quaternion.identity;

        var mr = o.AddComponent<MeshRenderer>();
        var mf = o.AddComponent<MeshFilter>();
        mr.material = mat;
        mf.mesh.vertices = verts;
        mf.mesh.triangles = tris;
        if (uvs != null)
            mf.mesh.uv = uvs;
        mf.mesh.RecalculateNormals();
        return o;
    }
	
}
