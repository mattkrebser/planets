﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SpaceGen
{
    public void GenSolarSystems(GenSettings settings, BBoxQuadTree<SolarSystem> systems)
    {
        System.Random f_r;
        //create fixed systems (NOTE a fixed system may not be created if it intersects another fixed system)
        for (int i = 0; i < settings.SystemTypes.Count; i++)
        {
            if (settings.SystemTypes[i].FixedSystem)
            {
                SolarSystemSettings system = settings.SystemTypes[i];
                f_r = new System.Random(system.FixedSeed);
                Vector3d min = new Vector3d(system.MinSystemSize, system.MinSystemSize, system.MinSystemSize);
                Vector3d max = new Vector3d(system.MaxSystemSize, system.MaxSystemSize, system.MaxSystemSize);
                BBox system_bbox = rand_bbox(min, max, settings.VoxelSpace, f_r);
                system_bbox.Center = system.FixedPosition;
                //if no bboxes intersect
                HashSet<SolarSystem> isystem;
                if ((isystem = systems.Intersects(system_bbox)).Count == 0)
                {
                    //make new system and add to tree
                    SolarSystem new_system = RandSystem(system, f_r, systems);
                    new_system.SolarSystemType = i;
                    new_system.type_name = system.gameObject.name;
                    systems.Add(new_system, system_bbox);
                }
                //release hashset back inot bbox tree pool
                systems.Free(isystem);
            }
        }

        //make random number generator with settings seed
        System.Random r = new System.Random(settings.seed);
        //get a list of non fixed systems (random systems)
        var sys_types = settings.SystemTypes.Where(x => !x.FixedSystem).ToList();

        if (sys_types.Count != 0)
            //create random systems
            for (int i = 0; i < settings.MaxNumberOfSystems; i++)
            {
                int rand_index = 0;
                SolarSystemSettings system = sys_types[rand_index=r.Next(0, sys_types.Count)];
                Vector3d min = new Vector3d(system.MinSystemSize, system.MinSystemSize, system.MinSystemSize);
                Vector3d max = new Vector3d(system.MaxSystemSize, system.MaxSystemSize, system.MaxSystemSize);
                BBox system_bbox = rand_bbox(min, max, settings.VoxelSpace, r);
                //if no bboxes intersect
                HashSet<SolarSystem> isystem;
                if ((isystem = systems.Intersects(system_bbox)).Count == 0)
                {
                    //make new system and add to tree
                    SolarSystem new_system = RandSystem(system, r, systems);
                    new_system.type_name = system.gameObject.name;
                    new_system.SolarSystemType = rand_index;
                    systems.Add(new_system, system_bbox);
                }
                //release hashset back inot bbox tree pool
                systems.Free(isystem);
            }
    }

    /// <summary>
    /// Returns a random bbox that falls within the input parameters
    /// </summary>
    /// <param name="min_size"></param>
    /// <param name="max_size"></param>
    /// <param name="bounding_space"></param>
    /// <param name="r"></param>
    /// <returns></returns>
    BBox rand_bbox(Vector3d min_size, Vector3d max_size, BBox bounding_space, System.Random r)
    {
        Vector3d size = Vector3d.zero;
        size.x = (float)r.NextDouble() * (max_size.x - min_size.x) + min_size.x;
        size.y = (float)r.NextDouble() * (max_size.y - min_size.y) + min_size.y;
        size.z = (float)r.NextDouble() * (max_size.z - min_size.z) + min_size.z;
        Vector3d pos = Vector3d.zero;
        pos.x = (float)r.NextDouble() * (bounding_space.Size.x-size.x) + bounding_space.Position.x;
        pos.y = (float)r.NextDouble() * (bounding_space.Size.y-size.y) + bounding_space.Position.y;
        pos.z = (float)r.NextDouble() * (bounding_space.Size.z-size.z) + bounding_space.Position.z;
        return new BBox(pos, size);
    }

    /// <summary>
    /// Returns a random solar system with a unique ID not yet existing on the input btree
    /// </summary>
    /// <param name="s"></param>
    /// <param name="r"></param>
    /// <param name="btree"></param>
    /// <returns></returns>
    SolarSystem RandSystem(SolarSystemSettings s, System.Random r, BBoxQuadTree<SolarSystem> btree)
    {
        SolarSystem new_s = new SolarSystem();
        new_s.ID = r.Next(0, int.MaxValue);
        while (btree.Contains(new_s))
            new_s.ID = r.Next(0, int.MaxValue);
        return new_s;
    }
}

