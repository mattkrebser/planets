﻿using UnityEngine;
using System.Collections.Generic;
using System;

[System.Serializable]
public class PlanetSettings : MonoBehaviour
{
    [Tooltip("The actual planet will be at most 1/3 of the max in bocks. 2/3 of the planet will account for gravity and atmosphere")]
    [Range(100, 20000)]
    public int MaxPlanetSize;
    [Tooltip("The actual planet will be at most 1/3 of the max in bocks. 2/3 of the planet will account for gravity and atmosphere")]
    [Range(100, 20000)]
    public int MinPlanetSize;
    [Tooltip("This base level for non empty voxels to start at as a percentage of the planet diamter."+
        " A value of 0.8 will allow from them center to 80% of the planet radius to be solid voxels")]
    [Range(0.5f, 1)]
    public double BaseLevel = 0.8d;

    [SerializeField]
    [Tooltip("Biome map of this planet")]
    public BiomeMap Map;

    [Tooltip("If true, this planet will always exist in this solar system type.")]
    public bool FixedPlanet;
    [Tooltip("Local fixed position of the planet. This field is only valid if 'FixedPlanet'=true. NOTE*** Intersecting fixed planets cannot exist. Position is the center of the bbox.")]
    public Vector3d FixedPosition;
    [Tooltip("An extra seed that is used if this is a fixed planet")]
    public int FixedSeed = 0;

    Dictionary<Color32, BiomeSettings> biomes;
    /// <summary>
    /// Get biome at the input x and y. Input x and y are normalized seperatly to be between 0 and 1
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public BiomeSettings GetBiome(double x, double y)
    {
        //initialize if not initialized
        if (biomes == null)
        {
            biomes = new Dictionary<Color32, BiomeSettings>();
            foreach (var pair in Map.Biomes)
                biomes.Add(pair.color, pair.biome);
        }
        //return biome
        var color = Map.ColorAt(x, y);
        return biomes[color];
    }
}

[System.Serializable]
public class Planet : ChunkParent, IEquatable<Planet>
{
    /// <summary>
    /// Trasnform of this planet. Position is relative to solar system. This point represents
    /// the center of the planet
    /// </summary>
    public override Transformd Transform
    {
        get
        {
            return _transform;
        }
    }
    Transformd _transform;

    /// <summary>
    /// A human readable type name for this planet.
    /// </summary>
    public string type_name;

    /// <summary>
    /// BBox surrounding the planet and its atmosphere
    /// </summary>
    public BBox PlanetBBox
    {
        get
        {
            return vbbox;
        }
    }
    BBox abbox;
    /// <summary>
    /// BBox for voxel space of planet
    /// </summary>
    public override BBox bbox
    {
        get
        {
            return vbbox;
        }
    }
    BBox vbbox;
    /// <summary>
    /// Planet type number. This refers to the key in the SolarSystemSettings.PlanetTypes list
    /// </summary>
    public int PlanetType
    {
        get
        {
            return ptype;
        }
    }
    int ptype;
    /// <summary>
    /// A unique ID for each planet in a solar system. 
    /// </summary>
    public override int ID
    {
        get
        {
            return pid;
        }
    }
    int pid;
    /// <summary>
    /// Solar system this planet is in
    /// </summary>
    public override SolarSystem ParentSystem
    {
        get
        {
            return parnt;
        }
    }
    SolarSystem parnt;

    /// <summary>
    /// Planet voxel creator object
    /// </summary>
    public override VoxelGroup.VoxelMaker VoxelCreator { get { return _vc; } }
    VoxelGroup.VoxelMaker _vc;

    /// <summary>
    /// Returns planet settings for this planet
    /// </summary>
    public PlanetSettings Settings
    {
        get
        {
            return Game.WorldSpace.Settings.SystemTypes[ParentSystem.SolarSystemType].PlanetTypes[PlanetType];
        }
    }

    public Planet(BBox bbox, int planetType, int id, SolarSystem parent, BBox voxelbbox)
    {
        abbox = bbox;
        ptype = planetType;
        pid = id;
        parnt = parent;
        vbbox = voxelbbox;
        _transform = new Transformd();
        _transform.Position = vbbox.Center + parent.bbox.Center;
    }

    /// <summary>
    /// Generate the planet
    /// </summary>
    public void GeneratePlanet()
    {
        _vc = new PlanetMaker(this);
        voxels = new VoxelGroup(this, _transform);
    }

    VoxelGroup voxels = null;
    /// <summary>
    /// Voxel data for the planet
    /// </summary>
    public VoxelGroup Voxels
    {
        get
        {
            return voxels;
        }
    }

    /// <summary>
    /// Set planet ID. Not that this will distory planet hash function and equality comparison.
    /// Planets are compared via their ID
    /// </summary>
    /// <param name="s"></param>
    public override void SetID(int s)
    {
        pid = s;
    }

    /// <summary>
    /// Generate data for the planet
    /// </summary>
    /// <param name="planet_settings"></param>
    /// <param name="settings"></param>
    public void GeneratePlanet(GenSettings settings)
    {
        _vc = new PlanetMaker(this);
        _vc.seed = seed(settings);
        voxels = new VoxelGroup(this, _transform);
    }

    public bool Equals(Planet p)
    {
        return p == null ? false : p.ID == ID;
    }
    public override bool Equals(object obj)
    {
        return obj is Planet ? Equals((Planet)obj) : false;
    }
    public override int GetHashCode()
    {
        return ID.GetHashCode();
    }

    /// <summary>
    /// Generate the seed for this planet with the input settings.
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public int seed(GenSettings s)
    {
        unchecked
        {
            return (int)bbox.Position.x * (int)bbox.Position.y * (int)bbox.Position.z * s.seed;
        }
    }
}