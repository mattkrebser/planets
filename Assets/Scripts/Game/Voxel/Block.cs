﻿using UnityEngine;
using System.Collections.Generic;
using System;


[System.Serializable]
public class Block : MonoBehaviour, IEquatable<Block>
{
    //******Please not, Block IDs should only ever be positive numbers!!!!

    /// <summary>
    /// Null block. (Think air voxel) ID == 0. This value must always == 0.
    /// This value holds for booth smooth and cube voxel types
    /// </summary>
    public const int NULL = 0;
    /// <summary>
    /// Invalid block value, (think error) ID == 1. This value must always == 1
    /// </summary>
    public const int INVALID = 1;

    /// <summary>
    /// cube block type integer value. Must == 0
    /// </summary>
    public const int CUBE_BLOCKTYPE = 0;
    /// <summary>
    /// null block type integer value. This value must always == -1
    /// </summary>
    public const int NULL_BLOCKTYPE = -1;
    /// <summary>
    /// custom block type integer value. This value must always == 2
    /// </summary>
    public const int CUSTOM_BLOCKTYPE = 2;

    /// <summary>
    /// Id for this block
    /// </summary>
    public int ID
    {
        get
        {
            return id;
        }
    }
    /// <summary>
    /// Set this block's id
    /// </summary>
    /// <param name="_id"></param>
    internal void SetID(int _id)
    {
        id = _id;
    }
    int id;

    /// <summary>
    /// Is this block type drawn?
    /// </summary>
    public bool Drawable = true;

    /// <summary>
    /// Can objects collide with this voxel?
    /// </summary>
    public bool Collide = true;

    /// <summary>
    /// Allows texture tiling/ batching?
    /// </summary>
    public bool Tileable = true;

    /// <summary>
    /// block type?
    /// </summary>
    public BlockType Block_Type = BlockType.CUBE;

    public Texture2D texture;
    public Material BlockMaterial;

    /// <summary>
    /// UV tile position for blocks whos textures are a part of tile sets. (lower left corner)
    /// </summary>
    [HideInInspector]
    public Vector2 TileMin;
    /// <summary>
    /// UV tile position for blocks whos textures are a part of tile sets. (upper right corner)
    /// </summary>
    [HideInInspector]
    public Vector2 TileMax;

    /// <summary>
    /// BatchID, specifies which batch group the block is a part of. A batch group is a group of block
    /// types whos resulting mesh(s) can be batched together
    /// </summary>
    [HideInInspector]
    public int BatchID = 0;

    public bool Equals(Block b)
    {
        return b == null ? false : b.id == id;
    }
    public override bool Equals(object o)
    {
        return o is Block ? Equals((Block)o) : false;
    }
    public override int GetHashCode()
    {
        return id.GetHashCode();
    }
    public override string ToString()
    {
        return name;
    }

    /// <summary>
    /// block type!
    /// </summary>
    public enum BlockType
    {
        CUBE = CUBE_BLOCKTYPE,
        CUSTOM = CUSTOM_BLOCKTYPE,
        NULL = NULL_BLOCKTYPE
    }
}
