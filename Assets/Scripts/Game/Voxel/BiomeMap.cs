﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class BiomeMap : MonoBehaviour
{
    [Tooltip("Biome map, each pixel represents a biome and its possible neighbors." +
        " 0...X is temperature, 0...Y is humidity.")]
    public Texture2D Map;
    [Tooltip("Representation of biome colors (each pixel has a color).")]
    public List<BiomeType> Biomes = new List<BiomeType>();

    /// <summary>
    /// Returns the color at the input value. Input x and y are normalized seperatly to be between 0 and 1
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public Color32 ColorAt(double x, double y)
    {
        int ix = (int)(Map.width * x);
        int iy = (int)(Map.height * y);
        ix = ix >= Map.width ? Map.width - 1 : ix;
        iy = iy >= Map.height ? Map.height - 1 : iy;
        return Map.GetPixel(ix, iy);
    }
}

[System.Serializable]
public struct BiomeType
{
    public Color32 color;
    public BiomeSettings biome;

    public BiomeType(Color32 c, BiomeSettings b)
    {
        biome = b; color = c;
    }
}
