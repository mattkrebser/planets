﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Voxel = System.Int32;

public partial class VoxelGroup : IEquatable<VoxelGroup>
{
    /// <summary>
    /// Class that provides chunk generation functionality
    /// </summary>
    public partial class VoxelMaker : VoxelGroup.VoxelCreator
    {
        /// <summary>
        /// Chnuk group parent
        /// </summary>
        public VoxelGroup Parent { get; set;}

        /// <summary>
        /// The seed used for random generation
        /// </summary>
        public int seed;

        public virtual Chunk GenerateChunk(Vector3Int pos, int resolution) { return null; }
        public virtual void Initialize() { }
        public virtual void GenerateAll(int resolution = 0) { }
    }

    public interface VoxelCreator
    {

        /// <summary>
        /// Generate a chunk.
        /// </summary>
        /// <param name="ChunkPos"></param>
        Chunk GenerateChunk(Vector3Int pos, int resolution);
        void Initialize();
        void GenerateAll(int resolution = 0);
    }
}
