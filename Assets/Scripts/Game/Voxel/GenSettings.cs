﻿using UnityEngine;
using System.Collections.Generic;


[System.Serializable]
public class GenSettings : MonoBehaviour
{
    [Tooltip("Seed for randomization")]
    public int seed;
    [Tooltip("Space the galaxy will take up. Keep in mind double precision lmits. Max recommended size of one billion!")]
    public BBox VoxelSpace;
    [Range(0, 100000)]
    [Tooltip("Maximum number of solar systems")]
    public int MaxNumberOfSystems = 1;
    [Tooltip("Solar sytem types, must have atleast one")]
    public List<SolarSystemSettings> SystemTypes = new List<SolarSystemSettings>();
    [Tooltip("Store all block objects here. The index of each block equals its ID. Keep this in mind when removing and adding elements.")]
    public BlockLibrary AllBlocks;
    [Tooltip("Player prefab object!")]
    public GameObject PlayerPrefab;
}