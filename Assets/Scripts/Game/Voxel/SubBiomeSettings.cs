﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SubBiomeSettings
{
    [Range(0, 1)]
    public double MinBiomeSize;
    [Range(0, 1)]
    public double MaxBiomeSize;
    [Range(0, 100000000)]
    public double BiomePrevelance;
}
