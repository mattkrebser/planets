﻿using UnityEngine;
using System.Collections.Generic;
using System;

[System.Serializable]
public class BiomeSettings : MonoBehaviour, IEquatable<BiomeSettings>
{
    public Block surface;

    public bool Equals(BiomeSettings b)
    {
        if (ReferenceEquals(surface, null))
            return b == null ? false : ReferenceEquals(b.surface, null);
        else
            return surface.Equals(b.surface);
    }
}
