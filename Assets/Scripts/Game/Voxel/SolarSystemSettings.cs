﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

[System.Serializable]
public class SolarSystemSettings : MonoBehaviour
{
    [Range(0, 100)]
    public int MaxNumberOfPlanets = 1;
    [Tooltip("Planets will only occupy the inner 2/3rds")]
    public double MaxSystemSize;
    [Tooltip("Planets will only occupy the inner 2/3rds")]
    public double MinSystemSize;
    public List<PlanetSettings> PlanetTypes = new List<PlanetSettings>();

    [Tooltip("If true, there will only be one instance of this solar system and it will always exists.")]
    public bool FixedSystem = false;
    [Tooltip("Position, if this sytem is fixed. This fieled is only valid if 'FixedSytem'=true. NOTE* Intersecting fixed systems cannot exist. This position is the center of the bbox.")]
    public Vector3d FixedPosition;
    [Tooltip("An extra seed that is used if this is a fixed solar system.")]
    public int FixedSeed = 0;
}

[System.Serializable]
public class SolarSystem : IEquatable<SolarSystem>
{
    /// <summary>
    /// A unique ID given to each solar system. Recommended: Don't change
    /// </summary>
    public int ID;

    /// <summary>
    /// Type refers to index in the GenSettings.SystemTypes list
    /// </summary>
    public int SolarSystemType;

    /// <summary>
    /// A human readable type name for this solar system.
    /// </summary>
    public string type_name;

    HashSet<Planet> planets;

    /// <summary>
    /// Bounding box for this solar system
    /// </summary>
    public BBox bbox
    {
        get
        {
            return Game.WorldSpace.SolarSystems.GetBBox(this);
        }
    }

    /// <summary>
    /// All planets in this solar system
    /// </summary>
    public IEnumerable<Planet> Planets
    {
        get
        {
            if (planets != null)
                foreach (var p in planets)
                    yield return p;
        }
    }

    /// <summary>
    /// BBoxTree for this solar system
    /// </summary>
    public BBoxQuadTree<PhysicsObject> Objects;

    public bool Equals(SolarSystem s)
    {
        return s == null ? false : s.ID == ID;
    }
    public override bool Equals(object obj)
    {
        return obj is SolarSystem ? Equals((SolarSystem)obj) : false;
    }
    public override int GetHashCode()
    {
        return ID.GetHashCode();
    }

    /// <summary>
    /// Generate the objects for this solar system
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="SolarSystems"></param>
    public void GenerateSystem(GenSettings settings, BBoxQuadTree<SolarSystem> SolarSystems)
    {
        BBox mybbox = SolarSystems.GetBBox(this);
        System.Random r = new System.Random(Seed(settings.seed, mybbox));
        SolarSystemSettings system_settings = settings.SystemTypes[SolarSystemType];
        //make a sun?

        //make planets
        planets = new HashSet<Planet>();

        //generate fixed planets
        for (int i = 0; i < system_settings.PlanetTypes.Count; i++)
        {
            int index = i;
            PlanetSettings planet_settings = system_settings.PlanetTypes[index];
            //if not fixed, then continue
            if (!planet_settings.FixedPlanet) continue;
            System.Random f_r = new System.Random(planet_settings.FixedSeed);
            BBox p_bbox = rand_bbox(planet_settings.MinPlanetSize, planet_settings.MaxPlanetSize, mybbox, f_r);
            p_bbox.Center = planet_settings.FixedPosition;
            Vector3d sizeO3 = p_bbox.Size / 3;
            BBox v_bbox = new BBox(
                new Vector3d(
                    p_bbox.Position.x + sizeO3.x,
                    p_bbox.Position.y + sizeO3.y,
                    p_bbox.Position.z + sizeO3.z),
                    sizeO3);
            //doesn't intersect anything
            if (!BodyIntersects(p_bbox))
            {
                Planet new_p = rand_planet(system_settings, f_r, p_bbox, index, this, v_bbox);
                new_p.type_name = planet_settings.gameObject.name;
                planets.Add(new_p);
            }
        }

        var p_types = system_settings.PlanetTypes.Where(x => !x.FixedPlanet).ToList();

        if (p_types.Count > 0)
            //generate random (non fixed) planets
            for (int i = 0; i < system_settings.MaxNumberOfPlanets; i++)
            {
                int index = r.Next(0, p_types.Count);
                PlanetSettings planet_settings = p_types[index];
                BBox p_bbox = rand_bbox(planet_settings.MinPlanetSize, planet_settings.MaxPlanetSize, mybbox, r);
                Vector3d sizeO3 = p_bbox.Size / 3;
                BBox v_bbox = new BBox(
                    new Vector3d(
                        p_bbox.Position.x + sizeO3.x,
                        p_bbox.Position.y + sizeO3.y,
                        p_bbox.Position.z + sizeO3.z),
                        sizeO3);
                //doesn't intersect anything
                if (!BodyIntersects(p_bbox))
                {
                    Planet new_p = rand_planet(system_settings, r, p_bbox, index, this, v_bbox);
                    new_p.type_name = planet_settings.gameObject.name;
                    planets.Add(new_p);
                }
            }
    }
    /// <summary>
    /// Get constant randomized seed
    /// </summary>
    /// <param name="seed"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    int Seed(int seed, BBox b)
    {
        unchecked
        {
            return (int)b.Position.x * (int)b.Position.y * (int)b.Position.z * seed;
        }
    }
    /// <summary>
    /// Returns a random bbox that falls within the input parameters
    /// </summary>
    /// <param name="min_size"></param>
    /// <param name="max_size"></param>
    /// <param name="bounding_space"></param>
    /// <param name="r"></param>
    /// <returns></returns>
    BBox rand_bbox(int min_size, int max_size, BBox bounding_space, System.Random r)
    {
        //we multiply by 2/3 and add 1/6 to shrink the space planets can be placed in
        //this creates a 1/3size border of empty around the solar system
        Vector3d size = Vector3d.zero;
        int p_size = r.Next(min_size, max_size);
        size.x = p_size; size.y = p_size; size.z = p_size;
        Vector3d pos = Vector3d.zero;
        pos.x = (double)r.NextDouble() * (bounding_space.Size.x * 0.66666666666d - size.x)
            + bounding_space.Position.x + bounding_space.Size.x * 0.166666666666d;
        pos.y = (double)r.NextDouble() * (bounding_space.Size.y * 0.66666666666d - size.y)
            + bounding_space.Position.y + bounding_space.Size.y * 0.166666666666d;
        pos.z = (double)r.NextDouble() * (bounding_space.Size.z * 0.66666666666d - size.z)
            + bounding_space.Position.z + bounding_space.Size.z * 0.166666666666d;
        return new BBox(pos, size);
    }
    /// <summary>
    /// Returns true if the input bbox intersects a body in the solar system
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    bool BodyIntersects(BBox b)
    {
        if (planets != null)
            foreach (Planet p in planets)
                if (p.bbox.Intersects(b))
                    return true;
        return false;
    }
    /// <summary>
    /// Return randomized planet
    /// </summary>
    /// <param name="system_settings"></param>
    /// <param name="r"></param>
    /// <returns></returns>
    Planet rand_planet(SolarSystemSettings system_settings, System.Random r,
        BBox bbox, int ptype, SolarSystem s, BBox v_bbox)
    {
        Planet p = new Planet(bbox, ptype, 0, s, v_bbox);
        p.SetID(r.Next(0, int.MaxValue));
        while (planets.Contains(p))
            p.SetID(r.Next(0, int.MaxValue));
        return p;
    }

    /// <summary>
    /// Copy the position from 'from' into 'to'. This function will transform the double precision transform about
    /// the origin and perform other operations so that it can be position/rotated/scaled into unity scene view.
    /// </summary>
    /// <param name="to"></param>
    /// <param name="from"></param>
    public void SetUnityTransform(Transform to, Transformd from)
    {
        var dist_vec = from.Position - bbox.Center;
        if (dist_vec.x > 65535 || dist_vec.y > 65535 || dist_vec.z > 65535)
        {
            //TODO: scale object based on distance and dont throw error
            throw new System.Exception("Error, object is too far.");
        }
        var to_origin = (Vector3)dist_vec;
        to.position = to_origin;
    }
}