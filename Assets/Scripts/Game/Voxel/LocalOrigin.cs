﻿using UnityEngine;
using System.Collections;

public class LocalOrigin : MonoBehaviour
{
    /// <summary>
    /// Local system.
    /// </summary>
    public SolarSystem LocalSystem;

    /// <summary>
    /// The origin of space. Could be any 3d vector. Typically the origin is set
    /// to the center of a solar sytem that the camera is rendering
    /// </summary>
    public Vector3d Origin
    {
        get
        {
            return LocalSystem.bbox.Center;
        }
    }
}
