﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

[System.Serializable]
public class BlockLibrary : MonoBehaviour
{
    ///library of blocks
    List<Block> BlockLib = new List<Block>();

    //batch dictionary
    static Dictionary<int, Material> batch_dict = 
        new Dictionary<int, Material>(); 

    static Block[] Instance;
    /// <summary>
    /// Block library, each block ID represents its index in the list
    /// </summary>
    public static Block[] Blocks
    {
        get
        {
            return Instance;
        }
    }
    /// <summary>
    /// get a block with type or id
    /// </summary>
    /// <param name="type"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static Block GetBlock(int id)
    {
        return Instance[ id];
    }

    /// <summary>
    /// Initialize the block library
    /// </summary>
	public void InitializeLibrary()
    {
        BlockLib = GetComponentsInChildren<Block>(true).ToList();
        int c_count = BlockLib.Count();
        Instance = new Block[c_count+2];

        //add null block to front
        var nullblock = new GameObject().AddComponent<Block>();
        nullblock.transform.parent = transform;
        nullblock.SetID(0);
        nullblock.Drawable = false;
        nullblock.gameObject.name = "NULL";
        nullblock.Block_Type = Block.BlockType.NULL;
        nullblock.Collide = false;
        BlockLib.Insert(0, nullblock);

        //add invalid block to front
        var invalidblock = new GameObject().AddComponent<Block>();
        invalidblock.transform.parent = transform;
        invalidblock.SetID(Block.INVALID);
        invalidblock.Drawable = false;
        invalidblock.gameObject.name = "INVALID";
        invalidblock.Block_Type = Block.BlockType.NULL;
        invalidblock.Collide = false;
        BlockLib.Insert(1, invalidblock);

        //set ids for all blocks
        for (int i = 0; i < BlockLib.Count; i++)
            if (BlockLib[i] != null)
            {
                BlockLib[i].SetID(i);
                Instance[i] = BlockLib[i];
            }

        //create tile textures
        CreateTileTextures();
        //create batch groups
        AssignBatchIDs();
    }
    /// <summary>
    /// clear block library
    /// </summary>
    public void ClearLib()
    {
        Instance = null;
        BlockLib.Clear();
    }
    /// <summary>
    /// get batch material with the input batch id
    /// </summary>
    /// <param name="batch_id"></param>
    /// <returns></returns>
    public static Material BatchMaterial(int batch_id)
    {
        return batch_dict[batch_id];
    }

    /// <summary>
    /// Find batch ID for every block
    /// </summary>
    void AssignBatchIDs()
    {
        //create a dictionary of all non null entries organized by type
        Dictionary<BatchType, List<Block>> Batches = new Dictionary<BatchType, List<Block>>();
        foreach (var b in Blocks)
        {
            if (b != null)
            {
                var type = new BatchType(b.texture, b.BlockMaterial);
                List<Block> blist;
                if (!Batches.TryGetValue(type, out blist))
                    Batches.Add(type, blist = new List<Block>());
                blist.Add(b);
            }
        }

        int id = 0;
        //iterate through the items
        //Special cases: Material or Texture = null is unique id for all elements
        foreach (var bpair in Batches)
        {
            var type = bpair.Key;
            var blist = bpair.Value;
            //unqiue id
            if (type.m == null || type.t == null)
            {
                id++;
                blist.ForEach(x => x.BatchID = id++);
            }
            //same id (batched group)
            else
            {
                id++;
                blist.ForEach(x => x.BatchID = id);
            }
        }

        //temp dictionary object
        Dictionary<int, ClassTuple<Material, Texture>> batchdict =
        new Dictionary<int, ClassTuple<Material, Texture>>();

        //construct batch_id dictionary
        foreach (var block in Blocks)
            if (block != null)
                if (!batchdict.ContainsKey(block.BatchID))
                    batchdict.Add(block.BatchID, new ClassTuple<Material,
                        Texture>(block.BlockMaterial, block.texture));

        //construct material instances!
        foreach (var batch in batchdict)
        {
            var mat = batch.Value.t1;
            var matcopy = mat == null ? null : new Material(mat);
            if (mat != null) matcopy.mainTexture = batch.Value.t2;
            batch_dict.Add(batch.Key, matcopy);
        }
    }

    /// <summary>
    /// Create tile textures! 
    /// Also initializes all block tile positions (Required even if not in a tile texture)
    /// </summary>
    void CreateTileTextures()
    {
        int max_texture_size = 2048;
        int padding = 2;
        Dictionary<Material, List<Block>> BlocksByMaterial = new Dictionary<Material, List<Block>>();
        //to dictionary
        foreach (var b in Blocks)
        {
            List<Block> blist;
            //b can be null (some entries are not filled), and b.material can be null (invisible block)
            //both cases are allowed. Also the texture must be non null.
            if (b != null && b.BlockMaterial != null && b.texture != null && b.Tileable)
            {
                if (!BlocksByMaterial.TryGetValue(b.BlockMaterial, out blist))
                    BlocksByMaterial.Add(b.BlockMaterial, blist = new List<Block>());
                blist.Add(b);
            }
        }

        //iterate through dictionary
        foreach (var blist in BlocksByMaterial.Values)
        {
            //seperate textures further by dimensions and maximum texture size
            foreach (var partition in SeperateTextures(blist, max_texture_size))
            {
                //no point in creating tile textures with a single texture on it...
                if (partition.Count > 1)
                {
                    //create a tile texture for this set
                    CreateTileTexture(partition, max_texture_size, padding);
                }
            }
        }

        //initialie non tiled blocks
        foreach (var b in Blocks)
            if (b!= null && b.TileMax == Vector2.zero)
                b.TileMax = new Vector2(1, 1);
    }

    /// <summary>
    /// Input a set of textures and returns seperate sets of textures. The sets are seperated by
    /// different widths and maximum texture area partitions
    /// </summary>
    /// <param name="blist"></param>
    /// <param name="maxSize"></param>
    /// <returns></returns>
    IEnumerable<List<Block>> SeperateTextures(List<Block> blist, int maxSize)
    {
        //create sets of blocks that have equal dimensions 
        //(we dont have to look at height because textures are squares)
        //The square property will be enforced later on
        Dictionary<int, List<Block>> BlocksByWidth = new Dictionary<int, List<Block>>();
        foreach (var b in blist)
        {
            List<Block> wlist;
            if (!BlocksByWidth.TryGetValue(b.texture.width, out wlist))
                BlocksByWidth.Add(b.texture.width, wlist = new List<Block>());
            wlist.Add(b);
        }
        //Note, textures with an area greater than the input max area can still be created
        //this Area thresholding just limits the textures to at most be of 1024x2048 size (not including padding)
        //a padding value of 1 with image squares of size 1 could increase the image size to 9*1024*1024
        //we will still be well within the max limit for texture sizes
        int maxArea = maxSize * maxSize;
        //Partition the lists into smaller sets that will fit inside
        foreach (var wlist in BlocksByWidth.Values)
        {
            int area = 0;
            List<Block> alist = new List<Block>();
            for (int i = 0; i < wlist.Count; i++)
            {
                bool return_list = false;
                //compute area of texture
                int widthsqrd = wlist[i].texture.width * wlist[i].texture.width;
                //if texture is too big...
                if (widthsqrd > maxArea)
                    throw new System.Exception("Error, texture length is too big");
                //compute next area
                int next_area = area + widthsqrd;
                //if area too big...
                if (next_area > maxArea)
                    return_list = true;
                //if last element
                else if (i == wlist.Count - 1)
                {
                    return_list = true;
                    alist.Add(wlist[i]);
                }
                //otherwise, add and continue
                else
                    alist.Add(wlist[i]);
                //if return...
                if (return_list)
                {
                    yield return alist;
                    //if not last iteration, reset list
                    if (i != wlist.Count - 1)
                        alist = new List<Block>();
                }
            }
        }
    }

    /// <summary>
    /// Create tile textures for textures who share similar materials. The input should be a list of square textures
    /// with the same size
    /// </summary>
    Texture2D CreateTileTexture(IEnumerable<Block> input_blocks, int max_size, int pad)
    {
        if (input_blocks == null || input_blocks.Count() == 0)
            return null;
        List<Block> textures = new List<Block>();
        int w = 0, h = 0;
        //enforce square textures
        foreach (var block in input_blocks)
        {
            if (block.texture != null)
            {
                if (textures.Count == 0)
                {
                    w = block.texture.width;
                    h = block.texture.height;
                }
                if (block.texture.width != block.texture.height)
                    throw new System.Exception("Error, block textures must all be squares");
                else if (block.texture.width != w || block.texture.height != h)
                    throw new System.Exception("Error, mis-matched width height block texture group");
                else if (block.texture.width > max_size)
                    throw new System.Exception("Max block texture width is " + max_size.ToString() + " pixels!");
                else
                    textures.Add(block);
            }
        }

        //account for padding
        w += pad * 2;
        h += pad * 2;

        //if empty
        if (textures.Count == 0)
            return null;

        //regroup into 1024 width rows
        int running_width = 0;
        List<List<Block>> grouped_textures = new List<List<Block>>();
        List<Block> CurrentList = new List<Block>();
        grouped_textures.Add(CurrentList);
        for (int i = 0; i < textures.Count; i++)
        {
            int next_width = running_width + textures[i].texture.width;
            if (next_width > max_size)
            {
                grouped_textures.Add(new List<Block>());
                CurrentList = grouped_textures[grouped_textures.Count - 1];
                running_width = textures[i].texture.width;
            }
            else
            {
                CurrentList.Add(textures[i]);
                running_width = next_width;
            }
        }

        //make new texture
        int width = grouped_textures.Count == 1 ? grouped_textures[0].Count * w : 1024;
        int height = grouped_textures.Count * h;
        //TODO: custom texture resolutions (full,half,etc..)
        Texture2D result = new Texture2D(width, height, TextureFormat.ARGB32, false);
        result.filterMode = FilterMode.Point;
        result.alphaIsTransparency = true;
        result.wrapMode = TextureWrapMode.Repeat;

        //copy each row into the texture
        Vector2Int position = new Vector2Int(pad, pad);
        for (int i = 0; i < grouped_textures.Count; i++)
        {
            var list = grouped_textures[i];
            for (int j = 0; j < list.Count; j++)
            {
                //compute normalized texture positions
                list[j].TileMin = new Vector2(position.x / (float)result.width, position.y / (float)result.height);
                list[j].TileMax = new Vector2((position.x + list[j].texture.width) / (float)result.width,
                    (position.y + list[j].texture.height) / (float)result.height);
                //copy texture into result
                TexCopy(result, list[j].texture, position, pad);
                //assign texture
                list[j].texture = result;
                //increment position
                position.x += w;
            }
            //increment height
            position.y += h;
        }

        result.Apply();

        DebugTexture(result);

        return result;
    }

    /// <summary>
    /// Copy 'from' into 'to' at the desired position with the desired amount of padding on all sides
    /// </summary>
    /// <param name="to"> copy to this</param>
    /// <param name="from"> copy from this</param>
    /// <param name="position"> Position to copy image to </param>
    /// <param name="pad"> Padding to add around the image after copying </param>
    void TexCopy(Texture2D to, Texture2D from, Vector2Int position, int pad)
    {
        Vector2Int start = new Vector2Int(position.x, position.y);
        Vector2Int stop = new Vector2Int(position.x + from.width, position.y + from.height);

        //copy texture
        int m = 0, n = 0;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
                n++;
            }
            m++;
        }

        //copy padding
        //left side
        start = new Vector2Int(position.x - pad, position.y);
        stop = new Vector2Int(position.x, position.y + from.height);
        m = 0; n = 0;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
                n++;
            }
        }

        //right side
        start = new Vector2Int(position.x + from.width, position.y);
        stop = new Vector2Int(position.x + from.width + pad, position.y + from.height);
        m = from.width - 1; n = 0;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
                n++;
            }
        }

        //top
        start = new Vector2Int(position.x, position.y + from.height);
        stop = new Vector2Int(position.x + from.width, position.y + from.height + pad);
        m = 0; n = from.height - 1;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
            }
            m++;
        }

        //bottom
        start = new Vector2Int(position.x, position.y - pad);
        stop = new Vector2Int(position.x + from.width, position.y);
        m = 0; n = 0;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
            }
            m++;
        }

        //top left corner
        start = new Vector2Int(position.x - pad, position.y + from.height);
        stop = new Vector2Int(position.x, position.y + from.height + pad);
        m = 0; n = from.height - 1;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
            }
        }

        //top right corner
        start = new Vector2Int(position.x + from.width, position.y + from.height);
        stop = new Vector2Int(position.x + from.width + pad, position.y + from.height + pad);
        m = from.width - 1; n = from.height - 1;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
            }
        }

        //bottom left corner
        start = new Vector2Int(position.x - pad, position.y - pad);
        stop = new Vector2Int(position.x, position.y);
        m = 0; n = 0;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
            }
        }

        //bottom right corner
        start = new Vector2Int(position.x + from.width, position.y - pad);
        stop = new Vector2Int(position.x + from.width + pad, position.y);
        m = from.width - 1; n = 0;
        for (int i = start.x; i < stop.x; i++)
        {
            for (int j = start.y; j < stop.y; j++)
            {
                to.SetPixel(i, j, from.GetPixel(m, n));
            }
        }
    }

    /// <summary>
    /// Display a texture as a mesh
    /// </summary>
    void DebugTexture(Texture2D t)
    {
        GameObject g = new GameObject("DEBUG_TEXTURE");
        var mr = g.AddComponent<MeshRenderer>();
        var mf = g.AddComponent<MeshFilter>();
        mr.material = new Material(Shader.Find("Unlit/Texture"));
        mr.material.mainTexture = t;

        Vector3[] verts = new Vector3[] { new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0, 0, 1), new Vector3(1, 0, 1) };
        Vector2[] uvs = new Vector2[] { new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1) };
        int[] tris = new int[] { 0, 2, 3,  0, 3, 1 };
        mf.mesh.vertices = verts;
        mf.mesh.triangles = tris;
        mf.mesh.uv = uvs;
        mf.mesh.RecalculateNormals();

        float w = t.width > t.height ? 1 : t.width / (float)t.height;
        float h = t.height > t.width ? 1 : t.height / (float)t.width;
        g.transform.localScale = new Vector3(w * 20, 1, h * 20);
    }

    /// <summary>
    /// tuple object
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    struct Tuple<T1, T2>
    {
        public T1 t1;
        public T2 t2;
        public Tuple(T1 _t1, T2 _t2)
        {
            t1 = _t1; t2 = _t2;
        }
    }

    /// <summary>
    /// A reference tuple object
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    class ClassTuple<T1, T2>
    {
        public T1 t1;
        public T2 t2;
        public ClassTuple(T1 _t1, T2 _t2)
        {
            t1 = _t1; t2 = _t2;
        }
    }

    /// <summary>
    /// Batch Type. Specifies properties that determine unique Batch groups for block materials and textures
    /// </summary>
    struct BatchType : IEquatable<BatchType>
    {
        public Texture2D t;
        public Material m;

        public BatchType(Texture2D tex, Material mat)
        {
            t = tex; m = mat;
        }
        public bool Equals(BatchType b)
        {
            return b.t == t && b.m == m;
        }
        public override bool Equals(object obj)
        {
            return obj is BatchType ? Equals((BatchType)obj) : false;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 29 + (t== null ? 0 : t.GetHashCode());
                hash = hash * 29 + (m == null ? 0 : m.GetHashCode());
                return hash;
            }
        }
    }
}
