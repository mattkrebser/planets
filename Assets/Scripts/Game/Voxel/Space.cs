﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

/// <summary>
/// All space that voxels can exist in anywhere in the playable game
/// </summary>
public class Space
{
    // Planets have local coordinates relative to their solar system
    // planets are placed about the minimum point of the solar system bbox

    /// <summary>
    /// All solar systems in space
    /// </summary>
    public BBoxQuadTree<SolarSystem> SolarSystems;

    /// <summary>
    /// List of points that voxels and oyther objects will be loaded about. (Eg a player)
    /// </summary>
    public List<LocalOrigin> origins = new List<LocalOrigin>();

    public GenSettings Settings
    {
        get
        {
            return gsettings;
        }
    }
    GenSettings gsettings = null;

    /// <summary>
    /// Generate a new universe!
    /// </summary>
    /// <param name="seed"></param>
    public void Generate(GenSettings settings)
    {
        //initialize block array obj
        settings.AllBlocks.InitializeLibrary();
        //make solar sytems
        double precision = Math.Max(Math.Max(settings.VoxelSpace.Size.x, settings.VoxelSpace.Size.z), settings.VoxelSpace.Size.z)
            / (settings.MaxNumberOfSystems / 10.0f);
        SolarSystems = new BBoxQuadTree<SolarSystem>(settings.VoxelSpace, precision);
        SpaceGen spaceGen = new SpaceGen();
        spaceGen.GenSolarSystems(settings, SolarSystems);
        gsettings = settings;

        //generate everything
        foreach (var s in SolarSystems.AllBBoxes)
        {
            s.Key.GenerateSystem(gsettings, SolarSystems);
            foreach (var p in s.Key.Planets)
                p.GeneratePlanet();
        }
    }

    /// <summary>
    /// CLear the entire space. Destroys all objects and everything.
    /// </summary>
    public void Clear()
    {
        SolarSystems = null;
    }
}

/// <summary>
/// Class representing the location of an object
/// </summary>
public class Location
{
    /// <summary>
    /// global position
    /// </summary>
    public Transformd point;
    /// <summary>
    /// parent object (if any)
    /// </summary>
    public VoxelGroup voxel_object;
    /// <summary>
    /// parent solar system
    /// </summary>
    public SolarSystem system;
}

