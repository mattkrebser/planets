﻿using UnityEngine;
using System.Collections;

public static class Geometry
{
    static double deg2rad = System.Math.PI / 180;

    /// <summary>
    /// Finds the intersection between 2 planes. Returns false if they are parallel.
    /// </summary>
    /// <param name="linePoint"></param>
    /// <param name="lineVec"></param>
    /// <param name="plane1"></param>
    /// <param name="plane2"></param>
    public static bool PlanePlaneIntersection(out Vector3d linePoint, out Vector3d lineDir,
        Plane_ex plane1, Plane_ex plane2)
    {
        linePoint = new Vector3d(0, 0, 0);

        //Get the normals of the planes.
        Vector3d plane1Normal = plane1.plane.normal;
        Vector3d plane2Normal = plane2.plane.normal;

        lineDir = Vector3d.Cross(plane1Normal, plane2Normal);
        //Determine if the cross product yielded (0,0,0) - parallel
        if (lineDir.x < 0.00001f && lineDir.x > -0.00001f &&
            lineDir.y < 0.00001f && lineDir.y > -0.00001f &&
            lineDir.z < 0.00001f && lineDir.z > -0.00001f)
        {
            linePoint = new Vector3d(0, 0, 0);
            return false;
        }

        //absolute values of the normal
        double ax = (lineDir.x >= 0 ? lineDir.x : -lineDir.x);
        double ay = (lineDir.y >= 0 ? lineDir.y : -lineDir.y);
        double az = (lineDir.z >= 0 ? lineDir.z : -lineDir.z);

        //Determine which coordinate is the largest from 0
        //x is biggest => maxc = 1, y is biggest => maxc = 2, z => maxc = 3
        //we use this to solve the plane equations
        int maxc;
        if (ax > ay)
        {
            if (ax > az)
                maxc = 1;
            else maxc = 3;
        }
        else
        {
            if (ay > az)
                maxc = 2;
            else maxc = 3;
        }

        double d1, d2;
        d1 = -Vector3d.Dot(plane1.plane.normal, plane1.v1);
        d2 = -Vector3d.Dot(plane2.plane.normal, plane2.v1);

        switch (maxc)
        {
            case 1:
                linePoint.x = 0;
                linePoint.y = (d2 * plane1.plane.normal.z - d1 * plane2.plane.normal.z) / lineDir.x;
                linePoint.z = (d1 * plane2.plane.normal.y - d2 * plane1.plane.normal.y) / lineDir.x;
                break;
            case 2:
                linePoint.x = (d1 * plane2.plane.normal.z - d2 * plane1.plane.normal.z) / lineDir.y;
                linePoint.y = 0;
                linePoint.z = (d2 * plane1.plane.normal.x - d1 * plane2.plane.normal.x) / lineDir.y;
                break;
            case 3:
                linePoint.x = (d2 * plane1.plane.normal.y - d1 * plane2.plane.normal.y) / lineDir.z;
                linePoint.y = (d1 * plane2.plane.normal.x - d2 * plane1.plane.normal.x) / lineDir.z;
                linePoint.z = 0;
                break;
        }
        return true;
    }

    /// <summary>
    /// Calculate line intersection. Returns false if skew. Ignores Coinciding lines
    /// </summary>
    /// <param name="intersection"></param>
    /// <param name="linePoint1"></param>
    /// <param name="lineVec1"></param>
    /// <param name="linePoint2"></param>
    /// <param name="lineVec2"></param>
    /// <returns></returns>
    public static bool LineLineIntersection(out Vector3d intersection, Vector3d linePoint1,
        Vector3d lineDir1, Vector3d linePoint2, Vector3d lineDir2)
    {
        intersection = new Vector3d(0, 0, 0);

        Vector3d lineDir3 = linePoint2 - linePoint1;
        Vector3d crossVec1and2 = Vector3d.Cross(lineDir1, lineDir2);
        Vector3d crossVec3and2 = Vector3d.Cross(lineDir3, lineDir2);

        double planarFactor = Vector3d.Dot(lineDir3, crossVec1and2);
        //Lines are not coplanar. Take into account rounding errors.
        if ((planarFactor >= 0.00001f) || (planarFactor <= -0.00001f))
        {
            return false;
        }

        //Note: sqrMagnitude does x*x+y*y+z*z on the input vector.
        double s = Vector3d.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;

        if ((s >= 0.0f) && (s <= 1.0f))
        {
            intersection = linePoint1 + (lineDir1 * s);
            return true;
        }
        else
        {
            //we aren't using coincidence :P
            /* //check for coincidence
             if (linePoint1 == linePoint2)
                 return true;
             Vector3d norm21 = (linePoint2 - linePoint1).normalized;
             Vector3d norm12 = (linePoint1 - linePoint2).normalized;
             Vector3d dir_norm = lineDir1.normalized;

             //check if dir_norm == norm21 or == dir_norm == norm12
             if ((norm21.x < dir_norm.x + 0.00001f && norm21.y < dir_norm.y + 0.00001f && norm21.z < dir_norm.z + 0.00001f
                 && norm21.x > dir_norm.x - 0.00001f && norm21.y > dir_norm.y - 0.00001f && norm21.z > dir_norm.z - 0.00001f) ||
                 (norm12.x < dir_norm.x + 0.00001f && norm12.y < dir_norm.y + 0.00001f && norm12.z < dir_norm.z + 0.00001f
                 && norm12.x > dir_norm.x - 0.00001f && norm12.y > dir_norm.y - 0.00001f && norm12.z > dir_norm.z - 0.00001f))
                 return true;
             */

            return false;
        }
    }

    /// <summary>
    /// Intersection of a line and plane. Returns false if parallel, otherwise true.
    /// </summary>
    /// <param name="intersection"></param>
    /// <param name="linePoint"></param>
    /// <param name="lineVec"></param>
    /// <param name="planeNormal"></param>
    /// <param name="planePoint"></param>
    /// <returns></returns>
    public static bool LinePlaneIntersection(out Vector3d intersection, Vector3d linePoint,
        Vector3d lineVec, Vector3d planeNormal, Vector3d planePoint)
    {
        Vector3d l1 = linePoint, l2 = linePoint + lineVec;
        double d1 = Vector3d.Dot(planeNormal, planePoint - l1);
        double d2 = Vector3d.Dot(planeNormal, l2 - l1);
        if (d2 == 0) { intersection = Vector3d.zero; return false; }
        intersection = l1 + d1 / d2 * (l2 - l1); return true;
    }

    /// <summary>
    /// Returns true if c lies on ab
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="c"></param>
    /// <returns></returns>
    public static bool PointOnSegment(Vector3d a, Vector3d b, Vector3d c)
    {
        double d1 = (c - a).sqrMagnitude;
        double d2 = (c - b).sqrMagnitude;
        double d3 = (b - a).sqrMagnitude;
        return ApproxEqual(d1 + d2, d3, 0.00001d);
        
    }
    static bool ApproxEqual(double a, double b, double epsilon)
    {
        return (a - b) - epsilon <= 0 && (b - a) + epsilon >= 0;
    }

    /// <summary>
    /// Distance between two line segments
    /// </summary>
    /// <param name="s1p0"></param>
    /// <param name="s1p1"></param>
    /// <param name="s2p0"></param>
    /// <param name="s2p1"></param>
    /// <returns></returns>
    public static double LineSegLineSegDistance(Vector3d s1p0, Vector3d s1p1, Vector3d s2p0, Vector3d s2p1)
    {
        // Copyright 2001 softSurfer, 2012 Dan Sunday
        // This code may be freely used, distributed and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        double epsilon = 0.000001d;
        Vector3d u = s1p1 - s1p0;
        Vector3d v = s2p1 - s2p0;
        Vector3d w = s1p0 - s2p0;
        double a = Vector3d.Dot(u, u); 
        double b = Vector3d.Dot(u, v);
        double c = Vector3d.Dot(v, v); 
        double d = Vector3d.Dot(u, w);
        double e = Vector3d.Dot(v, w);
        double D = a * c - b * b;  
        double sc, sN, sD = D; 
        double tc, tN, tD = D;  
        if (D < epsilon)
        { 
            sN = 0.0d;
            sD = 1.0d; 
            tN = e;
            tD = c;
        }
        else
        {   
            sN = (b * e - c * d);
            tN = (a * e - b * d);
            if (sN < 0.0d)
            {
                sN = 0.0d;
                tN = e;
                tD = c;
            }
            else if (sN > sD)
            { 
                sN = sD;
                tN = e + b;
                tD = c;
            }
        }
        if (tN < 0.0d)
        {  
            tN = 0.0d;
            if (-d < 0.0d)
                sN = 0.0d;
            else if (-d > a)
                sN = sD;
            else
            {
                sN = -d;
                sD = a;
            }
        }
        else if (tN > tD)
        {  
            tN = tD;
            if ((-d + b) < 0.0d)
                sN = 0d;
            else if ((-d + b) > a)
                sN = sD;
            else
            {
                sN = (-d + b);
                sD = a;
            }
        }
        sc = (System.Math.Abs(sN) < epsilon ? 0.0 : sN / sD);
        tc = (System.Math.Abs(tN) < epsilon ? 0.0 : tN / tD);
        Vector3d dP = w + (sc * u) - (tc * v);
        return dP.magnitude; 
    }
    /// <summary>
    /// Distance between two line segments
    /// </summary>
    /// <param name="s1p0"></param>
    /// <param name="s1p1"></param>
    /// <param name="s2p0"></param>
    /// <param name="s2p1"></param>
    /// <returns></returns>
    public static double LineSegLineSegDistanceSqrd(Vector3d s1p0, Vector3d s1p1, Vector3d s2p0, Vector3d s2p1)
    {
        // Copyright 2001 softSurfer, 2012 Dan Sunday
        // This code may be freely used, distributed and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        double epsilon = 0.000001d;
        Vector3d u = s1p1 - s1p0;
        Vector3d v = s2p1 - s2p0;
        Vector3d w = s1p0 - s2p0;
        double a = Vector3d.Dot(u, u);
        double b = Vector3d.Dot(u, v);
        double c = Vector3d.Dot(v, v);
        double d = Vector3d.Dot(u, w);
        double e = Vector3d.Dot(v, w);
        double D = a * c - b * b;
        double sc, sN, sD = D;
        double tc, tN, tD = D;
        if (D < epsilon)
        {
            sN = 0.0d;
            sD = 1.0d;
            tN = e;
            tD = c;
        }
        else
        {
            sN = (b * e - c * d);
            tN = (a * e - b * d);
            if (sN < 0.0d)
            {
                sN = 0.0d;
                tN = e;
                tD = c;
            }
            else if (sN > sD)
            {
                sN = sD;
                tN = e + b;
                tD = c;
            }
        }
        if (tN < 0.0d)
        {
            tN = 0.0d;
            if (-d < 0.0d)
                sN = 0.0d;
            else if (-d > a)
                sN = sD;
            else
            {
                sN = -d;
                sD = a;
            }
        }
        else if (tN > tD)
        {
            tN = tD;
            if ((-d + b) < 0.0d)
                sN = 0d;
            else if ((-d + b) > a)
                sN = sD;
            else
            {
                sN = (-d + b);
                sD = a;
            }
        }
        sc = (System.Math.Abs(sN) < epsilon ? 0.0 : sN / sD);
        tc = (System.Math.Abs(tN) < epsilon ? 0.0 : tN / tD);
        Vector3d dP = w + (sc * u) - (tc * v);
        return dP.sqrMagnitude;
    }
    /// <summary>
    /// Returns true if the point 'p' is inside the triangle defined by (t1,t2,t3).
    /// Assumes triangle and point are in the same plane!
    /// </summary>
    /// <param name="t1"></param>
    /// <param name="t2"></param>
    /// <param name="t3"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static bool PointInsideTriangle(Vector3d t1, Vector3d t2, Vector3d t3, Vector3d p)
    {
        //Compute vectors        
        Vector3d v0 = t3 - t1;
        Vector3d v1 = t2 - t1;
        Vector3d v2 = p - t1;

        //Compute dot products
        double dot00 = v0.x * v0.x + v0.y * v0.y + v0.z * v0.z;
        double dot01 = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
        double dot02 = v0.x * v2.x + v0.y * v2.y + v0.z * v2.z;
        double dot11 = v1.x * v1.x + v1.y * v1.y + v1.z * v1.z;
        double dot12 = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

        //Compute barycentric coordinates
        double denom = 1 / (dot00 * dot11 - dot01 * dot01);
        double u = (dot11 * dot02 - dot01 * dot12) * denom;
        double v = (dot00 * dot12 - dot01 * dot02) * denom;

        //Check if point is in triangle
        return (u >= -0.0001f) && (v >= -0.0001f) && (u + v < 1.0001f);
    }

    /// <summary>
    /// Returns true if points are coplanar. epsilon is a positive value. 
    /// </summary>
    /// <param name="t1"></param>
    /// <param name="t2"></param>
    /// <param name="t3"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static bool CoPlanar(Vector3d t1, Vector3d t2, Vector3d t3, Vector3d p, double epsilon = 0.000001f)
    {
        double val = Vector3d.Dot((p - t1), Vector3d.Cross((t3 - t1), (t2 - t1)));
        return val < epsilon && val > -epsilon;
    }

    /// <summary>
    /// Returns the closest distance from a line segment and a point
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static double LineSegmentPointDistance(Vector3d s0, Vector3d s1, Vector3d P)
    {
        // Copyright 2001 softSurfer, 2012 Dan Sunday
        // This code may be freely used, distributed and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        Vector3d v = s1 - s0;
        Vector3d w = P - s0;

        double c1 = Vector3d.Dot(w, v);
        if (c1 <= 0)
            return Vector3d.Distance(P, s0);

        double c2 = Vector3d.Dot(v, v);
        if (c2 <= c1)
            return Vector3d.Distance(P, s1);

        double b = c1 / c2;
        Vector3d Pb = s0 + b * v;
        return Vector3d.Distance(P, Pb);
    }

    /// <summary>
    /// Returns the closest squared distance from a line segment and a point
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static double LineSegmentPointDistanceSqrd(Vector3d s0, Vector3d s1, Vector3d P)
    {
        // Copyright 2001 softSurfer, 2012 Dan Sunday
        // This code may be freely used, distributed and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        Vector3d v = s1 - s0;
        Vector3d w = P - s0;

        double c1 = Vector3d.Dot(w, v);
        if (c1 <= 0)
            return (P - s0).sqrMagnitude;

        double c2 = Vector3d.Dot(v, v);
        if (c2 <= c1)
            return (P - s1).sqrMagnitude;

        double b = c1 / c2;
        Vector3d Pb = s0 + b * v;
        return (P - Pb).sqrMagnitude;
    }

    /// <summary>
    /// Returns true if input planes are parallel. Epsilon is a positive number
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="epsilon"></param>
    /// <returns></returns>
    public static bool ParallelPlanes(Plane p1, Plane p2, double epsilon = 0.0000001f)
    {
        Vector3d lineDir = Vector3d.Cross(p1.normal, p2.normal);
        //Determine if the cross product yielded (0,0,0) - parallel
        if (lineDir.x < epsilon && lineDir.x > -epsilon &&
            lineDir.y < epsilon && lineDir.y > -epsilon &&
            lineDir.z < epsilon && lineDir.z > -epsilon)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Line segment intersect bbox? Does not include improper intersections with edges or corners
    /// </summary>
    /// <param name="b"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static bool LineBBoxIntersect(BBox b, Vector3d start, Vector3d end)
    {
        return linebboxintersect(b, new Ray_ex(start, (end - start).normalized), 0, (start - end).magnitude);
    }
    /// <summary>
    /// Ray intersect bbox? Does not include improper intersections with edges or corners
    /// </summary>
    /// <param name="b"></param>
    /// <param name="ray"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static bool RayBBoxIntersect(BBox b, Ray_ex ray, double length)
    {
        return linebboxintersect(b, new Ray_ex(ray.origin, ray.direction), 0, length);
    }
    static bool linebboxintersect(BBox b, Ray_ex r, double t0, double t1)
    {
        //based on http://people.csail.mit.edu/amy/papers/box-jgt.pdf

        double tmin, tmax, tymin, tymax, tzmin, tzmax;

        tmin = ((r.sign.x == 1 ? b.Max : b.Min).x - r.origin.x) * r.inv_direction.x;
        tmax = ((1 - r.sign.x == 1 ? b.Max : b.Min).x - r.origin.x) * r.inv_direction.x;
        tymin = ((r.sign.y == 1 ? b.Max : b.Min).y - r.origin.y) * r.inv_direction.y;
        tymax = ((1 - r.sign.y == 1 ? b.Max : b.Min).y - r.origin.y) * r.inv_direction.y;

        if (tmin > tymax || tymin > tmax)
            return false;

        if (tymin > tmin)
            tmin = tymin;

        if (tymax < tmax)
            tmax = tymax;

        tzmin = ((r.sign.z == 1 ? b.Max : b.Min).z - r.origin.z) * r.inv_direction.z;
        tzmax = ((1 - r.sign.z == 1 ? b.Max : b.Min).z - r.origin.z) * r.inv_direction.z;

        if (tmin > tzmax || tzmin > tmax)
            return false;

        if (tzmin > tmin)
            tmin = tzmin;

        if (tzmax<tmax)
            tmax = tzmax;

        return tmin <= t1 && tmax >= t0;
    }

    /// <summary>
    /// Surface intersection. Returns true if intersected and outs intersection.
    /// Does not include coplanar intersections. Returns point where line first intersects bbox along 'dir'.
    /// Intersection is with a line and an axis aligned bounding box.
    /// </summary>
    /// <param name="b"></param>
    /// <param name="point"></param>
    /// <param name="dir"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static bool LineBBoxIntersect(BBox b, Vector3d point, Vector3d dir, 
        out Vector3d intersection, out Vector3d plane_normal)
    {
        var choose_3 = dir;
        var min = b.Min; var max = b.Max;
        //check x-direction side
        if (choose_3.x > 0)
        {
            //check -x side of bbox
            if (LinePlaneIntersection(out intersection, point, dir, Vector3d.left, min))
            {
                if (intersection.y >= min.y && intersection.y <= max.y && intersection.z >= min.z &&
                    intersection.z <= max.z)
                {
                    plane_normal = Vector3d.left;
                    return true;
                }
            }
        }
        else
        {
            //check +x side of bbox
            if (LinePlaneIntersection(out intersection, point, dir, Vector3d.right, max))
            {
                if (intersection.y >= min.y && intersection.y <= max.y && intersection.z >= min.z &&
                    intersection.z <= max.z)
                {
                    plane_normal = Vector3d.right;
                    return true;
                }
            }
        }

        //check y-direction side
        if (choose_3.y > 0)
        {
            //check -y side of bbox
            if (LinePlaneIntersection(out intersection, point, dir, Vector3d.down, min))
            {
                if (intersection.x >= min.x && intersection.x <= max.x && intersection.z >= min.z &&
                    intersection.z <= max.z)
                {
                    plane_normal = Vector3d.down;
                    return true;
                }
            }
        }
        else
        {
            //check +y side of bbox
            if (LinePlaneIntersection(out intersection, point, dir, Vector3d.up, max))
            {
                if (intersection.x >= min.x && intersection.x <= max.x && intersection.z >= min.z &&
                    intersection.z <= max.z)
                {
                    plane_normal = Vector3d.up;
                    return true;
                }
            }
        }

        //check z-direction side
        if (choose_3.z > 0)
        {
            //check -z side of bbox
            if (LinePlaneIntersection(out intersection, point, dir, Vector3d.back, min))
            {
                if (intersection.x >= min.x && intersection.x <= max.x && intersection.y >= min.y &&
                    intersection.y <= max.y)
                {
                    plane_normal = Vector3d.back;
                    return true;
                }
            }
        }
        else
        {
            //check +z side of bbox
            if (LinePlaneIntersection(out intersection, point, dir, Vector3d.forward, max))
            {
                if (intersection.x >= min.x && intersection.x <= max.x && intersection.y >= min.y &&
                    intersection.y <= max.y)
                {
                    plane_normal = Vector3d.forward;
                    return true;
                }
            }
        }

        plane_normal = Vector3d.zero;
        return false;
    }

    /// <summary>
    /// Transform a point about the origin. Transforms are translation 'position', Rotation 'rotation', Scale 'scale'
    /// </summary>
    /// <returns></returns>
    public static Vector3d TransformPoint(Vector3d point, Vector3d origin, Vector3d position, Vector3d eulerangles, Vector3d scale)
    {
        //useful: http://inside.mines.edu/fs_home/gmurray/ArbitraryAxisRotation/

        Vector3d s = scale;
        Vector3d r = eulerangles * deg2rad;
        Vector3d p = position;

        double cosx = System.Math.Cos(r.x);
        double sinx = System.Math.Sin(r.x);

        double cosy = System.Math.Cos(r.y);
        double siny = System.Math.Sin(r.y);

        double cosz = System.Math.Cos(r.z);
        double sinz = System.Math.Sin(r.z);

        Vector3d v = point - origin;

        //scale
        v.x *= s.x;
        v.y *= s.y;
        v.z *= s.z;
        Vector3d v2 = v;

        //rotation
        //about z-axis
        v2.x = v.x * cosz - v.y * sinz;
        v2.y = v.x * sinz + v.y * cosz;
        v = v2;

        //y-axis
        v2.x = v.x * cosy - v.z * siny;
        v2.z = v.x * siny + v.z * cosy;
        v = v2;

        //about x-axis
        v2.y = v.y * cosx - v.z * sinx;
        v2.z = v.y * sinx + v.z * cosx;

        //translate
        v2.x += p.x;
        v2.y += p.y;
        v2.z += p.z;

        return v2 + origin;
    }

    /// <summary>
    /// CLosest point on a line
    /// </summary>
    /// <param name="l1"></param>
    /// <param name="l2"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static Vector3d ClosestPointOnLine(Vector3d l1, Vector3d l2, Vector3d p)
    {
        var a = l2 - l1;
        var t = (-a.x * (l1.x - p.x) - a.y * (l1.y - p.y) - a.z * (l1.z - p.z)) / 
            (a.x * a.x + a.y * a.y + a.z * a.z);
        return l1 + t * a;
    }
}

/// <summary>
/// Extended plane.
/// </summary>
public struct Plane_ex
{
    /// <summary>
    /// Contained plane
    /// </summary>
    public Plane plane;
    /// <summary>
    /// Some point on the plane.
    /// </summary>
    public Vector3d v1;

    public Plane_ex(Plane p, Vector3d p1)
    {
        plane = p;
        v1 = p1;
    }
}

/// <summary>
/// Extended Ray
/// </summary>
public struct Ray_ex
{
    public Ray_ex(Vector3d _origin, Vector3d _direction)
    {
        origin = _origin;
        direction = _direction;
        inv_direction = new Vector3d(1 / _direction.x, 1 / _direction.y, 1 / _direction.z);
    }
    public Ray_ex(Ray r)
    {
        origin = r.origin;
        direction = r.direction;
        inv_direction = new Vector3d(1 / r.direction.x, 1 / r.direction.y, 1 / r.direction.z);
    }
    public Vector3d origin;
    public Vector3d direction;
    public Vector3d inv_direction;
    public Vector3Int sign
    {
        get
        {
            return new Vector3Int(inv_direction.x < 0 ? 1 : 0, inv_direction.y < 0 ? 1 : 0, inv_direction.z < 0 ? 1 : 0);
        }
    }
}