﻿using UnityEngine;
using System;
using System.Collections.Generic;


/// <summary>
/// Axis aligned bounding box
/// </summary>
[System.Serializable]
public struct BBox : IEquatable<BBox>
{
    /// <summary>
    /// Point of lowest magnitude on bbox. Position == Min
    /// </summary>
    public Vector3d Position;
    /// <summary>
    /// Length x Width x Height in bbox
    /// </summary>
    public Vector3d Size;

    /// <summary>
    /// Position == Min
    /// </summary>
    public Vector3d Min
    {
        get
        {
            return Position;
        }
    }
    /// <summary>
    /// Position + Size
    /// </summary>
    public Vector3d Max
    {
        get
        {
            return new Vector3d(Position.x + Size.x, Position.y + Size.y, Position.z + Size.z);
        }
    }
    /// <summary>
    /// Position + Size / 2
    /// </summary>
    public Vector3d Center
    {
        get
        {
            return Min + Size / 2;
        }
        set
        {
            var delta = Center - value;
            Position -= delta;
        }
    }

    public double Volume
    {
        get
        {
            return Size.x * Size.y * Size.z;
        }
    }

    /// <summary>
    /// Sphere that encapsulates this bbox
    /// </summary>
    public Sphere BoundingSphere
    {
        get
        {
            return new Sphere(Position, (Max - Min).magnitude);
        }
    }

    public BBox(Vector3d pos, Vector3d size)
    {
        Position = pos; Size = size;
    }

    public bool Equals(BBox bbox)
    {
        return Position == bbox.Position && Size == bbox.Size;
    }
    public override bool Equals(object obj)
    {
        return obj is BBox ? Equals((BBox)obj) : false;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 + Position.GetHashCode();
            hash = hash * 17 + Size.GetHashCode();
            return hash;
        }
    }
    public static bool operator ==(BBox b1, BBox b2)
    {
        return b1.Equals(b2);
    }
    public static bool operator !=(BBox b1, BBox b2)
    {
        return !b1.Equals(b2);
    }

    /// <summary>
    /// Returns true if the input bbox intersects or contains this bbox
    /// </summary>
    /// <param name="b1"></param>
    /// <returns></returns>
    public bool Intersects(BBox b1)
    {
        if (Min.x > b1.Max.x) return false;
        if (Max.x < b1.Min.x) return false;
        if (Min.y > b1.Max.y) return false;
        if (Max.y < b1.Min.y) return false;
        if (Min.z > b1.Max.z) return false;
        if (Max.z < b1.Min.z) return false;
        return true;
    }
    /// <summary>
    /// Returns true if the input point is inside the bbox
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool Contains(Vector3d point)
    {
        return point.x >= Min.x && point.x <= Max.x &&
            point.y >= Min.y && point.y <= Max.y &&
            point.z <= Max.z && point.z >= Min.z;
    }
    /// <summary>
    /// Returns true if the input point is inside the bbox. Returns false if the input point is on the surface.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool StrictContains(Vector3d point)
    {
        return point.x > Min.x && point.x < Max.x &&
            point.y > Min.y && point.y < Max.y &&
            point.z < Max.z && point.z > Min.z;
    }
    /// <summary>
    /// Returns true if this bbox can contain the input bbox
    /// </summary>
    /// <param name="b1"></param>
    /// <returns></returns>
    public bool Contains(BBox b1)
    {
        return b1.Min.x >= Min.x && b1.Max.x <= Max.x &&
               b1.Min.y >= Min.y && b1.Max.y <= Max.y &&
               b1.Min.z >= Min.z && b1.Max.z <= Max.z;
    }
    /// <summary>
    /// Returns an expanded bbox that fits this bbox and the input bbox
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public BBox Expand(BBox b)
    {
        Vector3d newmin = new Vector3d(System.Math.Min(Min.x, b.Min.x),
                                     System.Math.Min(Min.y, b.Min.y),
                                     System.Math.Min(Min.z, b.Min.z));
        Vector3d newmax = new Vector3d(System.Math.Max(Max.x, b.Max.x),
                                      System.Math.Max(Max.y, b.Max.y),
                                      System.Math.Max(Max.z, b.Max.z));
        Vector3d newsize = newmax - newmin;
        return new BBox(newmin, newsize);
    }
    /// <summary>
    /// Distance between point and bbox
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public double Distance(Vector3d point)
    {
        if (Contains(point)) return 0;
        Vector3d dist_point = point;
        if (point.x < Min.x)
            dist_point.x = Min.x;
        else if (point.x > Max.x)
            dist_point.x = Max.x;
        if (point.y < Min.y)
            dist_point.y = Min.y;
        else if (point.y > Max.y)
            dist_point.y = Max.y;
        if (point.z < Min.z)
            dist_point.z = Min.z;
        else if (point.z > Max.z)
            dist_point.z = Max.z;
        return Vector3d.Distance(dist_point, point);
    }
    /// <summary>
    /// Distance between two bbox
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public double Distance(BBox b)
    {
        Vector3d dist_point = Vector3d.zero;
        Vector3d dist_point2 = Vector3d.zero;
        if (b.Max.x < Min.x)
        {
            dist_point.x = Min.x;
            dist_point2.x = b.Max.x;
        }
        else if (b.Min.x > Max.x)
        {
            dist_point.x = Max.x;
            dist_point2.x = b.Min.x;
        }
        if (b.Max.y < Min.y)
        {
            dist_point.y = Min.y;
            dist_point2.y = b.Max.y;
        }
        else if (b.Min.y > Max.y)
        {
            dist_point.y = Max.y;
            dist_point2.y = b.Min.y;
        }
        if (b.Max.z < Min.z)
        {
            dist_point.z = Min.z;
            dist_point2.z = b.Max.z;
        }
        else if (b.Min.z > Max.z)
        {
            dist_point.z = Max.z;
            dist_point2.z = b.Min.z;
        }
        return Vector3d.Distance(dist_point, dist_point2);
    }
    /// <summary>
    /// Distance squared between point and bbox
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public double DistanceSq(Vector3d point)
    {
        if (Contains(point)) return 0;
        Vector3d dist_point = point;
        if (point.x <= Min.x)
            dist_point.x = Min.x;
        else if (point.x >= Max.x)
            dist_point.x = Max.x;
        if (point.y <= Min.y)
            dist_point.y = Min.y;
        else if (point.y >= Max.y)
            dist_point.y = Max.y;
        if (point.z <= Min.z)
            dist_point.z = Min.z;
        else if (point.z >= Max.z)
            dist_point.z = Max.z;
        return (dist_point - point).sqrMagnitude;
    }
    /// <summary>
    /// Distance between point and bbox, input point is assumed to be outside or on the surface of the bbox but not inside
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public double DistanceSq_OUTSIDE(Vector3d point)
    {
        Vector3d min = Min, max = Max;
        var dist_point = point;
        if (point.x <= min.x)
            dist_point.x = min.x;
        else if (point.x >= max.x)
            dist_point.x = max.x;
        if (point.y <= min.y)
            dist_point.y = min.y;
        else if (point.y >= max.y)
            dist_point.y = max.y;
        if (point.z <= min.z)
            dist_point.z = min.z;
        else if (point.z >= max.z)
            dist_point.z = max.z;
        return (dist_point - point).sqrMagnitude;
    }
    /// <summary>
    /// Distance squared between two bbox
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public double DistanceSq(BBox b)
    {
        Vector3d dist_point = Vector3d.zero;
        Vector3d dist_point2 = Vector3d.zero;
        if (b.Max.x < Min.x)
        {
            dist_point.x = Min.x;
            dist_point2.x = b.Max.x;
        }
        else if (b.Min.x > Max.x)
        {
            dist_point.x = Max.x;
            dist_point2.x = b.Min.x;
        }
        if (b.Max.y < Min.y)
        {
            dist_point.y = Min.y;
            dist_point2.y = b.Max.y;
        }
        else if (b.Min.y > Max.y)
        {
            dist_point.y = Max.y;
            dist_point2.y = b.Min.y;
        }
        if (b.Max.z < Min.z)
        {
            dist_point.z = Min.z;
            dist_point2.z = b.Max.z;
        }
        else if (b.Min.z > Max.z)
        {
            dist_point.z = Max.z;
            dist_point2.z = b.Min.z;
        }
        return (dist_point2 - dist_point).sqrMagnitude;
    }
    /// <summary>
    /// Returns true if this bbox contains the input bbox without any adge,corner,or surface intersections
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public bool StrictContains(BBox b1)
    {
        return b1.Min.x > Min.x && b1.Max.x < Max.x &&
               b1.Min.y > Min.y && b1.Max.y < Max.y &&
               b1.Min.z > Min.z && b1.Max.z < Max.z;
    }
    /// <summary>
    /// Intersects sphere?
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public bool IntersectsSphere(Sphere s)
    {
        return s.IntersectsBBox(new BBox(Position, Size));
    }
    /// <summary>
    /// Intersects capsule?
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public bool IntersectsCapsule(Capsule c)
    {
        return c.IntersectsBBox(new BBox(Position, Size));
    }
    /// <summary>
    /// Intersects box?
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public bool IntersectsBox(Box b)
    {
        return b.IntersectsBBox(new BBox(Position, Size));
    }
    /// <summary>
    /// Intersects Polygon3D
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public bool IntersectsPolygon3D(Polygon3D p)
    {
        if (p == null)
            throw new System.Exception("null polygon exception");
        return p.IntersectsBBox(new BBox(Position, Size));
    }

    /// <summary>
    /// Returns the normal of a bbox face that is closest to the input point.
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public Vector3Int ClosestNormalToPoint(Vector3d point)
    {
        var min = Min;
        var max = Max;
        var d_x_pos = Math.Abs(max.x - point.x);
        var d_x_neg = Math.Abs(min.x - point.x);
        var d_y_pos = Math.Abs(point.y - max.y);
        var d_y_neg = Math.Abs(point.y - min.y);
        var d_z_pos = Math.Abs(max.z - point.z);
        var d_z_neg = Math.Abs(min.z - point.z);
        Vector3d chosen = Vector3d.zero;
        Vector3Int dir = Vector3Int.zero;
        chosen.x = d_x_pos < d_x_neg ? d_x_pos : d_x_neg;
        chosen.y = d_y_pos < d_y_neg ? d_y_pos : d_y_neg;
        chosen.z = d_z_pos < d_z_neg ? d_z_pos : d_z_neg;
        dir.x = d_x_pos < d_x_neg ? 1 : -1;
        dir.y = d_y_pos < d_y_neg ? 1 : -1;
        dir.z = d_z_pos < d_z_neg ? 1 : -1;
        if (chosen.x < chosen.y)
        {
            if (chosen.x < chosen.z)
                return dir.x < 0 ? Vector3Int.left : Vector3Int.right;
            else
                return dir.z < 0 ? Vector3Int.back : Vector3Int.forward;
        }
        else
        {
            if (chosen.y < chosen.z)
                return dir.y < 0 ? Vector3Int.down : Vector3Int.up;
            else
                return dir.z < 0 ? Vector3Int.back : Vector3Int.forward;
        }
    }

    /// <summary>
    /// Returns a point on the bbox that is closest to the input point. If points being inside the bbox are not
    /// an issue for you, then StrictFitPoint is faster.
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public Vector3d ClosestPointToPoint(Vector3d v)
    {
        if (Contains(v))
        {
            var n = ClosestNormalToPoint(v);
            if (n.x != 0)
                return n.x < 0 ? new Vector3d(Min.x, v.y, v.z) : new Vector3d(Max.x, v.y, v.z);
            else if (n.y != 0)
                return n.y < 0 ? new Vector3d(v.x, Min.y, v.z) : new Vector3d(v.x, Max.y, v.z);
            else
                return n.z < 0 ? new Vector3d(v.x, v.y, Min.z) : new Vector3d(v.x, v.y, Max.z);
        }
        else
        {
            if (v.x >= Max.x)
                v.x = Max.x;
            else if (v.x <= Min.x)
                v.x = Min.x;
            if (v.y >= Max.y)
                v.y = Max.y;
            else if (v.y <= Min.y)
                v.y = Min.y;
            if (v.z >= Max.z)
                v.z = Max.z;
            else if (v.z <= Min.z)
                v.z = Min.z;
            return v;
        }
    }

    /// <summary>
    /// Fits the input vector to be inside this bbox within an distance 'e'. e=0 will put the point on the surface
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public Vector3d StrictFitPoint(Vector3d v, double e = 0.0000000001)
    {
        if (v.x >= Max.x)
            v.x = Max.x - e;
        else if (v.x <= Min.x)
            v.x = Min.x + e;
        if (v.y >= Max.y)
            v.y = Max.y - e;
        else if (v.y <= Min.y)
            v.y = Min.y + e;
        if (v.z >= Max.z)
            v.z = Max.z - e;
        else if (v.z <= Min.z)
            v.z = Min.z + e;
        return v;
    }

    public override string ToString()
    {
        return "Pos: " + Position.ToString() + " Size: " + Size.ToString();
    }

    /// <summary>
    /// Return VEctor3[] array containing the points of this bbox
    /// </summary>
    public Vector3d[] VertCopy
    {
        get
        {
            Vector3d[] v = new Vector3d[6];
            v[0] = Min;
            v[1] = new Vector3d(Min.x, Min.y, Min.z + Size.z);
            v[2] = new Vector3d(Min.x, Min.y + Size.y, Min.z);
            v[3] = new Vector3d(Min.x, Min.y + Size.y, Min.z + Size.z);
            v[4] = new Vector3d(Min.x + Size.x, Min.y, Min.z);
            v[5] = new Vector3d(Min.x + Size.x, Min.y, Min.z + Size.z);
            v[6] = new Vector3d(Min.x + Size.x, Min.y + Size.y, Min.z);
            v[7] = Max;
            return v;
        }
    }

    /// <summary>
    /// All vertices in the bbox
    /// </summary>
    public IEnumerable<Vector3d> Verts
    {
        get
        {
            Vector3d v = new Vector3d();
            v = Min;
            yield return v;
            v = new Vector3d(Min.x, Min.y, Min.z + Size.z);
            yield return v;
            v = new Vector3d(Min.x, Min.y + Size.y, Min.z);
            yield return v;
            v = new Vector3d(Min.x, Min.y + Size.y, Min.z + Size.z);
            yield return v;
            v = new Vector3d(Min.x + Size.x, Min.y, Min.z);
            yield return v;
            v = new Vector3d(Min.x + Size.x, Min.y, Min.z + Size.z);
            yield return v;
            v = new Vector3d(Min.x + Size.x, Min.y + Size.y, Min.z);
            yield return v;
            v = Max;
            yield return v;
        }
    }

    public bool valid
    {
        get
        {
            return Size.x > 0 && Size.y > 0 && Size.z > 0;
        }
    }

    public Vector3d p0
    {
        get
        {
            return Min;
        }
    }
    public Vector3d p1
    {
        get
        {
            return new Vector3d(Min.x, Min.y, Min.x + Size.z);
        }
    }
    public Vector3d p2
    {
        get
        {
            return new Vector3d(Min.x, Min.y + Size.y, Min.x);
        }
    }
    public Vector3d p3
    {
        get
        {
            return new Vector3d(Min.x, Min.y + Size.y, Min.x + Size.z);
        }
    }
    public Vector3d p4
    {
        get
        {
            return new Vector3d(Min.x + Size.x, Min.y, Min.x);
        }
    }
    public Vector3d p5
    {
        get
        {
            return new Vector3d(Min.x + Size.x, Min.y, Min.x + Size.z);
        }
    }
    public Vector3d p6
    {
        get
        {
            return new Vector3d(Min.x + Size.x, Min.y + Size.y, Min.x);
        }
    }
    public Vector3d p7
    {
        get
        {
            return Max;
        }
    }
}
