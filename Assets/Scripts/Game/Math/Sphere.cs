﻿using UnityEngine;
using System;

public class Sphere : IEquatable<Sphere>
{
    /// <summary>
    /// Center == Position
    /// </summary>
    public Vector3d Center;
    /// <summary>
    /// Center == Position
    /// </summary>
    public Vector3d Position
    {
        get
        {
            return Center;
        }
        set
        {
            Center = value;
        }
    }
    /// <summary>
    /// Radius of sphere
    /// </summary>
    public double Radius = 1;

    public Sphere(Vector3d center, double radius)
    {
        Center = center;
        Radius = radius;
    }

    public bool Equals(Sphere s)
    {
        return Center == s.Center && Radius == s.Radius;
    }
    public override bool Equals(object obj)
    {
        return obj is Sphere ? Equals((Sphere)obj) : false;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 * Center.GetHashCode();
            hash = hash * 17 + Radius.GetHashCode();
            return hash;
        }
    }

    /// <summary>
    /// Intersects BBox?
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public bool IntersectsBBox(BBox b)
    {
        double dist = Radius;
        if (Center.x < b.Min.x) dist -= Math.Abs(Center.x - b.Min.x);
        else if (Center.x > b.Max.x) dist -= Math.Abs(Center.x - b.Max.x);
        if (Center.y < b.Min.y) dist -= Math.Abs(Center.y - b.Min.y);
        else if (Center.y > b.Max.y) dist -= Math.Abs(Center.y - b.Max.y);
        if (Center.z < b.Min.z) dist -= Math.Abs(Center.z - b.Min.z);
        else if (Center.z > b.Max.z) dist -= Math.Abs(Center.z - b.Max.z);
        return dist >= 0;
    }
    /// <summary>
    /// Intersects sphere?
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public bool IntersectsSphere(Sphere s)
    {
        return (s.Center - Center).sqrMagnitude <= (Radius + s.Radius) * (Radius + s.Radius);
    }
    /// <summary>
    /// Intersects box?
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public bool IntersectsBox(Box b)
    {
        return b.IntersectsSphere(new Sphere(Center, Radius));
    }
    /// <summary>
    /// Intersects capsule?
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    public bool IntersectsCapsule(Capsule c)
    {
        return c.IntersectsSphere(new Sphere(Center, Radius));
    }
    /// <summary>
    /// Intersects polygon?
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public bool IntersectsPolygon3D(Polygon3D p)
    {
        if (p == null)
            throw new System.Exception("Null polygon error");
        return p.IntersectsSphere(new Sphere(Center, Radius));
    }

    public override string ToString()
    {
        return "Sphere: r" + Radius.ToString() + ", center: " + Center.ToString();
    }
}
