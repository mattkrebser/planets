﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// A transform describing orientation of an object
/// </summary>
public class Transformd
{
    public Vector3d Position
    {
        get
        {
            return pos;
        }
        set
        {
            var prev = pos;
            pos = value;
            if (OnModifyPositionEvent != null && prev != value)
                OnModifyPositionEvent(prev);
        }
    }
    Vector3d pos;
    public Quaterniond Rotation
    {
        get
        {
            return rot;
        }
        set
        {
            var prev = rot;
            rot = value;
            if (OnModifyRotationEvent != null && prev != value)
                OnModifyRotationEvent(rot);
        }
    }
    Quaterniond rot;
    public Vector3d Scale
    {
        get
        {
            return scale;
        }
        set
        {
            var prev = scale;
            scale = value;
            if (OnModifyScaleEvent != null && prev != value)
                OnModifyScaleEvent(prev);
        }
    }
    Vector3d scale;

    /// <summary>
    /// Pointer to an Action that will be invoked whenever this transform position is modified
    /// </summary>
    public Action<Vector3d> OnModifyPositionEvent
    {
        private get { return me; }
        set
        {
            if (me != null)
                throw new System.Exception("Error, event can only be initialized once.");
            me = value;
        }
    }
    Action<Vector3d> me;

    /// <summary>
    /// Pointer to an Action that will be invoked whenever this transform rotation is modified
    /// </summary>
    public Action<Quaterniond> OnModifyRotationEvent
    {
        private get { return me2; }
        set
        {
            if (me2 != null)
                throw new System.Exception("Error, event can only be initialized once.");
            me2 = value;
        }
    }
    Action<Quaterniond> me2;

    /// <summary>
    /// Pointer to an Action that will be invoked whenever this transform scale is modified
    /// </summary>
    public Action<Vector3d> OnModifyScaleEvent
    {
        private get { return me3; }
        set
        {
            if (me3 != null)
                throw new System.Exception("Error, event can only be initialized once.");
            me3 = value;
        }
    }
    Action<Vector3d> me3;

    public Transformd() { }
    public Transformd(Transform t)
    {
        Position = t.position;
        Rotation = t.rotation;
        Scale = t.localScale;
    }

    /// <summary>
    /// Update position without raising event
    /// </summary>
    /// <param name="position"></param>
    public void SilentUpdatePosition(Vector3d position)
    {
        pos = position;
    }
    /// <summary>
    /// update rotation withut raising event
    /// </summary>
    /// <param name="rotation"></param>
    public void SilentUpdateRotation(Quaterniond rotation)
    {
        rot = rotation;
    }
    /// <summary>
    /// Update scale without raising event
    /// </summary>
    /// <param name="s"></param>
    public void SilentUpdateScale(Vector3d s)
    {
        scale = s;
    }
}
