﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public struct Vector3Int : IEquatable<Vector3Int>
{
    public int x;
    public int y;
    public int z;

    public Vector3Int(Vector3 v)
    {
        x = (int)v.x; y = (int)v.y; z = (int)v.z;
    }
    public Vector3Int( int X, int Y, int Z)
    {
        x = X; y = Y; z = Z;
    }
    public Vector3 ToVector3
    {
        get
        {
            return new Vector3(x, y, z);
        }
    }

    public static Vector3Int zero
    {
        get
        {
            return new Vector3Int(0, 0, 0);
        }
    }

    public bool Equals(Vector3Int v)
    {
        return v.x == x && v.y == y && v.z == z;
    }
    public override bool Equals(object obj)
    {
        return obj is Vector3Int ? Equals((Vector3Int)obj) : false;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 + x.GetHashCode();
            hash = hash * 19 + y.GetHashCode();
            hash = hash * 19 + z.GetHashCode();
            return hash;
        }
    }
    public static bool operator==(Vector3Int v1, Vector3Int v2)
    {
        return v1.Equals(v2);
    }
    public static bool operator!=(Vector3Int v1, Vector3Int v2)
    {
        return !(v1 == v2);
    }

    public static Vector3Int operator -(Vector3Int a, Vector3Int b)
    {
        return new Vector3Int(a.x - b.x, a.y - b.y, a.z - b.z);
    }
    public static Vector3Int operator -(Vector3Int a)
    {
        return new Vector3Int(-a.x, -a.y, -a.z);
    }
    public static Vector3Int operator +(Vector3Int a, Vector3Int b)
    {
        return new Vector3Int(a.x + b.x, a.y + b.y, a.z + b.z);
    }
    public static Vector3Int operator *(Vector3Int a, int b)
    {
        return new Vector3Int(a.x * b, a.y * b, a.z * b);
    }
    public static Vector3Int operator *(int b, Vector3Int a)
    {
        return new Vector3Int(a.x * b, a.y * b, a.z * b);
    }
    public static Vector3Int operator /(Vector3Int a, int b)
    {
        return new Vector3Int(a.x / b, a.y / b, a.z / b);
    }

    public static int Dot(Vector3Int a, Vector3Int b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    /// <summary>
    /// Clamp a to be between min and max inclusive
    /// </summary>
    /// <param name="a"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static Vector3Int VectorClamp(Vector3Int a, Vector3Int min, Vector3Int max)
    {
        return new Vector3Int(
            a.x > max.x ? max.x : (a.x < min.x ? min.x : a.x),
            a.y > max.y ? max.y : (a.y < min.y ? min.y : a.y),
            a.z > max.z ? max.z : (a.z < min.z ? min.z : a.z));
    }

    public static Vector3Int forward
    {
        get
        {
            return new Vector3Int(0, 0, 1);
        }
    }
    public static Vector3Int back
    {
        get
        {
            return new Vector3Int(0, 0, -1);
        }
    }
    public static Vector3Int right
    {
        get
        {
            return new Vector3Int(1, 0, 0);
        }
    }
    public static Vector3Int left
    {
        get
        {
            return new Vector3Int(-1, 0, 0);
        }
    }
    public static Vector3Int up
    {
        get
        {
            return new Vector3Int(0, 1, 0);
        }
    }
    public static Vector3Int down
    {
        get
        {
            return new Vector3Int(0, -1, 0);
        }
    }
    /// <summary>
    /// Abosulte value all components
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector3Int Abs(Vector3Int v)
    {
        return new Vector3Int(Math.Abs(v.x), Math.Abs(v.y), Math.Abs(v.z));
    }

    public override string ToString()
    {
        return "Vector3Int(" + x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ")";
    }

}

[System.Serializable]
public struct Vector2Int : IEquatable<Vector2Int>
{
    public int x;
    public int y;

    public Vector2Int(Vector2 v)
    {
        x = (int)v.x; y = (int)v.y;;
    }
    public Vector2Int(int X, int Y)
    {
        x = X; y = Y;
    }
    public Vector2 ToVector2
    {
        get
        {
            return new Vector2(x, y);
        }
    }

    public static Vector2Int zero
    {
        get
        {
            return new Vector2Int(0, 0);
        }
    }

    public bool Equals(Vector2Int v)
    {
        return v.x == x && v.y == y;
    }
    public override bool Equals(object obj)
    {
        return obj is Vector2Int ? Equals((Vector2Int)obj) : false;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 + x.GetHashCode();
            hash = hash * 19 + y.GetHashCode();
            return hash;
        }
    }
    public override string ToString()
    {
        return "Vector2Int(" + x.ToString() + ", " + y.ToString() + ")";
    }
    public static bool operator ==(Vector2Int v1, Vector2Int v2)
    {
        return v1.Equals(v2);
    }
    public static bool operator !=(Vector2Int v1, Vector2Int v2)
    {
        return !(v1 == v2);
    }

    public static Vector2Int operator -(Vector2Int a, Vector2Int b)
    {
        return new Vector2Int(a.x - b.x, a.y - b.y);
    }
    public static Vector2Int operator -(Vector2Int a)
    {
        return new Vector2Int(-a.x, -a.y);
    }
    public static Vector2Int operator +(Vector2Int a, Vector3Int b)
    {
        return new Vector2Int(a.x + b.x, a.y + b.y);
    }
    public static Vector2Int operator *(Vector2Int a, int b)
    {
        return new Vector2Int(a.x * b, a.y * b);
    }
    public static Vector2Int operator *(int b, Vector2Int a)
    {
        return new Vector2Int(a.x * b, a.y * b);
    }
    public static Vector2Int operator /(Vector2Int a, int b)
    {
        return new Vector2Int(a.x / b, a.y / b);
    }
}

public struct Vector3x3
{
    /// <summary>
    /// row 0, col 0 to 2. row indexing is from top to bottom. column indexing is from left to right
    /// </summary>
    public Vector3d v1;
    /// <summary>
    /// row 1 col 0 to 2. row indexing is from top to bottom. column indexing is from left to right
    /// </summary>
    public Vector3d v2;
    /// <summary>
    /// row 2 col 0 to 2. row indexing is from top to bottom. column indexing is from left to right
    /// </summary>
    public Vector3d v3;
}


public struct Vector3Intx2
{
    /// <summary>
    /// row 0, col 0 to 2. row indexing is from top to bottom. column indexing is from left to right
    /// </summary>
    public Vector3Int v1;
    /// <summary>
    /// row 1 col 0 to 2. row indexing is from top to bottom. column indexing is from left to right
    /// </summary>
    public Vector3Int v2;
}

/// <summary>
/// 3 coordinate vector in double precision
/// </summary>
[System.Serializable]
public struct Vector3d : IEquatable<Vector3d>
{
    public double x;
    public double y;
    public double z;

    public Vector3d(Vector3 v)
    {
        x = v.x; y = v.y; z = v.z;
    }
    public Vector3d(double X, double Y, double Z)
    {
        x = X; y = Y; z = Z;
    }

    public static Vector3d zero
    {
        get
        {
            return new Vector3d(0, 0, 0);
        }
    }

    public bool Equals(Vector3d v)
    {
        return v.x == x && v.y == y && v.z == z;
    }
    public override bool Equals(object obj)
    {
        return obj is Vector3d ? Equals((Vector3d)obj) : false;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 + x.GetHashCode();
            hash = hash * 19 + y.GetHashCode();
            hash = hash * 19 + z.GetHashCode();
            return hash;
        }
    }
    public static bool operator ==(Vector3d v1, Vector3d v2)
    {
        return v1.Equals(v2);
    }
    public static bool operator !=(Vector3d v1, Vector3d v2)
    {
        return !(v1 == v2);
    }

    public static Vector3d Cross(Vector3d a, Vector3d b)
    {
        return new Vector3d(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }
    public static double Dot(Vector3d a, Vector3d b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }
    public static Vector3d Cross(Vector3 a, Vector3 b)
    {
        return new Vector3d(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }
    public static double Distance(Vector3d a, Vector3d b)
    {
        return Math.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y) + (b.z - a.z) * (b.z - a.z));
    }
    public static Vector3d Normalize(Vector3d v)
    {
        return v.normalized;
    }
    public static float Dot(Vector3 a, Vector3 b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }
    public override string ToString()
    {
        return "Vector3d(" + x.ToString() + "," + y.ToString() + "," + z.ToString() + ")";
    }
    public Vector3d normalized
    {
        get
        {
            double mag = magnitude;
            return new Vector3d(x / mag, y / mag, z / mag);
        }
    }
    /// <summary>
    /// Get/set magnitude. Setting magnitude will not change direction;
    /// </summary>
    public double magnitude
    {
        get
        {
            return System.Math.Sqrt(x * x + y * y + z * z);
        }
        set
        {
            var v = new Vector3d(x, y, z);
            v = v == Vector3.zero ? v :
            v * Math.Sqrt((value * value) / (v.x * v.x + v.y * v.y + v.z * v.z));
            x = v.x; y = v.y; z = v.z;
        }
    }
    public double sqrMagnitude
    {
        get
        {
            return x * x + y * y + z * z;
        }
    }

    public static Vector3d operator-(Vector3d a, Vector3d b)
    {
        return new Vector3d(a.x - b.x, a.y - b.y, a.z - b.z);
    }
    public static Vector3d operator-(Vector3d a)
    {
        return new Vector3d(-a.x, -a.y, -a.z);
    }
    public static Vector3d operator+(Vector3d a, Vector3d b)
    {
        return new Vector3d(a.x + b.x, a.y + b.y, a.z + b.z);
    }
    public static Vector3d operator*(Vector3d a, double b)
    {
        return new Vector3d(a.x * b, a.y * b, a.z * b);
    }
    public static Vector3d operator*(double b, Vector3d a)
    {
        return new Vector3d(a.x * b, a.y * b, a.z * b);
    }
    public static Vector3d operator/(Vector3d a, double b)
    {
        return new Vector3d(a.x / b, a.y / b, a.z / b);
    }
    /// <summary>
    /// Convert to Vector3 (loose precision)
    /// </summary>
    public Vector3 Vec3
    {
        get
        {
            return new Vector3((float)x, (float)y, (float)z);
        }
    }

    public static Vector3d one
    {
        get
        {
            return new Vector3d(1, 1, 1);
        }
    }

    public static Vector3d forward
    {
        get
        {
            return new Vector3d(0, 0, 1);
        }
    }
    public static Vector3d back
    {
        get
        {
            return new Vector3d(0, 0, -1);
        }
    }
    public static Vector3d right
    {
        get
        {
            return new Vector3d(1, 0, 0);
        }
    }
    public static Vector3d left
    {
        get
        {
            return new Vector3d(-1, 0, 0);
        }
    }
    public static Vector3d up
    {
        get
        {
            return new Vector3d(0, 1, 0);
        }
    }
    public static Vector3d down
    {
        get
        {
            return new Vector3d(0, -1, 0);
        }
    }
    public static explicit operator Vector3(Vector3d v)
    {
        return v.Vec3;
    }
    public static implicit operator Vector3d(Vector3 v)
    {
        return new Vector3d(v);
    }
    public static implicit operator Vector3d(Vector3Int v)
    {
        return new Vector3d(v.x, v.y, v.z);
    }
}

/// <summary>
/// two coordinated double precision vector
/// </summary>
[System.Serializable]
public struct Vector2d
{
    public double x;
    public double y;

    public Vector2d(Vector2 v)
    {
        x = v.x; y = v.y;
    }
    public Vector2d(double X, double Y)
    {
        x = X; y = Y;
    }

    public static Vector2d zero
    {
        get
        {
            return new Vector2d(0, 0);
        }
    }

    public bool Equals(Vector2d v)
    {
        return v.x == x && v.y == y;
    }
    public override bool Equals(object obj)
    {
        return obj is Vector2d ? Equals((Vector2d)obj) : false;
    }
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 + x.GetHashCode();
            hash = hash * 19 + y.GetHashCode();
            return hash;
        }
    }
    public static bool operator ==(Vector2d v1, Vector2d v2)
    {
        return v1.Equals(v2);
    }
    public static bool operator !=(Vector2d v1, Vector2d v2)
    {
        return !(v1 == v2);
    }

    public static double Distance(Vector2d a, Vector2d b)
    {
        return Math.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
    }
    public override string ToString()
    {
        return "Vector3d(" + x.ToString() + "," + y.ToString() + ")";
    }
    public Vector2d normalized
    {
        get
        {
            double mag = magnitude;
            return new Vector2d(x / mag, y / mag);
        }
    }
    public double magnitude
    {
        get
        {
            return System.Math.Sqrt(x * x + y * y);
        }
    }
    public double sqrMagnitude
    {
        get
        {
            return x * x + y * y;
        }
    }

    public static Vector2d operator -(Vector2d a, Vector2d b)
    {
        return new Vector2d(a.x - b.x, a.y - b.y);
    }
    public static Vector2d operator -(Vector2d a)
    {
        return new Vector2d(-a.x, -a.y);
    }
    public static Vector2d operator +(Vector2d a, Vector3d b)
    {
        return new Vector2d(a.x + b.x, a.y + b.y);
    }
    public static Vector2d operator *(Vector2d a, double b)
    {
        return new Vector2d(a.x * b, a.y * b);
    }
    public static Vector2d operator *(double b, Vector2d a)
    {
        return new Vector2d(a.x * b, a.y * b);
    }
    public static Vector2d operator /(Vector2d a, double b)
    {
        return new Vector2d(a.x / b, a.y / b);
    }
    /// <summary>
    /// Convert to Vector3 (loose precision)
    /// </summary>
    public Vector2 Vec2
    {
        get
        {
            return new Vector2((float)x, (float)y);
        }
    }

    public static Vector2d right
    {
        get
        {
            return new Vector2d(1, 0);
        }
    }
    public static Vector2d left
    {
        get
        {
            return new Vector2d(-1, 0);
        }
    }
    public static Vector2d up
    {
        get
        {
            return new Vector2d(0, 1);
        }
    }
    public static Vector2d down
    {
        get
        {
            return new Vector2d(0, -1);
        }
    }
    public static explicit operator Vector2(Vector2d v)
    {
        return v.Vec2;
    }
    public static implicit operator Vector2d(Vector2 v)
    {
        return new Vector2d(v);
    }
}