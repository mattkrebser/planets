﻿using UnityEngine;
using System.Collections;
using System.Linq;

/// <summary>
/// Game Class. Holds state information for the currently running game client.
/// </summary>
public static class Game
{
    public static string GamePath
    {
        get
        {
            return _path;
        }
    }
    static string _path = null;

    public static Space WorldSpace;

    /// <summary>
    /// Load a game saved at the input path
    /// </summary>
    /// <param name="path"></param>
    public static void Load(string path)
    {
        
    }
    /// <summary>
    /// Save this game
    /// </summary>
    public static void Save()
    {

    }
    /// <summary>
    /// Start a new game
    /// </summary>
    public static void NewGame(GenSettings settings)
    {
        //reset space
        if (WorldSpace != null)
            WorldSpace.Clear();
        else
            WorldSpace = new Space();
        //generate space
        WorldSpace.Generate(settings);

        //find the solar system that the player will start in
        //by default it is the SpawnSystem which is always in the same spot.
        var system = WorldSpace.SolarSystems.AllBBoxes.First(x => x.Key.type_name != null &&
            x.Key.type_name.Contains("SpawnSystem")).Key;
        var player = MonoBehaviour.Instantiate(settings.PlayerPrefab);
        player.GetComponent<LocalOrigin>().LocalSystem = system;
        WorldSpace.origins.Add(player.GetComponent<LocalOrigin>());
    }




    /// <summary>
    /// Quit this game
    /// </summary>
    public static void Quit()
    {
        WorldSpace.Clear();
        WorldSpace = null;
    }
}