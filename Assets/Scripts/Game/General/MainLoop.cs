﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Program Entry point for play mode
/// </summary>
public class MainLoop : MonoBehaviour
{
    public static MainLoop Singleton
    {
        get
        {
            return _single;
        }
    }
    static MainLoop _single;

    /// <summary>
    /// Arguments for input
    /// </summary>
    public PlayArguments PlayArguments;

    void Awake()
    {
        if (Singleton == null)
            _single = this;
        else
            Destroy(gameObject);

        StartGame(PlayArguments);
    }

    void OnDestroy()
    {
        _single = null;
    }

    /// <summary>
    /// Start the game.
    /// </summary>
    /// <param name="args"></param>
	public void StartGame(PlayArguments args)
    {
        if (args.Mode == PlayArguments.PlayMode.SinglePlayer)
        {
            if (args.NewGame)
            {
                Game.NewGame(args.Settings);
            }
            else
            {
                Game.Load(args.SavePath);
            }
        }
        else
        {
            //todo
        }
    }

    //main update loop.
    void Update()
    {

    }

    void FixedUpdate()
    {
        //process physics
        Phys.ProcessUpdateQueue();
    }
}

[System.Serializable]
public class PlayArguments
{
    /// <summary>
    /// game save path
    /// </summary>
    public string SavePath;
    /// <summary>
    /// Playmode type
    /// </summary>
    public PlayMode Mode;
    /// <summary>
    /// Maker newgame?
    /// </summary>
    public bool NewGame = false;
    /// <summary>
    /// Settings to play the game on
    /// </summary>
    public GenSettings Settings;
    
    /// <summary>
    /// Playmode type (multiplayer or not, which type of multiplayer)
    /// </summary>
    public enum PlayMode
    {
        SinglePlayer,
        Client,
        Server,
        Host
    }
}
