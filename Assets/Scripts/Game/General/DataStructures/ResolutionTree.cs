﻿using UnityEngine;
using System.Collections.Generic;
using System;
//todo: null dictionaries in resolution tree, this way getting highest resolution layer
//doesnt have to poll non existant chunk dictionaries

    /// <summary>
    /// Container that contains different resolutions of objects.
    /// Adding objects is O(log(n)) time. Access is O(1) time (if resolution is given).
    /// Modify value is O(1) (if resolution is given and node exists already)
    /// Remove Node is O(1) (if resolution is given).. Note Remove node also removes children.
    /// Contains resolution=0....N where '0' is considered maximum.
    /// A resolution is an increase by (*2) power of 2 in voxel length of a chunk.
    /// A resolution of '0', each voxel represents(2^0) blocks.
    /// A resolution of '3', each voxel represents(2^3) blocks.
    /// Although accessing chunks is practically constant time, it is recommended to cache results.
    /// </summary>
    public class ResolutionTree<T>
    {
        //-not really a tree, but sort of. Same time & space complexity as a quadtree (except we have O(1) access to all levels).
        //-each index in the tree (array) represents the resolution of the chunks
        Dictionary<Vector3Int,ResolutionTreeNode<T>>[] tree;

        /// <summary>
        /// Root Node
        /// </summary>
        public ResolutionTreeNode<T> Root { get; private set; }

        /// <summary>
        /// Get value at input position and resolution. Set value at input position and resolution.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public T this[int x, int y, int z, int res]
        {
            get
            {
                Vector3Int pos = new Vector3Int(x, y, z);
                T val;
                TryGetValue(pos, res, out val);
                return val;
            }
            set
            {
                Vector3Int pos = new Vector3Int(x, y, z);
                SetValue(pos, res, value);
            }
        }

        /// <summary>
        /// Get value at input position and resolution. Set value at input position and resolution.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public T this[Vector3Int pos, int res]
        {
            get
            {
                T val;
                TryGetValue(pos, res, out val);
                return val;
            }
            set
            {
                SetValue(pos, res, value);
            }
        }

        /// <summary>
        /// All items from resolution 0...N. This set Will probably contain default(T) values
        /// </summary>
        public IEnumerable<T> AllItems
        {
            get
            {
                if (tree != null)
                    for (int i = 0; i < tree.Length; i++)
                        if (tree[i] != null)
                            foreach (var t in tree[i].Values)
                                if (t != null)
                                    yield return t.Value;
            }
        }

        /// <summary>
        /// In resolution (0)
        /// </summary>
        Vector3Int TreeSize;
        /// <summary>
        /// T Resolution 2^TResolution voxels per chunk (where each voxel has a resolution 0...N)
        /// </summary>
        int TResolution = 0;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="treeSize"> size of the tree in resolution '0'. Resolution '0' is the highest
        /// resolution (best quality resolution). Resolution '1' is lower, '2' even lower, etc... </param>
        /// <param name="chunkResolution"> resolution of a chunk. A typical value is chunkResolution=4.
        /// This means that the chunk has a length of 2^4=16, and represents 16^3 voxels.</param>
        public ResolutionTree(Vector3Int treeSize, int chunkResolution)
        {
            if (chunkResolution == 0)
                throw new System.Exception("Error, 0 chunk resolution is not allowed");
            //find a resolution st. Size the entire voxel data set can be contained in one box
            int Size = Mathf.Max(Mathf.Max(treeSize.x, treeSize.y),treeSize.z);
            int res = 0;
            while (Size != 0)
            {
                Size = Size >> 1;
                res++;
            }
            TreeSize = treeSize;
            TResolution = chunkResolution;
            Root = new ResolutionTreeNode<T>(null, default(T), Vector3Int.zero, res - TResolution, TResolution);
            tree = new Dictionary<Vector3Int, ResolutionTreeNode<T>>[Root.resolution + 1];
            tree[Root.resolution] = new Dictionary<Vector3Int, ResolutionTreeNode<T>>();
            tree[Root.resolution].Add(Vector3Int.zero, Root);
        }

        /// <summary>
        /// Is the input position with the input resolution contained by the resolution map? 
        /// </summary>
        /// <param name="pos"> resolution '0' position of the value </param>
        /// <param name="res"> desired resolution of the value </param>
        /// <returns></returns>
        public bool Contains(Vector3Int pos, int res)
        {
            if (tree[res] == null)
                return false;
            return tree[res].ContainsKey(ToTCoordinates(pos, res));
        }

        /// <summary>
        /// Set the value of a node. If the desired node does not exist, then it is created and
        /// initialized with the input value.
        /// </summary>
        /// <param name="node_pos"> resolution '0' position of the node </param>
        /// <param name="node_res"> desired node resolution </param>
        /// <param name="node"> value to be added </param>
        /// <returns></returns>
        void SetValue(Vector3Int node_pos, int node_res, T value)
        {
            //check current resolution for existing node
            if (tree[node_res] != null)
            {
                ResolutionTreeNode<T> tnode;
                if (tree[node_res].TryGetValue(ToTCoordinates(node_pos, node_res), out tnode))
                {
                    tnode.Value = value;
                    return;
                }
            }

            ResolutionTreeNode<T> current;
            //check immediate parent resolution for existing node
            if (tree[node_res + 1] != null)
            {
                //try to get parent if ti exists, if not, set current=Root
                if (!tree[node_res + 1].TryGetValue(ToTCoordinates(node_pos, node_res + 1), out current))
                    current = Root;
            }
            else
                current = Root;

            //starting at current, search down
            while (node_res < current.resolution)
            {
                current = current.GetCreateNextChild(node_pos, current.resolution - 1,
                    TResolution, current.resolution - 1 == node_res ? value : default(T));
                var current_pos = ToTCoordinates(current.pos, current.resolution);
                if (tree[current.resolution] == null)
                    tree[current.resolution] = new Dictionary<Vector3Int, ResolutionTreeNode<T>>();
                if (!tree[current.resolution].ContainsKey(current_pos))
                    tree[current.resolution].Add(current_pos, current);
            }
            current.Value = value;
        }

        /// <summary>
        /// Try get a value at the input position and resolution. Returns true if a value was found.
        /// Returns false otherwise.
        /// </summary>
        /// <param name="pos"> resolution '0' position of the value </param>
        /// <param name="res"> desired resolution of the value</param>
        /// <returns></returns>
        bool TryGetValue(Vector3Int pos, int res, out T result)
        {
            var t_set = tree[res];
            result = default(T);
            ResolutionTreeNode<T> node;
            if (t_set != null && t_set.TryGetValue(ToTCoordinates(pos, res), out node))
            {
                result = node.Value;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns node value with highest resolution at input position.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="start_res"> start search at this resoluytion. (Highest possible resolution is 0) </param>
        /// <returns></returns>
        public T GetHighestResValue(Vector3Int pos, int start_res = 0)
        {
            if (Root == null)
                throw new System.Exception("Error no root node.");
            T val = default(T);
            while (start_res < Root.resolution)
            {
                if (TryGetValue(pos, start_res, out val))
                {
                    return val;
                }
                start_res++;
            }
            return val;
        }

        /// <summary>
        /// Try to get the node at the input position. Returns true if it exists. False otherwise.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool TryGetNode(Vector3Int pos, int res, out ResolutionTreeNode<T> node)
        {
            var t_set = tree[res];
            if (t_set != null && t_set.TryGetValue(ToTCoordinates(pos, res), out node))
                return true;
            node = null;
            return false;
        }

        /// <summary>
        /// Remove the input value (and node) at the input position and resolution. This also removes
        /// any of the desired node's children.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        public bool RemoveValue(Vector3Int pos, int res)
        {
            ResolutionTreeNode<T> node;
            if (TryGetNode(pos, res, out node))
            {
                if (node.parent == null)
                    throw new System.Exception("Error, node has no parent. Is this the root?");
                node.parent.RemoveNode(node);
                tree[res].Remove(pos);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true if input resolution '0' position is inbounds
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        bool InBounds(Vector3Int v)
        {
            return v.x > -1 && v.y > -1 && v.z > -1 && v.z < TreeSize.x && v.y < TreeSize.y && v.z < TreeSize.z;
        }

        /// <summary>
        /// Clear the tree!
        /// </summary>
        public void Clear()
        {
            if (tree == null)
                return;
            for (int i = 0; i < tree.Length; i++)
                tree[i] = null;
            Root = null;
        }

        /// <summary>
        /// Convert the input value (assumed resolution of 0) to the desired resolution. 
        /// (converting to lower resolutions, which is just dividing by powers of 2)
        /// </summary>
        /// <param name="val"></param>
        /// <param name="res"></param>
        public static int ToRes(int val, int res)
        {
            return val >> res;
        }
        /// <summary>
        /// Convert from old resolution to new resolution.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="oldres"></param>
        /// <param name="newres"></param>
        /// <returns></returns>
        public static int ToRes(int val, int oldres, int newres)
        {
            if (oldres > newres)
            {
                return val << (oldres - newres);
            }
            else if (newres > oldres)
            {
                return val >> (newres - oldres);
            }
            else
            {
                return val;
            }
        }
        /// <summary>
        /// Convert to desried resolution. Input assumed to be at (Resolution 0)
        /// </summary>
        /// <param name="val"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        public static Vector3Int ToRes(Vector3Int val, int res)
        {
            return new Vector3Int(val.x >> res, val.y >> res, val.z >> res);
        }
        /// <summary>
        /// Convert from resolution to resolution
        /// </summary>
        /// <param name="val"></param>
        /// <param name="oldres"></param>
        /// <param name="newres"></param>
        /// <returns></returns>
        public static Vector3Int ToRes(Vector3Int val, int oldres, int newres)
        {
            if (oldres > newres)
            {
                return new Vector3Int(val.x << (oldres - newres), val.y << (oldres - newres), val.z << (oldres - newres));
            }
            else if (newres > oldres)
            {
                return new Vector3Int(val.x >> (newres - oldres), val.y >> (newres - oldres), val.z >> (newres - oldres));
            }
            else
            {
                return val;
            }
        }
        /// <summary>
        /// Convert to coordinates the resolution tree understands. 'pos' is a 0 resolution coordinate, 'res' is the desired resolution level
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        Vector3Int ToTCoordinates(Vector3Int pos, int res)
        {
            return new Vector3Int(pos.x >> (TResolution + res), pos.y >> (TResolution + res), pos.z >> (TResolution + res));
        }

        /// <summary>
        /// Resolution Tree Node
        /// </summary>
        /// <typeparam name="K"></typeparam>
        public class ResolutionTreeNode<K>
        {
            /// <summary>
            /// Resolution 0 position of this node
            /// </summary>
            public Vector3Int pos { get; private set; }
            /// <summary>
            /// actual resolution of this node
            /// </summary>
            public int resolution { get; private set; }
            /// <summary>
            /// value on this node
            /// </summary>
            public K Value;
            /// <summary>
            /// children of this node
            /// </summary>
            ResolutionTreeNode<K>[] children;

            ResolutionTreeNode<K> _parent;
            public ResolutionTreeNode<K> parent
            {
                get
                {
                    return _parent;
                }
            }

            /// <summary>
            /// resolution '0' center of this node.
            /// </summary>
            public Vector3Int center { get; private set; }

            /// <summary>
            /// Create a resolution tree node. Input position expected to be in 'resolution 0'
            /// </summary>
            /// <param name="value"></param>
            /// <param name="position"></param>
            /// <param name="res"></param>
            public ResolutionTreeNode(ResolutionTreeNode<K> Parent, K value, Vector3Int position, int res, int tree_res)
            {
                Value = value;
                pos = ToRes(ToRes(position, res + tree_res), res + tree_res, 0);
                resolution = res;
                int width = 1 << res + tree_res;
                center = new Vector3Int(pos.x + width / 2, pos.y + width / 2, pos.z + width / 2);
                _parent = Parent;
            }

            public ResolutionTreeNode<K> minx_miny_minz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[0];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[0] = value;
                }
            }
            public ResolutionTreeNode<K> minx_miny_maxz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[1];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[1] = value;
                }
            }
            public ResolutionTreeNode<K> minx_maxy_minz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[2];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[2] = value;
                }
            }
            public ResolutionTreeNode<K> minx_maxy_maxz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[3];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[3] = value;
                }
            }
            public ResolutionTreeNode<K> maxx_miny_minz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[4];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[4] = value;
                }
            }
            public ResolutionTreeNode<K> maxx_miny_maxz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[5];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[5] = value;
                }
            }
            public ResolutionTreeNode<K> maxx_maxy_minz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[6];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[6] = value;
                }
            }
            public ResolutionTreeNode<K> maxx_maxy_maxz
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[7];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[8];
                    children[7] = value;
                }
            }

            /// <summary>
            /// Returns the node on the input position on this node. If it doesn't exist, it is created
            /// and then returned. The created node will be constructed with the input 'res','tree_res',
            /// and 'value' parameters. If the node at the input 'position' already exists, then the other
            /// input parameters will have no effect.
            /// </summary>
            /// <param name="pos"></param>
            /// <param name="resolution"></param>
            /// <returns></returns>
            public ResolutionTreeNode<K> GetCreateNextChild(Vector3Int position, int res, int tree_res, K value)
            {
                //we can determine where to get next the node from by just comparing to the center
                //of the the current node
                if (position.x < center.x)
                {
                    if (position.y < center.y)
                    {
                        if (position.z < center.z)
                        {
                            var node = minx_miny_minz;
                            if (node == null)
                                node = minx_miny_minz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                        else
                        {
                            var node = minx_miny_maxz;
                            if (node == null)
                                node = minx_miny_maxz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                    }
                    else
                    {
                        if (position.z < center.z)
                        {
                            var node = minx_maxy_minz;
                            if (node == null)
                                node = minx_maxy_minz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                        else
                        {
                            var node = minx_maxy_maxz;
                            if (node == null)
                                node = minx_maxy_maxz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                    }
                }
                else
                {
                    if (position.y < center.y)
                    {
                        if (position.z < center.z)
                        {
                            var node = maxx_miny_minz;
                            if (node == null)
                                node = maxx_miny_minz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                        else
                        {
                            var node = maxx_miny_maxz;
                            if (node == null)
                                node = maxx_miny_maxz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                    }
                    else
                    {
                        if (position.z < center.z)
                        {
                            var node = maxx_maxy_minz;
                            if (node == null)
                                node = maxx_maxy_minz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                        else
                        {
                            var node = maxx_maxy_maxz;
                            if (node == null)
                                node = maxx_maxy_maxz = 
                                    new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                            return node;
                        }
                    }
                }
            }

            /// <summary>
            /// Remove the input node.
            /// </summary>
            /// <param name="node"></param>
            /// <returns></returns>
            public bool RemoveNode(ResolutionTreeNode<K> remove_node)
            {
                if (remove_node == null)
                    return false;
                Vector3Int position = remove_node.pos;
                //we can determine where to get next the node from by just comparing to the center
                //of the the current node
                if (position.x < center.x)
                {
                    if (position.y < center.y)
                    {
                        if (position.z < center.z)
                        {
                            var node = minx_miny_minz;
                            if (node == null)return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            minx_miny_minz = null;
                            return true;
                        }
                        else
                        {
                            var node = minx_miny_maxz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            minx_miny_maxz = null;
                            return true;
                        }
                    }
                    else
                    {
                        if (position.z < center.z)
                        {
                            var node = minx_maxy_minz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            minx_maxy_minz = null;
                            return true;
                        }
                        else
                        {
                            var node = minx_maxy_maxz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            minx_maxy_maxz = null;
                            return true;
                        }
                    }
                }
                else
                {
                    if (position.y < center.y)
                    {
                        if (position.z < center.z)
                        {
                            var node = maxx_miny_minz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            maxx_miny_minz = null;
                            return true;
                        }
                        else
                        {
                            var node = maxx_miny_maxz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            maxx_miny_maxz = null;
                            return true;
                        }
                    }
                    else
                    {
                        if (position.z < center.z)
                        {
                            var node = maxx_maxy_minz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            maxx_maxy_minz = null;
                            return true;
                        }
                        else
                        {
                            var node = maxx_maxy_maxz;
                            if (node == null) return false;
                            if (node.pos != remove_node.pos ||
                                node.resolution != remove_node.resolution)
                                throw new System.Exception("Error removing node. Node positions do not align.");
                            node._parent = null;
                            maxx_maxy_maxz = null;
                            return true;
                        }
                    }
                }
            }

            /// <summary>
            /// Returns all child and sub child nodes
            /// </summary>
            public IEnumerable<ResolutionTreeNode<K>> AllChildren
            {
                get
                {
                    if (children == null)
                        yield break;

                    for (int i = 0; i < children.Length; i++)
                    {
                        var c = children[i];
                        if (c != null)
                            foreach (var cc in c.allchildren)
                                if (cc != null)
                                    yield return cc;
                    }
                }
            }
            IEnumerable<ResolutionTreeNode<K>> allchildren
            {
                get
                {
                    yield return this;

                    if (children == null)
                        yield break;

                    for (int i = 0; i < children.Length; i++)
                    {
                        var c = children[i];
                        if (c != null)
                            foreach (var cc in c.allchildren)
                                if (cc != null)
                                    yield return cc;
                    }
                }
            }
            /// <summary>
            /// immediate children of this node
            /// </summary>
            public IEnumerable<ResolutionTreeNode<K>> Children
            {
                get
                {
                    if (children == null)
                        yield break;
                    for (int i = 0; i < children.Length; i++)
                        if (children[i] != null)
                            yield return children[i];
                }
            }
        }
    }

    /// <summary>
    /// Container for multiple levels of chunks that contain different resolutions of voxel information.
    /// Contains resolution=0....N where '0' is considered maximum.
    /// A resolution is an increase by (*2) power of 2 in voxel length of a chunk.
    /// A resolution of '0', each voxel represents(2^0) blocks.
    /// A resolution of '3', each voxel represents(2^3) blocks.
    /// The resolution tree only contains one resolution for each (x,y,z) coordinate
    /// </summary>
    public class ResolutionTree2d<T>
    {
        //-not really a tree, but sort of. Same time & space complexity as a quadtree (except we have O(1) access to all levels).
        //-each index in the tree (array) represents the resolution of the chunks
        Dictionary<Vector2Int, ResolutionTreeNode<T>>[] tree;

        /// <summary>
        /// Root Node
        /// </summary>
        public ResolutionTreeNode<T> Root { get; private set; }

        /// <summary>
        /// Get value at input position and resolution. Set value at input position and resolution.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public T this[int x, int y, int res]
        {
            get
            {
                Vector2Int pos = new Vector2Int(x, y);
                T val;
                TryGetValue(pos, res, out val);
                return val;
            }
            set
            {
                Vector2Int pos = new Vector2Int(x, y);
                SetValue(pos, res, value);
            }
        }

        /// <summary>
        /// Get value at input position and resolution. Set value at input position and resolution.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public T this[Vector2Int pos, int res]
        {
            get
            {
                T val;
                TryGetValue(pos, res, out val);
                return val;
            }
            set
            {
                SetValue(pos, res, value);
            }
        }

        /// <summary>
        /// All items from resolution 0...N. Will probably return return default(T)
        /// </summary>
        public IEnumerable<T> AllItems
        {
            get
            {
                if (tree != null)
                    for (int i = 0; i < tree.Length; i++)
                        if (tree[i] != null)
                            foreach (var t in tree[i].Values)
                                if (t != null)
                                yield return t.Value;
            }
        }

        Vector2Int TreeSize;
        int TResolution = 0;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="treeSize"> size of the tree in resolution '0' </param>
        /// <param name="chunkResolution"> resolution of a chunk. A typical value is chunkResolution=4.
        /// This means that the chunk has a length of 2^4=16, and represents 16^3 voxels.</param>
        public ResolutionTree2d(Vector2Int treeSize, int chunkResolution)
        {
            if (chunkResolution == 0)
                throw new System.Exception("Error, 0 chunk resolution is not allowed");
            //find a resolution st. Size the entire voxel data set can be contained in one box
            int Size = Mathf.Max(treeSize.x, treeSize.y);
            int res = 0;
            while (Size != 0)
            {
                Size = Size >> 1;
                res++;
            }
            TreeSize = treeSize;
            TResolution = chunkResolution;
            Root = new ResolutionTreeNode<T>(null, default(T), Vector2Int.zero, res - TResolution, TResolution);
            tree = new Dictionary<Vector2Int, ResolutionTreeNode<T>>[Root.resolution + 1];
            tree[Root.resolution] = new Dictionary<Vector2Int, ResolutionTreeNode<T>>();
            tree[Root.resolution].Add(Vector2Int.zero, Root);
        }

        /// <summary>
        /// Is the input position with the input resolution contained by the resolution map? 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        public bool Contains(Vector2Int pos, int res)
        {
            if (tree[res] == null)
                return false;
            return tree[res].ContainsKey(ToTCoordinates(pos, res));
        }

        /// <summary>
        /// Set the value of a node. If the desired node does not exist, then it is created and
        /// initialized with the input value.
        /// </summary>
        /// <param name="node_pos"> resolution '0' position of the node </param>
        /// <param name="node_res"> desired node resolution </param>
        /// <param name="node"> value to be added </param>
        /// <returns></returns>
        void SetValue(Vector2Int node_pos, int node_res, T value)
        {
            //check current resolution for value
            if (tree[node_res] != null)
            {
                ResolutionTreeNode<T> tnode;
                if (tree[node_res].TryGetValue(ToTCoordinates(node_pos, node_res), out tnode))
                {
                    tnode.Value = value;
                    return;
                }
            }

            ResolutionTreeNode<T> current;
            //check immediate parent resolution for existing node
            if (tree[node_res + 1] != null)
            {
                //try to get parent if ti exists, if not, set current=Root
                if (!tree[node_res + 1].TryGetValue(ToTCoordinates(node_pos, node_res + 1), out current))
                    current = Root;
            }
            else
                current = Root;

            while (node_res < current.resolution)
            {
                current = current.GetCreateNextChild(node_pos, current.resolution - 1,
                    TResolution, current.resolution - 1 == node_res ? value : default(T));
                var current_pos = ToTCoordinates(current.pos, current.resolution);
                if (tree[current.resolution] == null)
                    tree[current.resolution] = new Dictionary<Vector2Int, ResolutionTreeNode<T>>();
                if (!tree[current.resolution].ContainsKey(current_pos))
                    tree[current.resolution].Add(current_pos, current);
            }
            current.Value = value;
        }

        /// <summary>
        /// Try get a value at the input position and resolution. Returns true if a value was found.
        /// Returns false otherwise.
        /// </summary>
        /// <param name="pos"> resolution '0' position of the value </param>
        /// <param name="res"> desired resolution of the value</param>
        /// <returns></returns>
        bool TryGetValue(Vector2Int pos, int res, out T result)
        {
            var t_set = tree[res];
            result = default(T);
            ResolutionTreeNode<T> node;
            if (t_set != null && t_set.TryGetValue(ToTCoordinates(pos, res), out node))
            {
                result = node.Value;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to get the node at the input position. Returns true if it exists. False otherwise.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool TryGetNode(Vector2Int pos, int res, out ResolutionTreeNode<T> node)
        {
            var t_set = tree[res];
            if (t_set != null && t_set.TryGetValue(ToTCoordinates(pos, res), out node))
                return true;
            node = null;
            return false;
        }

        /// <summary>
        /// Remove the input value (and node) at the input position and resolution. This also removes
        /// any of the desired node's children.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        public bool RemoveValue(Vector2Int pos, int res)
        {
            ResolutionTreeNode<T> node;
            if (TryGetNode(pos, res, out node))
            {
                if (node.parent == null)
                    throw new System.Exception("Error, node has no parent. Is this the root?");
                node.parent.RemoveNode(node);
                tree[res].Remove(pos);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Position in bounds?
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        bool InBounds(Vector2Int v)
        {
            return v.x > -1 && v.x < TreeSize.x && v.y > -1 && v.y < TreeSize.y;
        }

        /// <summary>
        /// Clear the tree!
        /// </summary>
        public void Clear()
        {
            if (tree == null)
                return;
            for (int i = 0; i < tree.Length; i++)
                tree[i] = null;
            Root = null;
        }

        /// <summary>
        /// Convert the input value (assumed resolution of 0) to the desired resolution. 
        /// (converting to lower resolutions, which is just dividing by powers of 2)
        /// </summary>
        /// <param name="val"></param>
        /// <param name="res"></param>
        public static int ToRes(int val, int res)
        {
            return val >> res;
        }
        /// <summary>
        /// Convert from old resolution to new resolution.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="oldres"></param>
        /// <param name="newres"></param>
        /// <returns></returns>
        public static int ToRes(int val, int oldres, int newres)
        {
            if (oldres > newres)
            {
                return val << (oldres - newres);
            }
            else if (newres > oldres)
            {
                return val >> (newres - oldres);
            }
            else
            {
                return val;
            }
        }
        /// <summary>
        /// Convert to desried resolution. Input assumed to be at (Resolution 0)
        /// </summary>
        /// <param name="val"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        public static Vector2Int ToRes(Vector2Int val, int res)
        {
            return new Vector2Int(val.x >> res, val.y >> res);
        }
        /// <summary>
        /// Convert from resolution to resolution
        /// </summary>
        /// <param name="val"></param>
        /// <param name="oldres"></param>
        /// <param name="newres"></param>
        /// <returns></returns>
        public static Vector2Int ToRes(Vector2Int val, int oldres, int newres)
        {
            if (oldres > newres)
            {
                return new Vector2Int(val.x << (oldres - newres), val.y << (oldres - newres));
            }
            else if (newres > oldres)
            {
                return new Vector2Int(val.x >> (newres - oldres), val.y >> (newres - oldres));
            }
            else
            {
                return val;
            }
        }
        /// <summary>
        /// Convert to coordinates the resolution tree understands. 'pos' is a 0 resolution coordinate, 'res' is the desired resolution level
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="res"></param>
        /// <returns></returns>
        Vector2Int ToTCoordinates(Vector2Int pos, int res)
        {
            return new Vector2Int(pos.x >> (TResolution + res), pos.y >> (TResolution + res));
        }

        /// <summary>
        /// Resolution Tree Node
        /// </summary>
        /// <typeparam name="K"></typeparam>
        public class ResolutionTreeNode<K>
        {
            /// <summary>
            /// Resolution 0 position of this node
            /// </summary>
            public Vector2Int pos { get; private set; }
            /// <summary>
            /// actual resolution of this node
            /// </summary>
            public int resolution { get; private set; }
            /// <summary>
            /// value on this node
            /// </summary>
            public K Value;
            /// <summary>
            /// children of this node
            /// </summary>
            ResolutionTreeNode<K>[] children;

            /// <summary>
            /// resolution '0' center of this node.
            /// </summary>
            public Vector2Int center { get; private set; }

            ResolutionTreeNode<K> _parent;
            public ResolutionTreeNode<K> parent
            {
                get
                {
                    return _parent;
                }
            }

            /// <summary>
            /// Create a resolution tree node. Input position expected to be in 'resolution 0'
            /// </summary>
            /// <param name="value"></param>
            /// <param name="position"></param>
            /// <param name="res"></param>
            public ResolutionTreeNode(ResolutionTreeNode<K> Parent, K value, Vector2Int position,
                int res, int tree_res)
            {
                Value = value;
                pos = ToRes(ToRes(position, res + tree_res), res + tree_res, 0);
                resolution = res;
                int width = 1 << res + tree_res;
                center = new Vector2Int(pos.x + width / 2, pos.y + width / 2);
                _parent = Parent;
            }

            public ResolutionTreeNode<K> minx_miny
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[0];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[4];
                    children[0] = value;
                }
            }
            public ResolutionTreeNode<K> minx_maxy
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[1];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[4];
                    children[1] = value;
                }
            }
            public ResolutionTreeNode<K> maxx_miny
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[2];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[4];
                    children[2] = value;
                }
            }
            public ResolutionTreeNode<K> maxx_maxy
            {
                get
                {
                    if (children == null)
                        return null;
                    return children[3];
                }
                set
                {
                    if (children == null)
                        children = new ResolutionTreeNode<K>[4];
                    children[3] = value;
                }
            }

            /// <summary>
            /// Returns the node on the input position on this node. If it doesn't exist, it is created
            /// and then returned. The created node will be constructed with the input 'res','tree_res',
            /// and 'value' parameters. If the node at the input 'position' already exists, then the other
            /// input parameters will have no effect.
            /// </summary>
            /// <param name="pos"></param>
            /// <param name="resolution"></param>
            /// <returns></returns>
            public ResolutionTreeNode<K> GetCreateNextChild(Vector2Int position, int res, int tree_res, K value)
            {
                //we can determine where to get next the node from by just comparing to the center
                //of the the current node
                if (position.x < center.x)
                {
                    if (position.y < center.y)
                    {
                        var node = minx_miny;
                        if (node == null)
                            node = minx_miny = new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                        return node;
                    }
                    else
                    {
                        var node = minx_maxy;
                        if (node == null)
                            node = minx_maxy = new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                        return node;
                    }
                }
                else
                {
                    if (position.y < center.y)
                    {
                        var node = maxx_miny;
                        if (node == null)
                            node = maxx_miny = new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                        return node;
                    }
                    else
                    {
                        var node = maxx_maxy;
                        if (node == null)
                            node = maxx_maxy = new ResolutionTreeNode<K>(this, value, position, res, tree_res);
                        return node;
                    }
                }
            }

            /// <summary>
            /// Remove the input node.
            /// </summary>
            /// <param name="node"></param>
            /// <returns></returns>
            public bool RemoveNode(ResolutionTreeNode<K> remove_node)
            {
                if (remove_node == null)
                    return false;
                Vector2Int position = remove_node.pos;
                //we can determine where to get next the node from by just comparing to the center
                //of the the current node
                if (position.x < center.x)
                {
                    if (position.y < center.y)
                    {
                        var node = minx_miny;
                        if (node == null) return false;
                        if (node.pos != remove_node.pos ||
                            node.resolution != remove_node.resolution)
                            throw new System.Exception("Error removing node. Node positions do not align.");
                        node._parent = null;
                        minx_miny = null;
                        return true;
                    }
                    else
                    {
                        var node = minx_maxy;
                        if (node == null) return false;
                        if (node.pos != remove_node.pos ||
                            node.resolution != remove_node.resolution)
                            throw new System.Exception("Error removing node. Node positions do not align.");
                        node._parent = null;
                        minx_maxy = null;
                        return true;
                    }
                }
                else
                {
                    if (position.y < center.y)
                    {
                        var node = maxx_miny;
                        if (node == null) return false;
                        if (node.pos != remove_node.pos ||
                            node.resolution != remove_node.resolution)
                            throw new System.Exception("Error removing node. Node positions do not align.");
                        node._parent = null;
                        maxx_miny = null;
                        return true;
                    }
                    else
                    {
                        var node = maxx_maxy;
                        if (node == null) return false;
                        if (node.pos != remove_node.pos ||
                            node.resolution != remove_node.resolution)
                            throw new System.Exception("Error removing node. Node positions do not align.");
                        node._parent = null;
                        maxx_maxy = null;
                        return true;
                    }
                }
            }

            /// <summary>
            /// Returns all child and sub child nodes
            /// </summary>
            public IEnumerable<ResolutionTreeNode<K>> AllChildren
            {
                get
                {
                    if (children == null)
                        yield break;

                    for (int i = 0; i < children.Length; i++)
                    {
                        var c = children[i];
                        if (c != null)
                            foreach (var cc in c.allchildren)
                                if (cc != null)
                                    yield return cc;
                    }
                }
            }
            IEnumerable<ResolutionTreeNode<K>> allchildren
            {
                get
                {
                    yield return this;

                    if (children == null)
                        yield break;

                    for (int i = 0; i < children.Length; i++)
                    {
                        var c = children[i];
                        if (c != null)
                            foreach (var cc in c.allchildren)
                                if (cc != null)
                                    yield return cc;
                    }
                }
            }
            /// <summary>
            /// immediate children of this node
            /// </summary>
            public IEnumerable<ResolutionTreeNode<K>> Children
            {
                get
                {
                    if (children == null)
                        yield break;
                    for (int i = 0; i < children.Length; i++)
                        if (children[i] != null)
                            yield return children[i];
                }
            }
        }
    }
