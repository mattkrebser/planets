﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;


//Adding new bbox to the tree: The input bbox will recurssivly create bbox nodes until it reaches a bbox that
//is smaller than the tree precision or smaller then the input bbox
//Retreiving bbox from item(T): O(1)
//Adding bbox item: log(N)
//removing bbox item: log(N)
//updating: log(N) if an object migrates to a new containing bbox, other wise O(1)


/// <summary>
/// Tree that can contained arbitrarily positioned unrotated bounding boxes and associate them to objects.
/// </summary>
/// <typeparam name="T"></typeparam>
public class BBoxQuadTree<T> where T : IEquatable<T>
{
    Dictionary<T, Pair<List<TreeNode<T>>,BBox>> items = new Dictionary<T, Pair<List<TreeNode<T>>, BBox>>();
    /// <summary>
    /// Root node
    /// </summary>
    TreeNode<T> Root;
    /// <summary>
    /// Minim box size at lowets level of quadtree.
    /// </summary>
    double Precision = 1;

    /// <summary>
    /// Returns the space this quadtree uses. Returns default(BBox) if unitialized
    /// </summary>
    public BBox Space
    {
        get
        {
            if (Root == null)
                return default(BBox);
            return Root.bbox;
        }
    }

    int node_count = 0;
    /// <summary>
    /// Number of expanded nodes
    /// </summary>
    public int NodeCount
    {
        get
        {
            return node_count;
        }
    }
    /// <summary>
    /// Number of items
    /// </summary>
    public int Count
    {
        get
        {
            return items.Count;
        }
    }

    /// <summary>
    /// define a quadtree that has the input BBox size and position
    /// </summary>
    /// <param name="Space"></param>
    public BBoxQuadTree(BBox Space)
    {
        Root = new TreeNode<T>();
        Root.bbox = Space;
        node_count++;
        Precision = Math.Max(Math.Max(Space.Size.x, Space.Size.y), Space.Size.z) / 1000.0f;
        Precision = Precision < 0.0001f ? 0.0001f : Precision;
    }
    /// <summary>
    /// define a quadtree that has the input BBox space size, and a minimum boxsize
    /// </summary>
    /// <param name="Space"></param>
    /// <param name="minBoxSize"></param>
    public BBoxQuadTree(BBox Space,  double minBoxSize)
    {
        Root = new TreeNode<T>();
        Root.bbox = Space;
        node_count++;
        Precision = minBoxSize;
    }

    /// <summary>
    /// Collection of all items in this quad tree
    /// </summary>
    public IEnumerable<KeyValuePair<T, BBox>> AllBBoxes
    {
        get
        {
            foreach (var v in items)
            {
                yield return new KeyValuePair<T, BBox>(v.Key, v.Value.item2);
            }
        }
    }

    /// <summary>
    /// Ienumerable of all expanded bbox nodes
    /// </summary>
    public IEnumerable<BBox> AllExpandedBBoxNodes
    {
        get
        {
            return RecurseReturn(Root);
        }
    }
    IEnumerable<BBox> RecurseReturn(TreeNode<T> node)
    {
        if (node.bbox == Root.bbox)
            yield return Root.bbox;
        foreach (var v in node.children)
            if (v != null)
            {
                yield return v.bbox;
                foreach (var n in RecurseReturn(v))
                    yield return n;
            }
    }

    //item pools so we aren't constantly creating new objects
    Pool<TreeNode<T>> node_pool = new Pool<TreeNode<T>>();
    Pool<List<TreeNode<T>>> list_pool = new Pool<List<TreeNode<T>>>();
    Pool<HashSet<T>> set_pool = new Pool<HashSet<T>>();

    class TreeNode<K> where K : IEquatable<K>
    {
        public TreeNode<K>[] children = new TreeNode<K>[8];
        public TreeNode<K> parent_node;
        public Dictionary<K, BBox> items;
        public BBox bbox;

        public TreeNode() { }

        /// <summary>
        /// Set position box and parent
        /// </summary>
        /// <param name="position"></param>
        /// <param name="parent"></param>
        public void Set(int position, TreeNode<K> parent)
        {
            Vector3d center = parent.bbox.Center;
            Vector3d size = center - parent.bbox.Min;
            Vector3d pmin = parent.bbox.Min;
            if (position < 0 || position > 7)
                throw new System.Exception("Input error!");
            if (position == 0)
                bbox = new BBox(pmin, size);
            else if (position == 1)
                bbox = new BBox(new Vector3d(pmin.x, pmin.y, pmin.z + size.z), size);
            else if (position == 2)
                bbox = new BBox(new Vector3d(pmin.x, pmin.y + size.y, pmin.z), size);
            else if (position == 3)
                bbox = new BBox(new Vector3d(pmin.x, pmin.y + size.y, pmin.z + size.z), size);
            else if (position == 4)
                bbox = new BBox(new Vector3d(pmin.x + size.x, pmin.y, pmin.z), size);
            else if (position == 5)
                bbox = new BBox(new Vector3d(pmin.x + size.x, pmin.y, pmin.z + size.z), size);
            else if (position == 6)
                bbox = new BBox(new Vector3d(pmin.x + size.x, pmin.y + size.y, pmin.z), size);
            else
                bbox = new BBox(center, size);

            parent_node = parent;
        }

        /// <summary>
        /// Leaf node with no items
        /// </summary>
        public bool IsEmptyLeaf
        {
            get
            {
                return children[0] == null && children[1] == null && children[2] == null && children[3] == null &&
                    children[4] == null && children[5] == null && children[6] == null && children[7] == null &&
                    (items == null || items.Count == 0);
            }
        }
        /// <summary>
        /// leaf node
        /// </summary>
        public bool IsLeaf
        {
            get
            {
                return children[0] == null && children[1] == null && children[2] == null && children[3] == null &&
                    children[4] == null && children[5] == null && children[6] == null && children[7] == null;
            }
        }
        /// <summary>
        /// Has no items contained?
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return items == null || items.Count == 0;
            }
        }
        /// <summary>
        /// Number of items on this node. Does not include children nodes count
        /// </summary>
        public int Count
        {
            get
            {
                return IsEmpty ? 0 : items.Count;
            }
        }

        /// <summary>
        /// Returns true if this tree node is allowed to divide into smaller pieces withoutout making pieces smaller than 'precision'
        /// </summary>
        /// <param name="precision"></param>
        /// <returns></returns>
        public bool CanDivide(double precision)
        {
            return bbox.Size.x / 2 > precision && bbox.Size.y / 2 > precision && bbox.Size.z / 2 > precision;
        }

        //children[0] => 0,0,0 where (x,y,z)
        //[1] => 0,0,1
        //[2] => 0,1,0
        //[3] => 0,1,1
        //[4] => 1,0,0 etc...

        /// <summary>
        /// Returns a structure representing which children fit to the input bbox
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public next_result Next(BBox b)
        {
            Vector3d center = bbox.Center;
            Vector3d size = center - bbox.Min;
            next_result res = default(next_result);

            //if too big. We keep objects in bboxes that are atleast 5x larger (to reduce object fragmentation)
            if (b.Size.x*5 > bbox.Size.x || b.Size.y*5 > bbox.Size.y || b.Size.z*5 > bbox.Size.z)
            {
                res.LargerThanChildrenBBoxes = true;
                return res;
            }

            //if not inside or intersecting
            if (!bbox.Intersects(b))
                return res;

            //compare each different bbox point with center
            if (new BBox(bbox.Min, size).Intersects(b))
                res._0 = true;
            if (new BBox(new Vector3d(bbox.Min.x, bbox.Min.y, bbox.Min.z + size.z), size).Intersects(b))
                res._1 = true;
            if (new BBox(new Vector3d(bbox.Min.x, bbox.Min.y + size.y, bbox.Min.z), size).Intersects(b))
                res._2 = true;
            if (new BBox(new Vector3d(bbox.Min.x, bbox.Min.y + size.y, bbox.Min.z + size.z), size).Intersects(b))
                res._3 = true;
            if (new BBox(new Vector3d(bbox.Min.x + size.x, bbox.Min.y, bbox.Min.z), size).Intersects(b))
                res._4 = true;
            if (new BBox(new Vector3d(bbox.Min.x + size.x, bbox.Min.y, bbox.Min.z + size.z), size).Intersects(b))
                res._5 = true;
            if (new BBox(new Vector3d(bbox.Min.x + size.x, bbox.Min.y + size.y, bbox.Min.z), size).Intersects(b))
                res._6 = true;
            if (new BBox(center, size).Intersects(b))
                res._7 = true;

            return res;
        }
        public struct next_result
        {
            public bool _0;
            public bool _1;
            public bool _2;
            public bool _3;
            public bool _4;
            public bool _5;
            public bool _6;
            public bool _7;
            /// <summary>
            /// Number of children bboxes that the input bbox intersected with
            /// </summary>
            public int count
            {
                get
                {
                    return (_0 ? 1 : 0) + (_1 ? 1 : 0) + (_2 ? 1 : 0) + (_3 ? 1 : 0) +
                           (_4 ? 1 : 0) + (_5 ? 1 : 0) + (_6 ? 1 : 0) + (_7 ? 1 : 0);
                }
            }
            /// <summary>
            /// Did any children bboxes intersect?
            /// </summary>
            public bool HasChild
            {
                get
                {
                    return _0 || _1 || _2 || _3 || _4 || _5 || _6 || _7;
                }
            }
            /// <summary>
            /// Was the input bbox too big?
            /// </summary>
            public bool LargerThanChildrenBBoxes;
        }
        /// <summary>
        /// Add item
        /// </summary>
        /// <param name="item"></param>
        /// <param name="b"></param>
        public void Add(K item, BBox b)
        {
            if (items == null)
                items = new Dictionary<K, BBox>();
            items.Add(item, b);
        }
        /// <summary>
        /// Remove the input item from this node
        /// </summary>
        /// <param name="item"></param>
        public void Remove(K item)
        {
            if (items != null)
            {
                items.Remove(item);
            }
        }
        /// <summary>
        /// Remove this node from its parent. Returns true if success
        /// </summary>
        /// <returns></returns>
        public bool RemoveThisNode()
        {
            TreeNode<K> pnode = parent_node;
            if (pnode != null)
            {
                if (pnode.children[0] == this)
                {
                    pnode.children[0] = null;
                    return true;
                }
                if (pnode.children[1] == this)
                {
                    pnode.children[1] = null;
                    return true;
                }
                if (pnode.children[2] == this)
                {
                    pnode.children[2] = null;
                    return true;
                }
                if (pnode.children[3] == this)
                {
                    pnode.children[3] = null;
                    return true;
                }
                if (pnode.children[4] == this)
                {
                    pnode.children[4] = null;
                    return true;
                }
                if (pnode.children[5] == this)
                {
                    pnode.children[5] = null;
                    return true;
                }
                if (pnode.children[6] == this)
                {
                    pnode.children[6] = null;
                    return true;
                }
                if (pnode.children[7] == this)
                {
                    pnode.children[7] = null;
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Has item?
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool HasItem(K item)
        {
            return items == null ? false : items.ContainsKey(item);
        }
    }

    /// <summary>
    /// Add a new item to this bbox quad tree. Must be a unique item
    /// </summary>
    /// <param name="item"></param>
    /// <param name="b"></param>
    public void Add(T item, BBox b)
    {
        if (items.ContainsKey(item))
            throw new System.Exception("Error, BBoxTree already contains the input item:" + typeof(T).ToString());

        var result = Root.Next(b);

        //if bbox doesn't fit, increase quadtree size until it does
        if (!result.HasChild)
        {
            IncreaseSize(b);
        }

        //find lowest acceptable nodes to add the item at
        RecurssiveAdd(Root, item, b);

        //set pool size
        node_pool.MaxSize = Mathf.Max(node_pool.MaxSize, node_count);
        list_pool.MaxSize = Mathf.Max(node_pool.MaxSize, items.Count);
    }
    /// <summary>
    /// Recurse until a suitable parent bbox is found
    /// </summary>
    /// <param name="node"></param>
    /// <param name="item"></param>
    /// <param name="b"></param>
    void RecurssiveAdd(TreeNode<T> node, T item, BBox b)
    {
        //push down node (if we reach this node, then something will be put on it or as a child, so we must push down)
        if (node.Count == 1 && node.IsLeaf)
        {
            PushDown(node);
        }

        //get next nodes to search
        var next = node.Next(b);

        //if input bbox is bigger than children bboxes, or parent cannot divide any more, or this is an empty leaf node
        if (next.LargerThanChildrenBBoxes || !node.CanDivide(Precision) || node.IsEmptyLeaf)
        {
            //add to parent and done
            node.Add(item, b);
            dictAdd(item, node, b);
        }
        //otherwise it can fit
        else if (next.HasChild)
        {
            //foreach child the bbox intersects with, try to add...
            if (next._0)
            {
                if (node.children[0] == null)
                {
                    node.children[0] = node_pool.Get();
                    node.children[0].Set(0, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[0], item, b);
            }
            if (next._1)
            {
                if (node.children[1] == null)
                {
                    node.children[1] = node_pool.Get();
                    node.children[1].Set(1, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[1], item, b);
            }
            if (next._2)
            {
                if (node.children[2] == null)
                {
                    node.children[2] = node_pool.Get();
                    node.children[2].Set(2, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[2], item, b);
            }
            if (next._3)
            {
                if (node.children[3] == null)
                {
                    node.children[3] = node_pool.Get();
                    node.children[3].Set(3, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[3], item, b);
            }
            if (next._4)
            {
                if (node.children[4] == null)
                {
                    node.children[4] = node_pool.Get();
                    node.children[4].Set(4, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[4], item, b);
            }
            if (next._5)
            {
                if (node.children[5] == null)
                {
                    node.children[5] = node_pool.Get();
                    node.children[5].Set(5, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[5], item, b);
            }
            if (next._6)
            {
                if (node.children[6] == null)
                {
                    node.children[6] = node_pool.Get();
                    node.children[6].Set(6, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[6], item, b);
            }
            if (next._7)
            {
                if (node.children[7] == null)
                {
                    node.children[7] = node_pool.Get();
                    node.children[7].Set(7, node);
                    node_count++;
                }
                RecurssiveAdd(node.children[7], item, b);
            }
        }
    }
    /// <summary>
    /// Add the item and node info to the dictionary
    /// </summary>
    /// <param name="item"></param>
    /// <param name="node"></param>
    /// <param name="b"></param>
    void dictAdd(T item, TreeNode<T> node, BBox b)
    {
        if (!items.ContainsKey(item))
        {
            items[item] = new Pair<List<TreeNode<T>>,BBox>(list_pool.Get(), b);
        }
        items[item].item1.Add(node);
    }
    /// <summary>
    /// Increase the size of root node to fit the input bbox
    /// </summary>
    /// <param name="include"></param>
    void IncreaseSize(BBox include)
    {
        bool forward = true;
        TreeNode<T> old_root = Root;
        while (!Root.bbox.Contains(include))
        {
            BBox b = forward ? new BBox(Root.bbox.Position, Root.bbox.Size * 2) :
                new BBox(Root.bbox.Position - Root.bbox.Size, Root.bbox.Size * 2);
            TreeNode<T> newRoot = node_pool.Get();
            newRoot.bbox = b;
            Root.parent_node = newRoot;
            node_count++;
            int index = forward ? 0 : 7;
            newRoot.children[index] = Root;
            forward = !forward;
            Root = newRoot;
        }
        //incase the old root was empty, try to remove empty nodes
        RecurssiveRemove(old_root);
    }
    /// <summary>
    /// If this node only contains one item and the item can fit into a child node, then push it down one level into  child node.
    /// </summary>
    /// <param name="position"></param>
    void PushDown(TreeNode<T> node)
    {
        if (node.Count == 1 && node.IsLeaf)
        {
            var first = node.items.First();
            var next = node.Next(first.Value);
            //if input bbox is bigger than children bboxes, or parent cannot divide any more
            if (next.LargerThanChildrenBBoxes || !node.CanDivide(Precision))
            {
                //do nothing
            }
            //otherwise it can fit
            else if (next.HasChild)
            {
                //foreach child the bbox intersects with, try to add...
                if (next._0)
                {
                    //if no node.. make a new one
                    if (node.children[0] == null)
                    {
                        node.children[0] = node_pool.Get();
                        node.children[0].Set(0, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[0].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[0], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._1)
                {
                    if (node.children[1] == null)
                    {
                        node.children[1] = node_pool.Get();
                        node.children[1].Set(1, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[1].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[1], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._2)
                {
                    if (node.children[2] == null)
                    {
                        node.children[2] = node_pool.Get();
                        node.children[2].Set(2, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[2].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[2], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._3)
                {
                    if (node.children[3] == null)
                    {
                        node.children[3] = node_pool.Get();
                        node.children[3].Set(3, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[3].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[3], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._4)
                {
                    if (node.children[4] == null)
                    {
                        node.children[4] = node_pool.Get();
                        node.children[4].Set(4, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[4].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[4], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._5)
                {
                    if (node.children[5] == null)
                    {
                        node.children[5] = node_pool.Get();
                        node.children[5].Set(5, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[5].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[5], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._6)
                {
                    if (node.children[6] == null)
                    {
                        node.children[6] = node_pool.Get();
                        node.children[6].Set(6, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[6].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[6], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
                if (next._7)
                {
                    if (node.children[7] == null)
                    {
                        node.children[7] = node_pool.Get();
                        node.children[7].Set(7, node);
                        node_count++;
                    }
                    //add to child node
                    node.children[7].Add(first.Key, first.Value);
                    dictAdd(first.Key, node.children[7], first.Value);
                    //remove from parent
                    node.Remove(first.Key);
                    items[first.Key].item1.Remove(node);
                }
            }
        }
    }
    /// <summary>
    /// Update the bbox for the input item
    /// </summary>
    /// <param name="item"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public void Update(T item, BBox b)
    {
        if (!items.ContainsKey(item))
            throw new System.Exception("Error, BBoxTree does not contain the input item:" + typeof(T).ToString());

        //try simple update
        if (SimpleUpdate(item, b))
            return;

        //otherwise, remove old bboxes and place new ones
        Remove(item);
        Add(item, b);
    }
    /// <summary>
    /// A simple update where no nodes change
    /// </summary>
    /// <param name="item"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    bool SimpleUpdate(T item, BBox b)
    {
        //check to see if change is needed
        var l = items[item].item1;
        //see if all bboxs contain the object, if all bboxes contain the new bbox, then no change is needed
        for (int i = 0; i < l.Count; i++)
            if (!l[i].bbox.StrictContains(b))
                return false;
        //if all of the input bbox is contained, then an add/remove is not needed, so just update current nodes
        for (int i = 0; i < l.Count; i++)
        {
            //if it has the item, then remove and re add to update
            if (l[i].HasItem(item))
            {
                l[i].Remove(item);
                l[i].Add(item, b);
            }
        }
        //update bbox reference
        items[item] = new Pair<List<TreeNode<T>>, BBox>(items[item].item1, b);
        return true;
    }
    /// <summary>
    /// Contains item?
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool Contains(T item)
    {
        return items.ContainsKey(item);
    }
    /// <summary>
    /// Remove the input item
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool Remove(T item)
    {
        if (!items.ContainsKey(item))
            return false;

        //get list of nodes
        List<TreeNode<T>> nodes = items[item].item1;

        //remove all nodes and empty parents from tree
        for (int i = 0; i < nodes.Count; i++)
        {
            nodes[i].Remove(item);
            RecurssiveRemove(nodes[i]);
        }

        //remove from dict
        items.Remove(item);

        //add to pool
        nodes.Clear();
        list_pool.Add(nodes);

        return true;
    }
    /// <summary>
    /// Recurssivly remove empty leaf nodes
    /// </summary>
    /// <param name="node"></param>
    void RecurssiveRemove(TreeNode<T> node)
    {
        if (node.IsEmptyLeaf)
        {
            if (node.RemoveThisNode())
            {
                //recurssivly remove other nodes
                RecurssiveRemove(node.parent_node);
                //release to node pool
                node_pool.Add(node);
                node_count--;
            }
        }
    }
    /// <summary>
    /// Get a list of items that are within 'distance' of 'point'
    /// </summary>
    /// <param name="distance"></param>
    /// <param name="Point"></param>
    /// <returns></returns>
    public HashSet<T> GetNear(double distance, Vector3d Point)
    {
        double dist_sq = distance * distance;
        TreeNode<T> node = Root;
        HashSet<T> closed = set_pool.Get();
        RecurssiveNear(closed, node, Point, dist_sq);
        return closed;
    }
    void RecurssiveNear(HashSet<T> closed, TreeNode<T> node, Vector3d point, double dist_sq)
    {
        //add items on this node
        if (node.items != null)
            foreach (var v in node.items)
            {
                if (!closed.Contains(v.Key) && v.Value.DistanceSq(point) <= dist_sq)
                    closed.Add(v.Key);
            }

        //for each child
        for (int i = 0; i < node.children.Length; i++)
        {
            //if nonnull child and child is within distance
            if (node.children[i] != null && node.children[i].bbox.DistanceSq(point) <= dist_sq)
            {
                RecurssiveNear(closed, node.children[i], point, dist_sq);
            }
        }
    }
    /// <summary>
    /// Returns a list of items that the input bbox intersects with
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public HashSet<T> Intersects(T item)
    {
        BBox b = GetBBox(item);
        return Intersects(b);
    }
    /// <summary>
    /// Returns a list of items that the input bbox intersects with
    /// </summary>
    /// <param name="b"></param>
    /// <returns></returns>
    public HashSet<T> Intersects(BBox b)
    {
        double dist = (b.Center - b.Min).magnitude;
        var near_bboxes = GetNear(dist, b.Center);
        HashSet<T> intersects = set_pool.Get();
        foreach (var box in near_bboxes)
            if (GetBBox(box).Intersects(b))
                intersects.Add(box);
        near_bboxes.Clear();
        set_pool.Add(near_bboxes);
        return intersects;
    }
    /// <summary>
    /// Get the bbox for an input item
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public BBox GetBBox(T item)
    {
        if (!items.ContainsKey(item))
            throw new System.Exception("Error, BBoxTree does not contain the input item:" + typeof(T).ToString());
        return items[item].item2;
    }
    /// <summary>
    /// Return a list of objects of which the input ray intersected its bbox
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="direction"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public HashSet<T> RayCast(Vector3d origin, Vector3d direction, double length)
    {
        return LineCast(origin, origin + length * direction.normalized);
    }
    /// <summary>
    /// Returns a list of objects who's bboxes are intersected by the input line segment. Objects are not sorted.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="stop"></param>
    /// <returns></returns>
    public HashSet<T> LineCast(Vector3d start, Vector3d stop)
    {
        Ray_ex r = new Ray_ex(start, (stop - start).normalized);
        double length = (stop - start).magnitude;
        HashSet<T> results = set_pool.Get();

        if (Geometry.RayBBoxIntersect(Root.bbox, r, length))
            RecurssiveLineCast(r, length, Root, results);
        return results;
    }
    /// <summary>
    /// Recurssivly line cast on child nodes
    /// </summary>
    /// <param name="r"></param>
    /// <param name="length"></param>
    /// <param name="node"></param>
    /// <param name="items"></param>
    void RecurssiveLineCast(Ray_ex r, double length, TreeNode<T> node, HashSet<T> items)
    {
        //add items in this node
        if (!node.IsEmpty)
            foreach (var item in node.items)
            {
                if (Geometry.RayBBoxIntersect(item.Value, r, length))
                    if (!items.Contains(item.Key))
                        items.Add(item.Key);
            }

        //line cast on children if the line intersects them
        for (int i = 0; i < node.children.Length; i++)
        {
            if (node.children[i] != null &&
                Geometry.RayBBoxIntersect(node.children[i].bbox, r, length))
                RecurssiveLineCast(r, length, node.children[i], items);
        }
    }
    /// <summary>
    /// Cast a sphere against the bbox tree, get a list of intersecting objects
    /// </summary>
    /// <param name="s"></param>
    /// <param name="direction"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public HashSet<T> SphereCast(Sphere s, Vector3d direction, double length)
    {
        HashSet<T> results = set_pool.Get();
        Vector3d dir = direction.normalized;
        Capsule c = new Capsule(s.Center, Quaternion.LookRotation(dir.Vec3), length, s.Radius);
        Vector3d start = s.Center + -dir * s.Radius;
        Vector3d stop = s.Center + dir * (length + s.Radius);
        if (Root.bbox.IntersectsCapsule(c))
            RecurssiveSphereCast(c, start, stop, Root, results);
        return results;
    }
    /// <summary>
    /// Recurssivly line cast on child nodes
    /// </summary>
    /// <param name="r"></param>
    /// <param name="length"></param>
    /// <param name="node"></param>
    /// <param name="items"></param>
    void RecurssiveSphereCast(Capsule c,
        Vector3d start, Vector3d stop, TreeNode<T> node, HashSet<T> items)
    {
        //this function first recurses down to the furthest bbox that contains both the start and end point
        //then it does capsule intersection tests to find out which bbox to use

        //add items in this node
        if (!node.IsEmpty)
            foreach (var item in node.items)
            {
                //the casting of a sphere along a line creates a capsule, so we do a bbox capsule intersection test
                if (!items.Contains(item.Key) && item.Value.IntersectsCapsule(c))
                    items.Add(item.Key);
            }

        //check if a capsule intersects the bbox
        for (int i = 0; i < node.children.Length; i++)
        {
            if (node.children[i] != null)
            {
                //if bbox intersects with the capsule made by casting the sphere
                if (node.children[i].bbox.IntersectsCapsule(c))
                {
                    RecurssiveSphereCast(c, start, stop, node.children[i], items);
                }
            }
        }
    }

    /// <summary>
    /// Clear item pools
    /// </summary>
    public void ResetPools()
    {
        list_pool.Clear();
        node_pool.Clear();
        set_pool.Clear();
    }

    /// <summary>
    /// Returning a hashset from the methods that returns Hashsets will improve performance because
    /// this object pools hashsets. SO after you are done using them, it is good to call
    /// BBoxTree.Free(some_hashset_you_got_from_this_object)
    /// </summary>
    /// <param name="set"></param>
    public void Free(HashSet<T> set)
    {
        //we dont want sets getting too large
        if (set.Count > 10)
            set.TrimExcess();
        set.Clear();
        set_pool.Add(set);
    }

    struct Pair<M, M1>
    {
        public M item1;
        public M1 item2;
        public Pair(M i1, M1 i2)
        {
            item1 = i1; item2 = i2;
        }
    }
}
