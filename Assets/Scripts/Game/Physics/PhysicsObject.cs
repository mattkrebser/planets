﻿using UnityEngine;
using System.Collections.Generic;
using System;

/// <summary>
/// Base physics object use for physics computation
/// </summary>
public class PhysicsObject : IEquatable<PhysicsObject>
{
    private static HashSet<int> phys_ids = new HashSet<int>();
    private static System.Random r = new System.Random();
    private int _id;
    /// <summary>
    /// Every physics object has a unique instance ID
    /// </summary>
    public int PHYSICS_ID
    {
        get
        {
            return _id;
        }
    }

    /// <summary>
    /// Enabled Flag, if not enabled then physics processing wont occur for this object
    /// </summary>
    public bool Enabled = true;

    /// <summary>
    /// transform of this object
    /// </summary>
    public Transformd transform
    {
        get
        {
            return t;
        }
    }
    Transformd t;

    /// <summary>
    /// Position, rotation, scale of this object immediatly before it was translated to its current state.
    /// </summary>
    public Transformd previous_transform;

    /// <summary>
    /// Velocity of this physics object
    /// </summary>
    public Vector3d Velocity
    {
        get
        {
            return vel;
        }
        set
        {
            var prev = vel;
            vel = value;
            if (prev != value)
                OnUpdateVelocity(prev);
        }
    }
    Vector3d vel;

    /// <summary>
    /// Get BBox for this physics object
    /// </summary>
    public virtual BBox bbox
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Parent solar system
    /// </summary>
    public virtual SolarSystem ParentSystem
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// PhysicsObject Type
    /// </summary>
    public virtual PhysType type
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Can objects collide into this object? True means yes. Points are typically not collideable
    /// </summary>
    public bool Collideable
    {
        get
        {
            return collideable;
        }
        set
        {
            if (value != collideable)
            {
                if (ParentSystem.Objects == null)
                    ParentSystem.Objects = new BBoxQuadTree<PhysicsObject>(ParentSystem.bbox, VoxelGroup.ChunkSize / 2);
                if (value)
                {
                    ParentSystem.Objects.Add(this, bbox);
                }
                else
                {
                    ParentSystem.Objects.Remove(this);
                }
            }
        }
    }
    bool collideable = false;

    /// <summary>
    /// Set position without checking for collision between start and end points
    /// </summary>
    /// <param name="position"></param>
    public virtual void SetPosition(Vector3d position)
    {
        throw new System.NotImplementedException();
    }

    public PhysicsObject(Transformd local_transform)
    {
        if (local_transform == null)
            throw new System.Exception("Null inputs to constructor!");
        //set transform
        t = local_transform;
        previous_transform = new Transformd();
        previous_transform.Position = t.Position;
        previous_transform.Rotation = t.Rotation;
        previous_transform.Scale = t.Scale;
        //set id
        setnewid();
        //set on modify event function
        t.OnModifyPositionEvent = this.OnObjectPositionModified;
        t.OnModifyRotationEvent = this.OnObjectRotationModified;
        t.OnModifyScaleEvent = this.OnObjectScaleModified;
    }

    /// <summary>
    /// On modified event. Called immediatly after a change is made. The input parameter value is old value
    /// </summary>
    public virtual void OnObjectPositionModified(Vector3d prev_val)
    {
        previous_transform.Position = prev_val;
    }

    /// <summary>
    /// On modified event. Called immediatly after a change is made. The input parameter value is old value
    /// </summary>
    public virtual void OnObjectRotationModified(Quaterniond prev_val)
    {
        previous_transform.Rotation = prev_val;
    }

    /// <summary>
    /// On modified event. Called immediatly after a change is made. The input parameter value is old value
    /// </summary>
    public virtual void OnObjectScaleModified(Vector3d prev_val)
    {
        previous_transform.Scale = prev_val;
    }

    /// <summary>
    /// OnUpdate Velocity function. Called whenever this.Velocity is modified to a different value.
    /// </summary>
    /// <param name="prev_val">previous velocity value.</param>
    public virtual void OnUpdateVelocity(Vector3d prev_val)
    {

    }

    /// <summary>
    /// Called this first frame this object is stuck
    /// </summary>
    public virtual void OnStuck() { }
    /// <summary>
    /// called if this object becomes un-stuck
    /// </summary>
    public virtual void OnUnStuck() { }

    /// <summary>
    /// Set the id of this object to a new unique ID
    /// </summary>
    private void setnewid()
    {
        _id = r.Next(int.MinValue, int.MaxValue);
        while (phys_ids.Contains(_id))
            _id = r.Next(int.MinValue, int.MaxValue);
        phys_ids.Add(_id);
    }

    ~PhysicsObject()
    {
        //free id
        phys_ids.Remove(_id);
    }

    public override bool Equals(object obj)
    {
        var obj_cast = obj as PhysicsObject;
        return obj_cast != null && obj_cast._id == _id;
    }
    public override int GetHashCode()
    {
        return _id.GetHashCode();
    }
    public bool Equals(PhysicsObject obj)
    {
        return obj != null && obj._id == _id;
    }

    /// <summary>
    /// Is physics object colliding with anything?
    /// </summary>
    /// <returns></returns>
    public virtual bool IsColliding() { throw new System.NotImplementedException(); }
    /// <summary>
    /// Returns true if is or isnt colliding. If true, the function outputs a resolution.
    /// The resolution will resolve along the translation of this physics object
    /// from 'previous_transform' to 'transform'
    /// </summary>
    /// <returns></returns>
    public virtual CollisionResult TryResolveIsColliding(out CollisionResolution resolution)
    { throw new System.NotImplementedException(); }
    /// <summary>
    /// Determine if (and resolve) collisions with other objects. Collisions take into account how this object
    /// moves through space (using velocity).
    /// </summary>
    /// <param name="resolution"></param>
    /// <returns></returns>
    public virtual CollisionResult TryResolveIsCollidingContinuous(out CollisionResolution resolution)
    {
        throw new NotImplementedException();
    }
}

/// <summary>
/// Types of physics objects. Each physics object class type should have its own enum type
/// </summary>
public enum PhysType
{
    physpoint,
    physvoxel
}