﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Class for polling collisions between various physicsobjects
/// </summary>
public static class collisions
{
    const double e = 0.0000000001;

    /// <summary>
    /// Resolve collision between physpoint and voxel group
    /// </summary>
    /// <param name="p"></param>
    /// <param name="v"></param>
    /// <returns></returns>
	public static CollisionResult TryResolveIsColliding(PhysPoint p, VoxelGroup v, 
        out CollisionResolution resolution)
    {
        resolution = default(CollisionResolution);
        var pos = p.transform.Position;
        if (v.bbox.Contains(pos))
        {
            //get voxel group voxel position
            var ipos = locald2int(pos, v);
            var lpos = locald2d(pos, v);
            //get chunk at this position
            var chunk = v.VoxelData.GetHighestResValue(ipos, 0);
            //get block from chunk
            var block = chunk[ipos.x, ipos.y, ipos.z];
            //see if colliding
            var collision = BlockLibrary.Blocks[block].Collide;

            if (collision)
            {
                //curent block bbox
                var vlen = (double)chunk.VoxelLength;
                BBox bbox = new BBox(ipos, new Vector3d(vlen, vlen, vlen));
                Vector3d min = bbox.Min, max = bbox.Max;

                var closest_dist_sq = double.MaxValue;
                var closest_pos = Vector3Int.zero;
                //check 26 closest blocks, get the closest
                for (int x = -1; x < 2; x++)
                {
                    for (int y = -1; y < 2; y++)
                    {
                        for (int z = -1; z < 2; z++)
                        {
                            int lx = ipos.x + x, ly = ipos.y + y, lz = ipos.z + z;
                            var vblock = BlockLibrary.Blocks[chunk[lx, ly, lz]];
                            if (!vblock.Collide)
                            {
                                if (x == 0 && y == 0 && z == 0) continue;
                                var block_bbox = new BBox(new Vector3d(lx, ly, lz), new Vector3d(vlen, vlen, vlen));
                                var dist_sq = block_bbox.DistanceSq_OUTSIDE(lpos);
                                if (dist_sq < closest_dist_sq)
                                {
                                    closest_dist_sq = dist_sq;
                                    closest_pos = new Vector3Int(x, y, z);
                                }
                            }
                        }
                    }
                }

                //TODO ? make chunk access that returns voxel length, otherwise this could error on multi resolution section

                //if none found, then collision is unresolved
                if (closest_pos == Vector3Int.zero)
                    return CollisionResult.CollidingUnresolved;

                var new_bbox = new BBox(closest_pos + ipos, bbox.Size);
                var res = new_bbox.StrictFitPoint(lpos);

                resolution.FaceNormal = new_bbox.ClosestNormalToPoint(res);
                resolution.Position = globald2d(res, v);
                resolution.Rotation = p.transform.Rotation;
                resolution.Scale = p.transform.Scale;
                resolution.VelocityTangent = Vector3d.zero;

                return CollisionResult.CollidingResolved;
            }
        }
        return CollisionResult.NotColliding;
    }
    /// <summary>
    /// Resolve collision between physPoint and Voxelgroup in a continuous maner (uses velocity)
    /// </summary>
    /// <param name="p"></param>
    /// <param name="v"></param>
    /// <param name="resolution"></param>
    /// <returns></returns>
    public static CollisionResult TryResolveIsCollidingContinuous(PhysPoint p, VoxelGroup v,
        out CollisionResolution resolution)
    {
        resolution = default(CollisionResolution);
        return CollisionResult.CollidingUnresolved;
    }

    /// <summary>
    /// Is point colliding with voxel bbox?
    /// </summary>
    /// <param name="p"></param>
    /// <param name="v"></param>
    /// <returns></returns>
    public static bool IsColliding(PhysPoint p, VoxelGroup v)
    {
        var pos = p.transform.Position;
        if (v.bbox.Contains(pos))
        {
            //get voxel group voxel position
            var ipos = locald2int(pos, v);
            //get chunk at this position
            var chunk = v.VoxelData.GetHighestResValue(ipos, 0);
            //get block from chunk
            var block = chunk[ipos.x, ipos.y, ipos.z];
            //see if colliding
            var collision = BlockLibrary.Blocks[block].Collide;
            return collision;
        }
        return false;
    }

    //helper methods
    /// <summary>
    /// converts a solar system relative point to a voxel group relative point. The input vector is assumed
    /// to be inside the voxle group bbox
    /// </summary>
    /// <param name="v"></param>
    /// <param name="vox"></param>
    /// <returns></returns>
    static Vector3Int locald2int(Vector3d v, VoxelGroup vox)
    {
        //TODO: transform rotation as well :(
        var n = v - vox.bbox.Position;
        return new Vector3Int((int)n.x, (int)n.y, (int)n.z);
    }
    /// <summary>
    /// COnvert input position to be local to voxel group
    /// </summary>
    /// <param name="v"></param>
    /// <param name="vox"></param>
    /// <returns></returns>
    static Vector3d locald2d(Vector3d v, VoxelGroup vox)
    {
        return  v - vox.bbox.Position;
    }
    /// <summary>
    /// COnvert input local position to solar system position
    /// </summary>
    /// <param name="v"></param>
    /// <param name="vox"></param>
    /// <returns></returns>
    static Vector3d globald2d(Vector3d v, VoxelGroup vox)
    {
        return vox.bbox.Position + v;
    }
}


public struct CollisionResolution
{
    /// <summary>
    /// New position
    /// </summary>
    public Vector3d Position;
    /// <summary>
    /// new rotation
    /// </summary>
    public Quaterniond Rotation;
    /// <summary>
    /// new scale
    /// </summary>
    public Vector3d Scale;

    //TODO? Newtonian physics vector (like a reflected direction vector)

    /// <summary>
    /// Velocity along the face tangent. (not normalized)
    /// </summary>
    public Vector3d VelocityTangent;
    /// <summary>
    /// normal of the collided face. (not normalized)
    /// </summary>
    public Vector3d FaceNormal;

    public CollisionResolution(Vector3d pos, Quaterniond rot, Vector3d scale)
    {
        Position = pos; Rotation = rot; Scale = scale;
        VelocityTangent = Vector3d.zero;
        FaceNormal = Vector3d.zero;
    }
}

public enum CollisionResult
{
    CollidingResolved = 1,
    NotColliding = 0,
    CollidingUnresolved = 2
}