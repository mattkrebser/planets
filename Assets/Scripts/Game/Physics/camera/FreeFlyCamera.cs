﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Free flying camera
/// </summary>
public class FreeFlyCamera : MonoBehaviour
{
    /// <summary>
    /// camera that will be moved by this object
    /// </summary>
    public Camera MyCamera;

    /// <summary>
    /// Keys to move forward
    /// </summary>
    public List<KeyCode> Forward;
    /// <summary>
    /// keys to move back
    /// </summary>
    public List<KeyCode> Back;
    /// <summary>
    /// keys to move left
    /// </summary>
    public List<KeyCode> Left;
    /// <summary>
    /// keys to move right
    /// </summary>
    public List<KeyCode> Right;
    /// <summary>
    /// keys to move up
    /// </summary>
    public List<KeyCode> up;
    /// <summary>
    /// keys to move down
    /// </summary>
    public List<KeyCode> down;
    /// <summary>
    /// move faster key
    /// </summary>
    public List<KeyCode> sprint;

    /// <summary>
    /// MoveSpeed of the camera
    /// </summary>
    public float MoveSpeed = 1.0f;
    /// <summary>
    /// Trun sensitivity of the camera
    /// </summary>
    public float Sensitivity = 1.0f;
    /// <summary>
    /// Hide player mouse?
    /// </summary>
    public bool HideMouse
    {
        get
        {
            return mousehide;
        }
        set
        {
            mousehide = value;

            if (mousehide)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
        }
    }
    bool mousehide = true;

    /// <summary>
    /// Sprint speed multiplier
    /// </summary>
    public float SprintMultiplier = 1.4f;

    /// <summary>
    /// point collider for this free fly camera
    /// </summary>
    private PhysPoint point_collider;

    void Start()
    {
        HideMouse = true;
        point_collider = new PhysPoint(new Transformd(transform), GetComponent<LocalOrigin>().LocalSystem);
        Phys.SynchTransform(transform, point_collider);
    }

    float dx, dy;

	void LateUpdate ()
    {
        //rotate camera based on mouse input
        dx += Sensitivity * Input.GetAxis("Mouse X");
        dy -= Sensitivity * Input.GetAxis("Mouse Y");
        point_collider.transform.Rotation = Quaternion.Euler(new Vector3(dy, dx, 0));
    }

    public void FixedUpdate()
    {
        if (MyCamera == null)
            throw new System.Exception("Error, FreeFlyCamera has a null attatched camera!");

        //get position of camera
        Vector3 delta = Vector3.zero;
        var sprintm = 1.0f;

        //if ay keys in the forward list being pressed
        if (Forward != null && Forward.Any(x => Input.GetKey(x)))
        {
            delta += MyCamera.transform.forward;
        }
        if (Back != null && Back.Any(x => Input.GetKey(x)))
        {
            delta += -MyCamera.transform.forward;
        }
        if (Left != null && Left.Any(x => Input.GetKey(x)))
        {
            delta += -MyCamera.transform.right;
        }
        if (Right != null && Right.Any(x => Input.GetKey(x)))
        {
            delta += MyCamera.transform.right;
        }
        if (up != null && up.Any(x => Input.GetKey(x)))
        {
            delta += MyCamera.transform.up;
        }
        if (down != null && down.Any(x => Input.GetKey(x)))
        {
            delta += -MyCamera.transform.up;
        }
        if (sprint != null && sprint.Any(x => Input.GetKey(x)))
        {
            sprintm = SprintMultiplier;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Phys.RayCast.cast(point_collider.transform.Position, transform.forward, 10000, point_collider);
        }

        delta *= MoveSpeed * sprintm;
        point_collider.transform.Position += delta;
    }
}
