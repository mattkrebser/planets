﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// General purpose physics class for handling physics updates
/// </summary>
public static partial class Phys
{
    //using hashsets just because it is easy, TODO? optimize in future for fewer collections

    private static Dictionary<PhysicsObject, int> update_list = 
        new Dictionary<PhysicsObject, int>();
    private static HashSet<PhysicsObject> position_updated = new HashSet<PhysicsObject>();
    private static HashSet<PhysicsObject> non_position_updated = new HashSet<PhysicsObject>();

    private static HashSet<PhysicsObject> vel_update_list =
        new HashSet<PhysicsObject>();

    private static Dictionary<PhysicsObject, int> resynch_list =
        new Dictionary<PhysicsObject, int>();

    private static Dictionary<PhysicsObject, Transform> synched_list =
        new Dictionary<PhysicsObject, Transform>();

    private static HashSet<PhysicsObject> stuck_list = new HashSet<PhysicsObject>();

    private static Queue<PhysicsObject> unstuck_event_list = new Queue<PhysicsObject>();
    private static Queue<PhysicsObject> stuck_event_list = new Queue<PhysicsObject>();
    private static Queue<PhysicsObject> blocked_velocity_update = new Queue<PhysicsObject>();

    /// <summary>
    /// Enque an object to be processed by the physics loop (transform change)
    /// </summary>
    /// <param name="obj"></param>
    public static void EnqueTransformUpdate(PhysicsObject obj, UpdateType type)
    {
        int val;
        if (update_list.TryGetValue(obj, out val))
        {
            update_list[obj] = set(val, type, true);
        }
        else
        {
            update_list.Add(obj, set(0, type, true));
        }
    }

    /// <summary>
    /// Enque an object to be processed by the physics loops (velocity cange)
    /// </summary>
    /// <param name="obj"></param>
    public static void EnqueVelocityUpdate(PhysicsObject obj)
    {
        if (!vel_update_list.Contains(obj))
            vel_update_list.Add(obj);
    }

    /// <summary>
    /// Make the physics thread update a synched transform. No collision or physics processing will occur
    /// </summary>
    /// <param name="obj"></param>
    public static void EnqueResynch(PhysicsObject obj, UpdateType type)
    {
        int val;
        if (resynch_list.TryGetValue(obj, out val))
        {
            resynch_list[obj] = set(val, type, true);
        }
        else
        {
            resynch_list.Add(obj, set(0, type, true));
        }
    }

    /// <summary>
    /// Process all objects on the update queue (updates movements, collisions, all physics related stuff)
    /// </summary>
    public static void ProcessUpdateQueue()
    {
        //determine which objects update their position
        foreach (var obj in update_list)
        {
            if (type_enabled(obj.Value, UpdateType.Position))
                position_updated.Add(obj.Key);
            else
                non_position_updated.Add(obj.Key);
        }

        //do position transform physics operations
        handle_position_transforms();

        //velocity + forces pass
        physics_pass();

        //non positinal transforms (rotation and scale)
        handle_non_position_transforms();

        //synchronize unity transforms
        sync_pass();

        //stuck and un stuck events
        foreach (var obj in unstuck_event_list)
            obj.OnUnStuck();
        foreach (var obj in stuck_event_list)
            obj.OnStuck();

        //clear update list
        update_list.Clear();
        resynch_list.Clear();
        stuck_event_list.Clear();
        unstuck_event_list.Clear();
        vel_update_list.Clear();
        position_updated.Clear();
        non_position_updated.Clear();

        //re-queue blocked events
        foreach (var obj in blocked_velocity_update)
            vel_update_list.Add(obj);
        blocked_velocity_update.Clear();
    }

    static void handle_position_transforms()
    {
        //do transform update physics pass
        foreach (var obj in position_updated)
        {
            //check if colliding and resolve
            CollisionResolution resolution;
            var result = obj.TryResolveIsColliding(out resolution);
            if (result == CollisionResult.CollidingResolved)
            {
                obj.transform.SilentUpdatePosition(resolution.Position);
                obj.transform.SilentUpdateRotation(resolution.Rotation);
                obj.transform.SilentUpdateScale(resolution.Scale);
                //if unstuck event
                if (stuck_list.Remove(obj)) unstuck_event_list.Enqueue(obj);
            }
            else if (result == CollisionResult.CollidingUnresolved)
            {
                if (stuck_list.Contains(obj))
                {
                    obj.transform.SilentUpdatePosition(obj.previous_transform.Position);
                    obj.transform.SilentUpdateRotation(obj.previous_transform.Rotation);
                    obj.transform.SilentUpdateScale(obj.previous_transform.Scale);
                }
                else
                {
                    //stuck event
                    stuck_list.Add(obj);
                    stuck_event_list.Enqueue(obj);
                }
            }
            else
            {
                //unstuck event
                if (stuck_list.Remove(obj))
                    unstuck_event_list.Enqueue(obj);
            }
        }
    }

    static void handle_non_position_transforms()
    {
        //non position transforms behaves differently then positional transforms updates
        //because it doesn't use a 'stuck' queue, instead it just doesn't allow objects to be stuck
        //if they are only transformed via scale or rotation 
        //
        //-(if you scale or rotate an objects so that it
        //  is stuck, the scaling/rotating is undone)
        //-objects can only be 'stuck' through position updates

        foreach (var obj in non_position_updated)
        {
            //otherwise
            CollisionResolution resolution;
            var result = obj.TryResolveIsColliding(out resolution);
            if (result == CollisionResult.CollidingResolved)
            {
                obj.transform.SilentUpdatePosition(resolution.Position);
                obj.transform.SilentUpdateRotation(resolution.Rotation);
                obj.transform.SilentUpdateScale(resolution.Scale);
                //if unstuck event
                if (stuck_list.Remove(obj)) unstuck_event_list.Enqueue(obj);
            }
            else if (result == CollisionResult.CollidingUnresolved)
            {
                //if unresolved collision, then go to previous
                obj.transform.SilentUpdatePosition(obj.previous_transform.Position);
                obj.transform.SilentUpdateRotation(obj.previous_transform.Rotation);
                obj.transform.SilentUpdateScale(obj.previous_transform.Scale);
            }
        }
    }

    static void sync_pass()
    {
        //update unity transforms pass
        foreach (var obj in position_updated)
        {
            Transform synched_transform;
            if (synched_list.TryGetValue(obj, out synched_transform))
            {
                synched_transform.position = (Vector3)obj.transform.Position;
                synched_transform.rotation = (Quaternion)obj.transform.Rotation;
                synched_transform.localScale = (Vector3)obj.transform.Scale;
            }
        }

        //resynch list
        foreach (var obj in resynch_list)
        {
            //if updated then just continue
            if (update_list.ContainsKey(obj.Key) || vel_update_list.Contains(obj.Key)) continue;

            Transform synched_transform;
            if (synched_list.TryGetValue(obj.Key, out synched_transform))
            {
                var value = obj.Value;
                if (type_enabled(value, UpdateType.Position))
                    synched_transform.position = (Vector3)obj.Key.transform.Position;
                if (type_enabled(value, UpdateType.Rotation))
                    synched_transform.rotation = (Quaternion)obj.Key.transform.Rotation;
                if (type_enabled(value, UpdateType.Scale))
                    synched_transform.localScale = (Vector3)obj.Key.transform.Scale;
            }
        }
    }

    static void physics_pass()
    {
        foreach (var obj in vel_update_list)
        {
            //transform updates block velocity updates for 1 frame
            if (update_list.ContainsKey(obj))
                blocked_velocity_update.Enqueue(obj);
            else
            {
                CollisionResolution resolution;
                var result = obj.TryResolveIsCollidingContinuous(out resolution);
            }
        }
    }

    /// <summary>
    /// Synch a unity transform with a physics object. May only synch one transform per physics object.
    /// This function overwrites any previously synched value.
    /// </summary>
    /// <param name="t"></param>
    /// <param name="p"></param>
    public static void SynchTransform(Transform t, PhysicsObject p)
    {
        if (synched_list.ContainsKey(p))
            synched_list[p] = t;
        else
            synched_list.Add(p, t);
    }

    public enum UpdateType
    {
        Position = 0,
        Rotation = 1,
        Scale = 2
    }

    static int set(int current_value, UpdateType add_this, bool value)
    {
        int index = (int)add_this;
        if (value)
            current_value = current_value | (1 << index);
        else
            current_value = current_value & ~(1 << index);
        return current_value;
    }
    static bool type_enabled(int current_value, UpdateType type)
    {
        var val = (current_value & (1 << (int)type)) != 0 ? 1 : 0;
        if (val == 0)
            return false;
        return true;
    }

    public static partial class RayCast
    {

    }
}
