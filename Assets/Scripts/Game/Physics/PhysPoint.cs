﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A point collider for physics computation.
/// </summary>
public class PhysPoint : PhysicsObject
{
    public SolarSystem local_system;

	public PhysPoint(Transformd transform, SolarSystem system) : base(transform)
    {
        local_system= system;
    }

    public override BBox bbox
    {
        get
        {
            return new BBox(transform.Position, Vector3d.zero);
        }
    }

    public override SolarSystem ParentSystem
    {
        get
        {
            return local_system;
        }
    }

    public override PhysType type
    {
        get
        {
            return PhysType.physpoint;
        }
    }

    public override void OnObjectPositionModified(Vector3d new_value)
    {
        //call base function
        base.OnObjectPositionModified(new_value);

        if (Enabled)
            Phys.EnqueTransformUpdate(this, Phys.UpdateType.Position);
    }

    public override void OnObjectRotationModified(Quaterniond new_val)
    {
        base.OnObjectRotationModified(new_val);

        //rotation doesn't really effect a point, but if a player is using it as a collider
        //for a camera or player, then it will need to rotate
        if (Enabled)
            Phys.EnqueResynch(this, Phys.UpdateType.Rotation);
    }

    public override void OnUpdateVelocity(Vector3d prev_val)
    {
        Phys.EnqueVelocityUpdate(this);
    }

    public override bool IsColliding()
    {
        if (local_system == null || local_system.Objects == null) return false;
        var near = local_system.Objects.GetNear(0, transform.Position);

        foreach (var n in near)
        {
            switch (n.type)
            {
                case PhysType.physvoxel:
                    {
                        if (collisions.IsColliding(this, (VoxelGroup)n)) return true;
                        break;
                    }
                default:
                    throw new System.NotImplementedException("Unknown type " + n.type.ToString());
            }
        }

        //free set back to pool
        local_system.Objects.Free(near);

        return false;
    }

    public override CollisionResult TryResolveIsColliding(out CollisionResolution resolution)
    {
        if (local_system == null || local_system.Objects == null)
        { resolution = default(CollisionResolution); return CollisionResult.NotColliding; }

        var near = local_system.Objects.GetNear(0, transform.Position);
        resolution = default(CollisionResolution);
        CollisionResult result = CollisionResult.NotColliding;

        foreach (var n in near)
        {
            switch (n.type)
            {
                case PhysType.physvoxel:
                    {
                        var temp_result = collisions.TryResolveIsColliding(this, (VoxelGroup)n, out resolution);

                        if (temp_result == CollisionResult.CollidingUnresolved)
                            return CollisionResult.CollidingUnresolved;

                        if (temp_result == CollisionResult.CollidingResolved)
                            result = CollisionResult.CollidingResolved;
                        
                        break;
                    }
                default:
                    throw new System.NotImplementedException("Unknown type " + n.type.ToString());
            }
        }

        //free set back to pool
        local_system.Objects.Free(near);
        return result;
    }

    public override CollisionResult TryResolveIsCollidingContinuous(out CollisionResolution resolution)
    {
        if (local_system == null || local_system.Objects == null)
        { resolution = default(CollisionResolution); return CollisionResult.NotColliding; }

        var near = local_system.Objects.GetNear(0, transform.Position);
        resolution = default(CollisionResolution);
        CollisionResult result = CollisionResult.NotColliding;

        foreach (var n in near)
        {
            switch (n.type)
            {
                case PhysType.physvoxel:
                    {
                        var temp_result = collisions.TryResolveIsColliding(this, (VoxelGroup)n, out resolution);

                        if (temp_result == CollisionResult.CollidingUnresolved)
                            return CollisionResult.CollidingUnresolved;

                        if (temp_result == CollisionResult.CollidingResolved)
                            result = CollisionResult.CollidingResolved;

                        break;
                    }
                case PhysType.physpoint:
                    {
                        break;
                    }
                default:
                    throw new System.NotImplementedException("Unknown type " + n.type.ToString());
            }
        }

        //free set back to pool
        local_system.Objects.Free(near);
        return result;
    }
}
