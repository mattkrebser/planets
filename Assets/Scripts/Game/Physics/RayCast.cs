﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static partial class Phys
{
    public static partial class RayCast
    {
        public static bool cast(Vector3d origin, Vector3d direction, double dist, PhysicsObject p)
        {
            if (p == null || p.ParentSystem == null || p.ParentSystem.Objects == null)
                return false;
            var near = p.ParentSystem.Objects.RayCast(origin, direction, dist);

            foreach (var obj in near)
            {
                switch (obj.type)
                {
                    case PhysType.physvoxel:
                        {
                            Vector3d intersection;
                            bool result = voxel_cast(origin, direction,
                                dist * dist, (VoxelGroup)obj, out intersection);
                            break;
                        }
                    case PhysType.physpoint:
                        {
                            break;
                        }
                    default:
                        throw new System.NotImplementedException("Raycast(Physpoint): Unknown type " + obj.type.ToString());
                }
            }

            p.ParentSystem.Objects.Free(near);
            return false;
        }

        public static bool cast(Vector3d origin, Vector3d direction, double dist, SolarSystem s)
        {
            return false;
        }

        public static bool voxel_cast(Vector3d origin, Vector3d direction,
            double dist_sq, VoxelGroup v, out Vector3d intersection)
        {
            intersection = Vector3d.zero;
            //translated source
            //http://gamedev.stackexchange.com/questions/47362/cast-ray-to-select-block-in-voxel-game

            // From "A Fast Voxel Traversal Algorithm for Ray Tracing"
            // by John Amanatides and Andrew Woo, 1987
            // <http://www.cse.yorku.ca/~amana/research/grid.pdf>
            // <http://citeseer.ist.psu.edu/viewdoc/summary?doi=10.1.1.42.3443>
            // Extensions to the described algorithm:
            //   • Imposed a distance limit.
            //   • The face passed through to reach the current cube is provided to
            //     the callback.

            // The foundation of this algorithm is a parameterized representation of
            // the provided ray,
            //                    origin + t * direction,
            // except that t is not actually stored; rather, at any given point in the
            // traversal, we keep track of the *greater* t values which we would have
            // if we took a step sufficient to cross a cube boundary along that axis
            // (i.e. change the integer part of the coordinate) in the variables
            // tMaxX, tMaxY, and tMaxZ.

            //fit starting point to be inside voxel bbox
            var bbox = v.bbox;
            if (!bbox.StrictContains(origin))
            {
                Vector3d n;
                var result = Geometry.LineBBoxIntersect(bbox, origin, direction, out origin, out n);
                origin = bbox.StrictFitPoint(origin);
                //if the line doesnt intersect the bbox, then just return
                if (!result)
                    return false;
            }
            var l_bbox = v.VoxelGroupLocalBBox;
            //normalize direction
            direction = direction.normalized;

            //compute local origin
            var l_origin = locald2d(origin, v);

            // Cube containing origin point.
            var x = (int)l_origin.x;
            var y = (int)l_origin.y;
            var z = (int)l_origin.z;
            // Break out direction vector.
            var dx = direction.x;
            var dy = direction.y;
            var dz = direction.z;
            // Direction to increment x,y,z when stepping.
            var stepX = signum(dx);
            var stepY = signum(dy);
            var stepZ = signum(dz);
            // See description above. The initial values depend on the fractional
            // part of the origin.
            var tMaxX = intbound(l_origin.x, dx);
            var tMaxY = intbound(l_origin.y, dy);
            var tMaxZ = intbound(l_origin.z, dz);
            // The change in t when taking a step (always positive).
            var tDeltaX = stepX / dx;
            var tDeltaY = stepY / dy;
            var tDeltaZ = stepZ / dz;

            // Buffer for collecting face info
            var face = Vector3Int.zero;

            // Avoids an infinite loop.
            if (dx == 0 && dy == 0 && dz == 0)
                throw new System.Exception("Raycast in zero direction!");

            VoxelGroup.Chunk chunk = v.VoxelData.GetHighestResValue(new Vector3Int(x, y, z), 0);

            while ((new Vector3d(x, y, z) - l_origin).sqrMagnitude < dist_sq &&
                l_bbox.Contains(new Vector3d(x, y, z)))
            {
                //get current block position
                var cpos = new Vector3Int(x, y, z);

                //if current position isn't inside this chunk, then goto the next one
                if (!chunk.InBounds(x, y, z))
                    chunk = v.VoxelData.GetHighestResValue(cpos, 0);

                //get block
                var block = chunk.IsSingleBlockChunk ? BlockLibrary.Blocks[chunk.SingleBlockID] :
                    BlockLibrary.Blocks[chunk.UnboundedGetVoxel(x, y, z)];

                //if block is collideable
                if (block.Collide)
                {
                    intersection = exact_pos(face, cpos, chunk.VoxelLength, l_origin, direction);
                    intersection = globald2d(intersection, v);
                    return true;
                }

                // tMaxX stores the t-value at which we cross a cube boundary along the
                // X axis, and similarly for Y and Z. Therefore, choosing the least tMax
                // chooses the closest cube boundary. Only the first case of the four
                // has been commented in detail.
                if (tMaxX < tMaxY)
                {
                    if (tMaxX < tMaxZ)
                    {
                        // Update which cube we are now in.
                        x += stepX;
                        // Adjust tMaxX to the next X-oriented boundary crossing.
                        tMaxX += tDeltaX;
                        // Record the normal vector of the cube face we entered.
                        face.x = -stepX;
                        face.y = 0;
                        face.z = 0;
                    }
                    else
                    {
                        z += stepZ;
                        tMaxZ += tDeltaZ;
                        face.x = 0;
                        face.y = 0;
                        face.z = -stepZ;
                    }
                }
                else
                {
                    if (tMaxY < tMaxZ)
                    {
                        y += stepY;
                        tMaxY += tDeltaY;
                        face.x = 0;
                        face.y = -stepY;
                        face.z = 0;
                    }
                    else
                    {
                        // Identical to the second case, repeated for simplicity in
                        // the conditionals.
                        z += stepZ;
                        tMaxZ += tDeltaZ;
                        face.x = 0;
                        face.y = 0;
                        face.z = -stepZ;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Return exact position of line cube face intersection
        /// </summary>
        /// <param name="face"></param>
        /// <param name="cube_pos"></param>
        /// <param name="voxel_len"></param>
        /// <param name="start"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        static Vector3d exact_pos(Vector3Int face, Vector3Int cube_pos, 
            int voxel_len, Vector3d start, Vector3d dir)
        {
            Vector3d r;
            var min = cube_pos;
            var max = min + new Vector3Int(voxel_len, voxel_len, voxel_len);
            if (face.x != 0)
            {
                if (face.x > 0)
                    Geometry.LinePlaneIntersection(out r, start, dir, Vector3d.right, max);
                else
                    Geometry.LinePlaneIntersection(out r, start, dir, Vector3d.left, min);
            }
            else if (face.y != 0)
            {
                if (face.y > 0)
                    Geometry.LinePlaneIntersection(out r, start, dir, Vector3d.up, max);
                else
                    Geometry.LinePlaneIntersection(out r, start, dir, Vector3d.down, min);
            }
            else if (face.z != 0)
            {
                if (face.z > 0)
                    Geometry.LinePlaneIntersection(out r, start, dir, Vector3d.forward, max);
                else
                    Geometry.LinePlaneIntersection(out r, start, dir, Vector3d.back, min);
            }
            else
                throw new System.Exception("Error, no input face.");
            return r;
        }

        //helper methods
        static double  intbound(double s, double ds)
        {
            // Find the smallest positive t such that s+t*ds is an integer.
            if (ds < 0)
            {
                return intbound(-s, -ds);
            }
            else
            {
                s = mod(s, 1);
                // problem is now s+t*ds = 1
                return (1 - s) / ds;
            }
        }
        static int signum(double x)
        {
            return x > 0 ? 1 : x < 0 ? -1 : 0;
        }
        static double mod(double value, int modulus)
        {
            return (value % modulus + modulus) % modulus;
        }
        /// <summary>
        /// converts a solar system relative point to a voxel group relative point. The input vector is assumed
        /// to be inside the voxle group bbox
        /// </summary>
        /// <param name="v"></param>
        /// <param name="vox"></param>
        /// <returns></returns>
        static Vector3Int locald2int(Vector3d v, VoxelGroup vox)
        {
            //TODO: transform rotation as well :(
            var n = v - vox.bbox.Position;
            return new Vector3Int((int)n.x, (int)n.y, (int)n.z);
        }
        /// <summary>
        /// COnvert input position to be local to voxel group
        /// </summary>
        /// <param name="v"></param>
        /// <param name="vox"></param>
        /// <returns></returns>
        static Vector3d locald2d(Vector3d v, VoxelGroup vox)
        {
            return v - vox.bbox.Position;
        }
        /// <summary>
        /// COnvert input local position to solar system position
        /// </summary>
        /// <param name="v"></param>
        /// <param name="vox"></param>
        /// <returns></returns>
        static Vector3d globald2d(Vector3d v, VoxelGroup vox)
        {
            return vox.bbox.Position + v;
        }
    }
}
