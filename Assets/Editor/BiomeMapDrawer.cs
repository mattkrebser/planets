﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(BiomeMap))]
public class BiomeMapDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        BiomeMap map = serializedObject.targetObject as BiomeMap;
        if (map != null && map.Map != null)
        {
            //find each color
            Dictionary<Color32, int> colors = new Dictionary<Color32, int>();
            for (int i = 0; i < map.Map.width; i++)
                for (int j = 0; j < map.Map.height; j++)
                    if (!colors.ContainsKey(map.Map.GetPixel(i, j)))
                        colors.Add(map.Map.GetPixel(i, j),0);
            //add an entry for each color
            foreach (var c in colors.Keys)
                if (!Contains(map, c))
                    map.Biomes.Add(new BiomeType(c, null));
            //remove non existant entries
            for (int i = map.Biomes.Count - 1; i >= 0; i--)
                if (!colors.ContainsKey(map.Biomes[i].color))
                    map.Biomes.RemoveAt(i);
            //remove duplicates
            for (int i = map.Biomes.Count - 1; i >= 0; i--)
            {
                if (colors[map.Biomes[i].color] > 0)
                    map.Biomes.RemoveAt(i);
                colors[map.Biomes[i].color]++;
            }
        }
        else
        {
            if (map != null)
            {
                map.Biomes.Clear();
            }
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("Map"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Biomes"), true);
        serializedObject.ApplyModifiedProperties();

    }


    bool Contains(BiomeMap b, Color32 c)
    {
        if (b.Map == null || b.Biomes == null || b.Biomes.Count == 0) return false;
        foreach (var val in b.Biomes)
            if (val.color.r == c.r && val.color.b == c.b && val.color.g == c.g)
                return true;
        return false;
    }
}